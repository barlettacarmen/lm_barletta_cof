package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		assertEquals(0,pl[0].getBuiltEmporium());
		assertEquals(1,pl[0].get_numAssistants());
		
	
	}

}
