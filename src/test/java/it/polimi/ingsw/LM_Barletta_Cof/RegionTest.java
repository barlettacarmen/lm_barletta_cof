package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class RegionTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		for(int i=1;i<9;i++){
		Configuration config = new Configuration("Config/file_config"+i+".txt", 2);
		Map m= new Map(pl,config);
		for(Region r: m.getRegions()){
				assertNotNull(r);
				assertNotNull(r.getCities());
				assertNotNull(r.getCouncil());
				assertNotNull(r.getRegionCard());
				assertNotNull(r.getName());
				assertNotNull(r.getPermits(1));
				assertNotNull(r.getPermits(0));
				
		}
				
	}
	}

}
