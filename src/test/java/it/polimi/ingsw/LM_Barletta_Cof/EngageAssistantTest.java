
package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;
public class EngageAssistantTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		pl[0].setCoin(30);
		//Configuration config = new Configuration("/Users/carmen/Desktop/file_configprova.txt", 2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		int oldAssistants=pl[0].get_numAssistants();
		EngageAssistant e=new EngageAssistant(pl[0],m);
		boolean check=e.doAction();
		pl[1].setCoin(0);
		assertEquals(pl[1].getCoin(),0);
		int oldAssistants2=pl[1].get_numAssistants();
		EngageAssistant e2=new EngageAssistant(pl[1],m);
		boolean check2=e2.doAction();
		assertNotNull(m);
		assertEquals(check,true);
		assertEquals(check2,false);
		assertEquals(pl[0].get_numAssistants(),oldAssistants+1);
		assertEquals(pl[1].get_numAssistants(),oldAssistants2);
	}

}
