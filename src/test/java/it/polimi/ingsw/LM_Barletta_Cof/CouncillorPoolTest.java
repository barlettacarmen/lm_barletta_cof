package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class CouncillorPoolTest {

	@Test
	public void test() {
		int availableCouncillors=0;
		boolean flag=false;
		Player[] pl= new Player[6];
		for(int i=0;i<6;i++)
			pl[i]=new Player(i+1);
		
		for(int i=1;i<9;i++){
		Configuration config = new Configuration("Config/file_config"+i+".txt", 2);
		Map m= new Map(pl,config);
			assertNotNull(m.getCouncillorPool());
			for(Councillor c: m.getCouncillorPool().getAvailableCouncillors()){
				if(c.isAvailable())
					availableCouncillors++;}
			if(availableCouncillors==8)
				 flag=true;
				assertTrue(flag);
				
	} 	
	
	}

}
