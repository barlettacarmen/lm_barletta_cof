package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class CityTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		for(int i=1;i<9;i++){
		Configuration config = new Configuration("Config/file_config"+i+".txt", 2);
		Map m= new Map(pl,config);
		for(Region r: m.getRegions())
			for(City c: r.getCities()){
				assertNotNull(c);
				assertNotNull(c.getColour());
				assertNotNull(c.getBonus());
				assertNotNull(c.getName());
				assertNotNull(c.getNextCities());
			}
				
	}
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		City c=m.getCityPointer("Arhon");
		assertFalse(c.isColonizedByMe(pl[0]));
		
	}
}
