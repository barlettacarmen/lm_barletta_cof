package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class PermitCardTest {

	@Test
	public void test() {
		Player[] pl= new Player[6];
		for(int i=0;i<6;i++)
			pl[i]=new Player(i+1);
		
		for(int i=1;i<9;i++){
		Configuration config = new Configuration("Config/file_config"+i+".txt", 2);
		Map m= new Map(pl,config);
		for(Region r: m.getRegions()){
			assertNotNull(r.getPermitsList());
			assertNotNull(r.getPermitsList().get(0).getBonus());
			assertNotNull(r.getPermitsList().get(0).getCitiesPermitted());
			assertNotNull(r.getPermitsList().get(1).getBonus());
			assertNotNull(r.getPermitsList().get(1).getCitiesPermitted());
		}
				
	}
	
	
	}

}
