package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class ItemTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		Market market=new Market(m);
		pl[0].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		pl[0].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		pl[1].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		pl[1].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		assertEquals (pl[1].getPermitCard().size(),2);
		market.addItem(pl[0],pl[0].getPermitCard().get(0), 10);
		market.addItem(pl[0],pl[0].getPoliticsCard().get(0), 80);
		PermitCard p=market.itemList.get(0).getPermitCard();
		int price2=market.itemList.get(1).getPrice();
		PoliticsCard p2=market.itemList.get(1).getPoliticsCard();
		int price=market.itemList.get(0).getPrice();
		assertNotNull(p);
		assertEquals(price,10);
		assertNotNull(p2);
		assertEquals(price2,80);
		assertEquals(market.getItemList().size(),2);
		
		assertNotNull(market);
		assertNotNull(m);
	}
	

	}


