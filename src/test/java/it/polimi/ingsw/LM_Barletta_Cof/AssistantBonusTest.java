package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class AssistantBonusTest {

	/** Testato lo useBonus come metodo che aggiunge assistenti a quelli già posseduti dal player
	 * 
	 */
	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		pl[0].set_numAssistants(0);
		pl[1].set_numAssistants(50);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		AssistantBonus as= new AssistantBonus(config);
		as.useBonus(pl[0], m);
		assertNotNull(as);
		assertEquals(0+as.getAssistants(),pl[0].get_numAssistants());
		assertEquals(50,pl[1].get_numAssistants());
		AssistantBonus assis= new AssistantBonus(config);
		as.useBonus(pl[1], m);
		assertEquals(50+as.getAssistants(),pl[1].get_numAssistants());
		assis.useBonus(pl[1],m);
		assertEquals(50+as.getAssistants()+assis.getAssistants(),pl[1].get_numAssistants());
		assertEquals(Constants.ASSISTANT_BONUS,assis.getId());
		assertEquals(Constants.ASSISTANT_BONUS,as.getId());
		
	}

}
