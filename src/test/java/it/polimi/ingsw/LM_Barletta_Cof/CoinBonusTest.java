package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class CoinBonusTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		pl[0].setCoin(0);
		pl[1].setCoin(23);
		Configuration config = new Configuration("/Users/carmen/Desktop/gameConfigurationFiles/file_config1.txt", 2);
		Map m= new Map(pl,config);
		CoinBonus cb=new CoinBonus(config);
		cb.useBonus(pl[0], m);
		assertNotNull(cb);
		assertEquals(0+cb.getCoin(),pl[0].getCoin());
		assertEquals(23,pl[1].getCoin());
		CoinBonus cbb=new CoinBonus(config);
		cb.useBonus(pl[1], m);
		assertEquals(23+cb.getCoin(),pl[1].getCoin());
		cbb.useBonus(pl[1],m);
		assertEquals(23+cb.getCoin()+cbb.getCoin(),pl[1].getCoin());
		assertEquals(Constants.COIN_BONUS,cbb.getId());
		assertEquals(Constants.COIN_BONUS,cb.getId());
	}

}
