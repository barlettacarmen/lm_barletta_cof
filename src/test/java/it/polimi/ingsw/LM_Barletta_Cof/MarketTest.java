package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class MarketTest {

	@Test
	public void test() {
				Player[] pl= new Player[2];
				pl[0]=new Player(1);
				pl[1]=new Player(2);
				Configuration config = new Configuration("Config/file_config1.txt", 2);
				Map m= new Map(pl,config);
				Market market=new Market(m);
				pl[0].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
				pl[0].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
				pl[1].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
				pl[1].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
				assertEquals (pl[1].getPermitCard().size(),2);
				market.addItem(pl[0],pl[0].getPermitCard().get(0), 10);
				assertEquals(market.getItemList().get(0).getPrice(),10);
				assertEquals(pl[1].getCoin(),22);
				market.addExchange(pl[0], pl[0].getPermitCard().get(1),"BLUE");
				assertEquals(market.getExchangeList().size(),1);
				assertEquals(market.getItemList().size(),1);
		
			    Item i=market.getItemList().get(0);
			    market.buyItem(pl[1],i);
			    assertEquals (pl[1].getCoin(),12);
				assertEquals(market.getItemList().size(),0);
				assertEquals (pl[1].getPermitCard().size(),3);
				assertEquals (pl[0].getPermitCard().size(),2);
				assertNotNull(market);
				assertNotNull(m);
			}
	


	}


