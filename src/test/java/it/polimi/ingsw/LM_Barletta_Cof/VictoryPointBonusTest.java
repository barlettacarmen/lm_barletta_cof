package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class VictoryPointBonusTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		pl[0].set_victoryPoint(0);
		pl[1].set_victoryPoint(100);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		VictoryPointBonus vp= new VictoryPointBonus(config);
		vp.useBonus(pl[0], m);
		assertNotNull(vp);
		assertEquals(vp.getVictoryPoints(),pl[0].get_victoryPoint());
		assertEquals(100,pl[1].get_victoryPoint());
		VictoryPointBonus vpp= new VictoryPointBonus(config);
		vp.useBonus(pl[1], m);
		assertEquals(100+vp.getVictoryPoints(),pl[1].get_victoryPoint());
		vpp.useBonus(pl[1],m);
		assertEquals(100+vp.getVictoryPoints()+vpp.getVictoryPoints(),pl[1].get_victoryPoint());
		assertEquals(Constants.VICTORY_POINT_BONUS,vp.getId());
		assertEquals(Constants.VICTORY_POINT_BONUS,vpp.getId());
	}

}
