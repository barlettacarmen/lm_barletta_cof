package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class KingCardTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		for(KingCard kc: m.getKingCard()){
			assertNotNull(kc);
			assertTrue(kc.getBonus().getVictoryPoints()>0);
			assertTrue(kc.isAvailable());
			assertTrue(kc.getBonus().getId()>0);
			
		}
	}

}
