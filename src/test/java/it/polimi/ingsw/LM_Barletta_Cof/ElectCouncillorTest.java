package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class ElectCouncillorTest {

	@Test
	public void test() {
		Councillor cou=null;
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		
		for(Councillor c: m.getCouncillorPool().getAvailableCouncillors())
			if(c.isAvailable())
				cou=c;
		ElectCouncillor e= new ElectCouncillor(pl[0], m, new Cli());
		Councillor cl= m.getRegions()[0].getCouncil().getCouncil().get(0);
		assertTrue(e.electCouncillor(m.getRegions()[0].getCouncil(),cou));
		assertEquals(m.getRegions()[0].getCouncil().getCouncil().get(3),cou);
		assertFalse(cou.isAvailable());
		assertTrue(cl.isAvailable());
				
				
	}			

}
