package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class CouncilTest {

	@Test
	public void test() {
		Player[] pl= new Player[6];
		for(int i=0;i<6;i++)
			pl[i]=new Player(i+1);
		
		for(int i=1;i<9;i++){
		Configuration config = new Configuration("Config/file_config"+i+".txt", 2);
		Map m= new Map(pl,config);{
		for(Region r: m.getRegions()){
			assertNotNull(r.getCouncil());
			for(Councillor c:r.getCouncil().getCouncil())
				assertFalse(c.isAvailable());
		}
		for(Councillor c:m.getKingsCouncil().getCouncil())
			assertFalse(c.isAvailable());
		}		
	}
	}

}
