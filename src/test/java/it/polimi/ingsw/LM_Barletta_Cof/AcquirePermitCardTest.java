package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class AcquirePermitCardTest {

	@Test
	public void test() {
		Player[] pl= new Player[6];
		for(int i=0;i<6;i++)
			pl[i]=new Player(i+1);
		
		
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		AcquirePermitCard apc= new AcquirePermitCard(pl[0],m,pl[0].getCli());
		PoliticsColours pc=m.getRegions()[0].getCouncil().getCouncil().get(0).getColour();
	
			if(pl[0].getPoliticsCard().get(0).getPoliticsColour().equals(pc))
				assertTrue(apc.checkAvailability(pc,0));
			
	
	}


}