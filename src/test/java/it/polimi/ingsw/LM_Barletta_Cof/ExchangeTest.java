package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExchangeTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		Market market=new Market(m);
		pl[0].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		pl[0].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		pl[1].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		pl[1].getPermitCard().add(new PermitCard(m.getRegions()[0],config));
		assertEquals (pl[1].getPermitCard().size(),2);
		market.addExchange(pl[0],pl[0].getPermitCard().get(0),"kultos");
		market.addExchange(pl[0],pl[0].getPoliticsCard().get(0), PoliticsColours.CELESTIAL);
		PermitCard p=market.exchangeList.get(0).getCardOffered();
		String price2=market.exchangeList.get(1).getPoliticsCardRequested().toString();
		PoliticsCard p2=market.exchangeList.get(1).getPoliticsCardOffered();
		String price=market.exchangeList.get(0).getCardRequested();
		assertNotNull(p);
		assertEquals(price,"kultos");
		assertNotNull(p2);
		assertEquals(price2,"CELESTIAL");
		assertEquals(market.getExchangeList().size(),2);
		Player test=market.exchangeList.get(0).getSeller();
		assertEquals(pl[0],test);
		assertNotNull(market);
		assertNotNull(m);
	}

}
