package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConfigurationTest {

	@Test
	public void test() {
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		assertEquals(5,config.getNumberOfCitiesForEachRegion());
		assertEquals("Juvelar",config.getKingsInitialPosition());
		assertEquals("Blue",config.getRegion1().get(0).getColour());
		assertEquals("Grey",config.getCityColours().get(2));
		assertEquals("Orange",config.getCityColours().get(3));
		assertEquals("Yellow",config.getCityColours().get(1));
	}

}
