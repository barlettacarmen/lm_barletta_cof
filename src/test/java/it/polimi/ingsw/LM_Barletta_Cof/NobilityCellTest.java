package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author carmen
 * Testato il nobility bonus che incrementa di uno il punteggio nobility del player solo se non si 
 * trova nell'ultima casella dal nobility track (20)
 * Testato il set_nobilityBonus del player che se riceve in ingresso un numero maggiore di 21, 
 * setta automaticamente al massimo (20) il punteggio nobility del player.
 * Test sui metodi che spostano il player nel nobility track assegnandogli i bonus
 *
 */
public class NobilityCellTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		pl[0].set_nobilityPoint(0);
		pl[1].set_nobilityPoint(100);
		assertEquals(20,pl[1].get_nobilityPoint());
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		VictoryPointBonus v= new VictoryPointBonus(config);
		int vp= v.getVictoryPoints();
		AssistantBonus a= new AssistantBonus(config);
		int as= a.getAssistants();
		CoinBonus c= new CoinBonus(config);
		int ac= c.getCoin();
		Bonus[] b={v,a,c};
		int cp= pl[1].getCoin();
		int ap= pl[1].get_numAssistants();
		int vpp= pl[1].get_victoryPoint();
		NobilityCell nc= new NobilityCell(b,20,m);
		m.getNobilityTrack()[20]=nc;
		m.getNobilityTrack()[20].putPlayerInNobilityCell(pl[1]);
		assertEquals(20,pl[1].get_nobilityPoint());
		assertEquals(20,m.getNobilityTrack()[20].existsPlayerInThisCell(pl[1]));
		assertEquals(cp+ac,pl[1].getCoin());
		assertEquals(ap+as,pl[1].get_numAssistants());
		assertEquals(vpp+vp,pl[1].get_victoryPoint());
		assertEquals(vpp+vp,m.getVictoryTrack()[1].getAmount());
		assertEquals(cp+ac,m.getCoinsTrack()[1].getAmount());
		
		VictoryPointBonus v2= new VictoryPointBonus(config);
		AssistantBonus a2= new AssistantBonus(config);
		CoinBonus c2= new CoinBonus(config);
		 Bonus[] b2={v2,a2,c2};
		NobilityCell nc2= new NobilityCell(b2,19,m);
		m.getNobilityTrack()[19]=nc2;
		m.updateNobilityTrackStatus(pl[0]);
		assertEquals(0,pl[0].get_nobilityPoint());
		assertEquals(0,m.getNobilityTrack()[0].existsPlayerInThisCell(pl[0]));
		pl[0].set_nobilityPoint(19);
		m.updateNobilityTrackStatus(pl[0]);
		assertEquals(19,m.getNobilityTrack()[19].existsPlayerInThisCell(pl[0]));
		m.getNobilityTrack()[19].putPlayerInNobilityCell(pl[0]);
		assertEquals(19,m.getNobilityTrack()[19].existsPlayerInThisCell(pl[0]));
		
		
		
	}

}
