package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class BuildEmporiumTest {

	@Test
	public void test() {
		boolean done=false;
	
		Player[] pl= new Player[6];
	for(int i=0;i<6;i++){
		pl[i]=new Player(i+1);
		System.out.println(pl[i].get_victoryPoint());
	}
	
	Configuration config = new Configuration("Config/file_config2.txt", 2);
	Map m= new Map(pl,config);
	BuildEmporium bd= new BuildEmporium(pl[0],m,new Cli());
	int n=bd.checkExistenceOfOtherEmporium(m.getCityPointer("Juvelar"));
	assertEquals(0,n);
	boolean k= bd.payAssistants(0);
	assertTrue(k);
	boolean j=bd.checkIfColonizedRegion(m.getRegions()[0]);
	assertFalse(j);
	bd.checkIfColonizedColor("Yellow");
		

	int points=m.getRegions()[0].getRegionCard().getBonus().getVictoryPoints();
	int ppl= pl[0].get_victoryPoint();
	assertTrue(m.getRegions()[0].getRegionCard().isAvailable());
	int kingC=m.getTheAvailableKingCard();
	int kp=m.getKingCard()[kingC].getBonus().getVictoryPoints();
	System.out.println(kp);
	assertTrue(m.getKingCard()[kingC].isAvailable());
	for(City c: m.getRegions()[0].getCities())
		bd.placeEmporium(c);
		assertEquals(5,pl[0].getBuiltEmporium());
		for(int i=0; i<5;i++){
			assertNotNull(pl[0].getEmporium().get(i));
			assertNotNull(pl[0].getEmporium().get(i).getCity());
		}
		System.out.println(pl[0].get_victoryPoint());
		assertTrue(bd.checkIfColonizedRegion(m.getRegions()[0]));
		assertFalse(m.getRegions()[0].getRegionCard().isAvailable());
		assertFalse(m.getKingCard()[kingC].isAvailable());
			
		
}
}	
