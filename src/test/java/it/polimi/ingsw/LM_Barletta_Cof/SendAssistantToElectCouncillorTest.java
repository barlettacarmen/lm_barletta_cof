package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class SendAssistantToElectCouncillorTest {

	@Test
	public void test() {
		Councillor cou=null;
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		
		for(Councillor c: m.getCouncillorPool().getAvailableCouncillors())
			if(c.isAvailable())
				cou=c;
		ElectCouncillor e= new ElectCouncillor(pl[0], m, new Cli());
		SendAssistantToElectCouncillor se= new SendAssistantToElectCouncillor(pl[0], m, new Cli());
		assertTrue(pl[0].get_numAssistants()>0);
		Councillor cl= m.getRegions()[1].getCouncil().getCouncil().get(0);
		assertTrue(e.electCouncillor(m.getRegions()[1].getCouncil(),cou));
		pl[0].set_numAssistants(pl[0].get_numAssistants()-1);
		assertEquals(m.getRegions()[1].getCouncil().getCouncil().get(3),cou);
		assertFalse(cou.isAvailable());
		assertTrue(cl.isAvailable());
		assertTrue(pl[0].get_numAssistants()==0);
	}

}
