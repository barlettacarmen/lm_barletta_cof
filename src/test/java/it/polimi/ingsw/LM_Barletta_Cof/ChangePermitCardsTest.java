package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class ChangePermitCardsTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		ChangePermitCards cpc= new ChangePermitCards(pl[0],m, new Cli());
		assertTrue(pl[0].get_numAssistants()>0);
		assertTrue(cpc.discardAssistant());
		assertTrue(pl[0].get_numAssistants()==0);
		assertFalse(cpc.discardAssistant());
		assertNotNull(m.getRegions()[2].getPermitsList());
		ArrayList<PermitCard>  previous= new ArrayList<PermitCard>(2);
		previous=m.getRegions()[2].getPermitsList();
		cpc.changePermitCards(m.getRegions()[2]);
		ArrayList<PermitCard>  previous2= new ArrayList<PermitCard>(2);
		previous=m.getRegions()[2].getPermitsList();
		assertNotNull(previous2);
		
	}

}
