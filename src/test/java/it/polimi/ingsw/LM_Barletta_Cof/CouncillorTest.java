package it.polimi.ingsw.LM_Barletta_Cof;

import static org.junit.Assert.*;

import org.junit.Test;

public class CouncillorTest {

	@Test
	public void test() {
		Player[] pl= new Player[2];
		pl[0]=new Player(1);
		pl[1]=new Player(2);
		Configuration config = new Configuration("Config/file_config1.txt", 2);
		Map m= new Map(pl,config);
		for(int i=0; i<3;i++){
			assertNotNull(m.getRegions()[i].getCouncil().getCouncil());
			for(int k=0;k<4;k++){
				assertNotNull(m.getRegions()[i].getCouncil().getCouncil().get(k));
				assertFalse(m.getRegions()[i].getCouncil().getCouncil().get(k).isAvailable());				}
			
	}

}
}