package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
import java.util.ArrayList;

public class Council implements Serializable {
	
	private ArrayList<Councillor> council= new ArrayList<Councillor>(Constants.NUM_OF_COLOURS);
	
	public Council(CouncillorPool cp){
		int i=0;
		RandomGenerator random= new RandomGenerator(Constants.NUM_OF_COUNCILLORS);
		for(int k=0;k<Constants.NUM_OF_COLOURS;k++){
			 i= random.nextInt();
			while(!(cp.getAvailableCouncillors().get(i-1).isAvailable()))
					i= random.nextInt();
			cp.getAvailableCouncillors().get(i-1).setAvailable(false);
			council.add(cp.getAvailableCouncillors().get(i-1));}
		
	}
	
	public Council(){
	
	}
	
	
	public ArrayList<Councillor> getCouncil() {
		return council;
	}

}
