package it.polimi.ingsw.LM_Barletta_Cof;
import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConfigureFile {
	private static final Logger log= Logger.getLogger( ConfigureFile.class.getName() );
	private ConfigureFile(){}
	public static void createNewFile(String path,Cli cli){
		try{
			int i;
			ArrayList<String> city;
			String c;
			boolean flag,blag;
			File file=new File(path);
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("NUMBER_OF_CITIES_FOR_EACH_REGION\n");
			do{
			i=cli.askForMaxNumerOfCitiesForEachRegion();}
			while(i<2);
			city= new ArrayList<String>(Constants.NUM_OF_REGIONS*i);
			bw.write(String.valueOf(i)+"\n");
			bw.write("NUMBER_OF_BONUS_FOR_EACH_CITY\n");
			bw.write(String.valueOf(cli.askForNumberOfBonusesForEachCity())+"\n");
			bw.write("MAX_NUMBER_OF_CITIES_FOR_EACH_PERMIT_CARD\n");
			bw.write(String.valueOf(cli.askForNumberOfCitiesonEachPermitCard())+"\n");
			bw.write("MAX_NUMBER_OF_BONUS_FOR_EACH_PERMIT_CARD\n");
			bw.write(String.valueOf(cli.askForMaxNumberOfBonusesForEachPermitCard())+"\n");
			bw.write("MAX_VALUE_OF_VICTORY_POINT_BONUS\n");
			bw.write(String.valueOf(cli.askForMaxValueOfVictoryPointBonus())+"\n");
			bw.write("MAX_VALUE_OF_COIN_BONUS\n");
			bw.write(String.valueOf(cli.askForMaxValueOfCoinBonus())+"\n");
			bw.write("MAX_NUM_OF_ASSISTANTS_BONUS\n");
			bw.write(String.valueOf(cli.askForMaxValueOfAssistantBonus())+"\n");
			bw.write("MAX_NUM_OF_BONUS_NOBILITY_CELL\n");
			bw.write(String.valueOf(cli.askForMaxNumBonusNobilityCell())+"\n");
			bw.write("REGION_1\n");
			for(int k=0;k<i;k++){
				c=cli.askForCityName().toUpperCase();
				while(city.contains(c))
					c=cli.askForCityName().toUpperCase();
				city.add(c);
				bw.write(c+"\n");	
				bw.write(cli.askForCityColour()+"\n");}
			bw.write("REGION_2\n");
			for(int k=0;k<i;k++){
				c=cli.askForCityName().toUpperCase();
				while(city.contains(c))
					c=cli.askForCityName().toUpperCase();
				city.add(c);
				bw.write(c+"\n");	
				bw.write(cli.askForCityColour()+"\n");}
			bw.write("REGION_3\n");
			for(int k=0;k<i;k++){
				c=cli.askForCityName().toUpperCase();
				while(city.contains(c))
					c=cli.askForCityName().toUpperCase();
				city.add(c);
				bw.write(c+"\n");	
				bw.write(cli.askForCityColour()+"\n");}
			bw.write("KING_INITIAL_POSITION\n");
			c=cli.askForKingInitalPosition().toUpperCase();
			while(!(city.contains(c))){
				c=cli.askForKingInitalPosition().toUpperCase();
			}
			bw.write(c+"\n");
			bw.write("CITY_LINKS\n");
			for(String s: city){
			bw.write("BEGIN_LINK\n");
			for(String st: city) System.out.print(st+" ");
			System.out.println();
			bw.write(s+"\n");
			cli.printInstructionToInsertLinks(s);
			c=cli.getString().toUpperCase();
			flag=true;
			blag=true;
			while(!("END_LINK".equals(c))&& flag){
					while(!(city.contains(c))&& blag){
						cli.printInvalidCity();
						c=cli.getString().toUpperCase();
						if("END_LINK".equals("c"))
							blag=false;}
					if(blag==false)
						flag=false;
					else {bw.write(c+"\n");
						c=cli.getString().toUpperCase();}}
					bw.write(c+"\n");
			}
			bw.write("END_CITY_LINKS\n");
			
		     bw.flush();
		     bw.close();
		     fw.close(); //it's closed!
		
	}catch(IOException e){
		//e.printStackTrace();
		log.log( Level.WARNING, e.toString(), e );
	}
		
	}
	
	public static void deleteCreatedFile(String path){
		File file= new File(path);
		file.delete();
		
	}
	

}
