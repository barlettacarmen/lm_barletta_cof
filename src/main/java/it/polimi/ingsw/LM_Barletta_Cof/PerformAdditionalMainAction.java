package it.polimi.ingsw.LM_Barletta_Cof;

public class PerformAdditionalMainAction implements QuickAction {
	Player player;
	Map map;
	public PerformAdditionalMainAction (Player p,Map m){
		player=p;
		map=m;
	}
	private boolean discardthreeAssistant(){
		boolean ok=false;
		if(player.get_numAssistants()>=3){
			player.set_numAssistants(player.get_numAssistants()-3);//3assistants is the price of an extra action
			ok=true;
		}
		return ok;
	}
	public static boolean checkAssistantsAvailability(Player player){
		boolean ok=false;
		if(player.get_numAssistants()>=3){
			ok=true;
		}
		return ok;
	}
	@Override
	public boolean doAction() {
		if(discardthreeAssistant()){
			return true;
		}
		return false;
		// TODO Auto-generated method stub

	}

}
