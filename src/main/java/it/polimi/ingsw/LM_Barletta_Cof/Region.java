package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
import java.util.ArrayList;

public class Region implements Serializable {
	private Configuration config; //necessario
	private String name;
	private ArrayList<City> cities;
	private RegionCard regionCard;
	private Council council;
	private ArrayList<PermitCard> permits;
	
	public Region(String name, Configuration config, CouncillorPool pool){
		this.config=config;
		this.name=name;
		cities= new ArrayList<City>(config.getNumberOfCitiesForEachRegion());
		ArrayList<RegionConfig> c=setCityRegion(this.name, config);
		for(RegionConfig rc: c){
			City city= new City(rc.getCity(), rc.getColour(), config,this);
			cities.add(city);}
		regionCard= new RegionCard(this, config);
		permits= new ArrayList<PermitCard>(2);
		for(int i=0;i<2;i++)
			this.permits.add(new PermitCard(this,config));
		this.council= new Council(pool);
		 
		
	}

	public ArrayList<City> getCities() {

		return cities;
	}

	public void setCities(ArrayList<City> cities) {
		this.cities = cities;
	}
	

	public ArrayList<RegionConfig> setCityRegion(String name, Configuration config){
		switch(name){
		case "REGION_1":
			return config.getRegion1();
		case "REGION_2":
			return config.getRegion2();
		//case "REGION_3":
		default:	return config.getRegion3();	
		
		}
	
	}

	public PermitCard getPermits(int i) {
		
		return this.permits.remove(i);
	}
	
	public ArrayList<PermitCard> getPermitsList() {
		return permits;
	}

	public void setPermits(){
		this.permits.add(new PermitCard(this,this.config));
	}
	
	public Council getCouncil() {
		return council;
	}
	
	public String getName() {
		return name;
	}
		
	
	public RegionCard getRegionCard() {
		return regionCard;
	}

	
}
