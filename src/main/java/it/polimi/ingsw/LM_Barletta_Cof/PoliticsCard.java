package it.polimi.ingsw.LM_Barletta_Cof;

public class PoliticsCard implements Card {
	private PoliticsColours colour;
	private boolean used=false;
	public PoliticsCard(){
		int n=RandomGenerator.choose_NumOfBonus(1,91);
		if(n>=1 && n<=13)
			this.colour=PoliticsColours.BLACK;
		else if(n>=14 && n<=26)
				this.colour=PoliticsColours.CELESTIAL;
		else if(n>=27 && n<=39)
				this.colour=PoliticsColours.ORANGE;
		else if(n>=40 && n<=52)
				this.colour=PoliticsColours.PINK;
		else if(n>=53 && n<=65)
			this.colour=PoliticsColours.PURPLE;
		else if(n>=66 && n<=78)
			this.colour=PoliticsColours.WHITE;
		else 
			this.colour=PoliticsColours.JOLLY;
		
	}

	public PoliticsColours getPoliticsColour() {
		return colour;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}
	
	

}
