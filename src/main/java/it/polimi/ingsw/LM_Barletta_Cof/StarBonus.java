package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class StarBonus implements Bonus, Serializable{
	private int id=Constants.STAR_BONUS;
	@Override
	public void useBonus(Player p, Map map){ 
		MainAction mainAction=null;
		String action;
		if(p.getMaxStarBonus()>0){
			p.getCli().printString("You're using a Star Bonus");
			int n= p.getCli().askForTipeOfMainAction();
			switch(n){
			case 1 :mainAction=new ElectCouncillor(p,map,p.getCli());
					action=("Elected councillor");break;
			case 2 :mainAction=new BuildWithHelpOfKing(p,map,p.getCli());
					action=("Built an emporium with the help of the king");break;
			case 3 :mainAction=new BuildWithPermitCard(p,map,p.getCli());
					action=("Built an emporium with a Permit Card");break;
			default :mainAction=new AcquirePermitCard(p,map,p.getCli());
					action=("Aquired Permit Card");break;
			}
			if(mainAction.doAction()){
				p.getCli().printString(action);
				p.setMaxStarBonus(p.getMaxStarBonus()-1);}
			}
		}
		
	

	@Override
	public int getId() {
		
		return id;
	}

}
