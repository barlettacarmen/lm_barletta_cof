package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
@FunctionalInterface
public interface MainAction extends Serializable{

	public static  Council chooseCouncil(Map m,CliGui cli) {//Asks for the council to choose, returns a pointer to the council
		int i=cli.askForCouncil();
		if(i==4)
			return m.getKingsCouncil();
		return m.getRegions()[i-1].getCouncil();
	}
	public boolean doAction();
}


