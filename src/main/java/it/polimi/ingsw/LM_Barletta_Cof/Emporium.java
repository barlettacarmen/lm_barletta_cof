package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class Emporium implements Serializable{
	private City position;
	private Player player;
	private boolean built;
	
	public Emporium(Player p){
		this.player=player;
		this.built=false;
	}
	
	public void buildEmporium(City c){
		this.position=c;
		this.built=true;
	}

	public City getCity() {
		return position;
	}

	public City getPosition() {
		return position;
	}

	public void setPosition(City position) {
		this.position = position;
	}

	public boolean getBuilt() {
		return built;
	}

	public void setBuilt(boolean built) {
		this.built = built;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	
}
