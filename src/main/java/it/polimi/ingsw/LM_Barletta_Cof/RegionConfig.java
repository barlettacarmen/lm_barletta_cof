package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class RegionConfig implements Serializable {
	private String city;
	private String colour;
	
	public RegionConfig(String city, String colour){
		this.city=city;
		this.colour=colour;
	}

	public String getCity() {
		return city;
	}

	public String getColour() {
		return colour;
	}
	

}
