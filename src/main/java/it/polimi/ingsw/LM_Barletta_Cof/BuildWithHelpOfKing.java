package it.polimi.ingsw.LM_Barletta_Cof;

public class BuildWithHelpOfKing extends BuildEmporium{
	private Player p;
	private Map map;
	private CliGui cli;
	private boolean[] cons={false,false,false,false};
	boolean[] playerCards;
	public BuildWithHelpOfKing (Player p, Map m,CliGui cli){
		super(p,m,cli);
		this.p=p;
		this.map=m;
		this.cli=cli;
		int cards=p.getPoliticsCard().size();
		playerCards=  new boolean[cards];
		for(int j=0;j<cards;j++)
			playerCards[j]=false;
	}
	public int satisfyKingsCouncil(Council c){
		int i;
		cli.printPlayerStatus(p);
		cli.printPlayersCardList(p);
		int multicolour=cli.askForMulticolourCardNumber();
		do{
		i=cli.askForNumberOfPoliticsCard();
		}while((i+multicolour)<=0||(i+multicolour)>4);
		int usedCard=0;
		int price=0;
		int jolly=0;
		int others=0;
		
		for(int j=0;j<p.getPoliticsCard().size();j++){
			if(p.getPoliticsCard().get(j).getPoliticsColour().toString().equals("JOLLY"))
				jolly++;
			else others++;
		}
		 if(multicolour>jolly){
			multicolour=jolly;}
		 if(i>others){
			 i=others;}
	
		 for(int k=0;k<c.getCouncil().size()&&usedCard<i;k++){
				if(checkAvailability(c.getCouncil().get(k).getColour(),k))
					usedCard++;
				
			}
		 if(usedCard<i){ //se non può soddisfare il consiglio ritorna -1 e quindi non fa spostare il re
			 p.getCli().printString("You can't satisfy the King's Council and so move the king");
			 return -1;}
		 if(usedCard==i&& usedCard!=0)
			 price=10-3*(usedCard-1);
		 else price=10-3*(multicolour-1);
		 	if(price==1)
				price=0;		
			price+=multicolour; 
		
		int cont=0;
		for(int x=0;x<p.getPoliticsCard().size()&&cont<multicolour;x++){
			if(p.getPoliticsCard().get(x).getPoliticsColour().equals(PoliticsColours.JOLLY)){
				p.getPoliticsCard().get(x).setUsed(true);
				cont++;
			}
		}
		
		for(int k=0;k<playerCards.length;k++){
			if(playerCards[k]==true)
					p.getPoliticsCard().get(k).setUsed(true);
			}
	 
		return price;
	}
	
	public boolean checkAvailability(PoliticsColours colour, int i){
		for(int k=0;k<p.getPoliticsCard().size();k++){
			if(p.getPoliticsCard().get(k).getPoliticsColour().equals(colour)&&!p.getPoliticsCard().get(k).isUsed()&&!playerCards[k]){
				playerCards[k]=true;
				cons[i]=true;
				return true;
			}
				
		}
		return false;
	}
	City build;
	public int moveKing (){
		boolean done;
		int price=0;
		
		City c= map.getKingsLocation();
		do{
		build=cli.askForNextCity(c);
		done=cli.askIfFinalMove();
		price=price+2;
		}while(!done);
		if(build.getName().equals(c.getName()))
			price=0;
		return price;
	}
	
	public void pay(int i){
		if(p.getCoin()>=i){
		p.setCoin(p.getCoin()-i);
		}
	//add an error message
	}
	@Override
	public boolean doAction(){
		boolean ok=false;
		int price1;
		int price2;
		 price1=satisfyKingsCouncil(map.getKingsCouncil());
		 if(price1==-1)
			 return ok;
		 price2=moveKing();
		 if(price1+price2<=p.getCoin())
			ok= tryToBuild();
		 else p.getCli().printString("You don't have enough Coins to satisfy King's Council and move the King");
		return ok;
	}
		 
	public boolean tryToBuild(){
		boolean flag=false;
	
	int assistants=0;	
	assistants=super.checkExistenceOfOtherEmporium(build);
	if(super.payAssistants(assistants)){
		super.placeEmporium(build);
		map.setKingsLocation(build);
		super.takeBonus(build);
		super.checkConnectedCitiesAlreadyColonizedByMeAndGetBonus(build);
		flag=true;
		
	}
	else p.getCli().printString("You don't have enough assistants to build the emporium");	
	return flag;
	}

	
}
