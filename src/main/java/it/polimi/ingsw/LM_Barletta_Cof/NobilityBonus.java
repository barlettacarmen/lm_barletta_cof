package it.polimi.ingsw.LM_Barletta_Cof;

public class NobilityBonus implements Bonus{
	
	private int id=Constants.NOBILITY_POINT_BONUS;
	
	@Override
	public void useBonus(Player p, Map m){
		if(p.get_nobilityPoint()<Constants.NOBILITY_TRACK_LENGTH){
		int n=p.get_nobilityPoint()+1;
		p.set_nobilityPoint(n);
		m.updateNobilityTrackStatus(p);}
		
	}
	@Override 
	public int getId() {
		return id;
	}

	
}
