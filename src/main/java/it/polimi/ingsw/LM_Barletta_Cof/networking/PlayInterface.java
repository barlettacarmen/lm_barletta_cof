package it.polimi.ingsw.LM_Barletta_Cof.networking;


import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.Market;
import it.polimi.ingsw.LM_Barletta_Cof.views.SocketGuiObject;

public interface PlayInterface extends Remote{
	public Map play(Map m,int id)throws RemoteException;
	public Market sell(Market market)throws RemoteException;
	public SocketGuiObject buy(Market market)throws RemoteException;
	public void setupPanel (Map m,int id) throws RemoteException;
	public void updatePanel (Map m) throws RemoteException;
	public void printString (String s) throws RemoteException;
}
