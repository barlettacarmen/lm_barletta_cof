package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import it.polimi.ingsw.LM_Barletta_Cof.views.First;
import it.polimi.ingsw.LM_Barletta_Cof.views.GamePanel;
import it.polimi.ingsw.LM_Barletta_Cof.views.Gui;
import it.polimi.ingsw.LM_Barletta_Cof.views.SocketGuiObject;
import it.polimi.ingsw.LM_Barletta_Cof.AcquirePermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.BuildWithHelpOfKing;
import it.polimi.ingsw.LM_Barletta_Cof.BuildWithPermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.ChangePermitCards;
import it.polimi.ingsw.LM_Barletta_Cof.ElectCouncillor;
import it.polimi.ingsw.LM_Barletta_Cof.EngageAssistant;
import it.polimi.ingsw.LM_Barletta_Cof.MainAction;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.Market;
import it.polimi.ingsw.LM_Barletta_Cof.PerformAdditionalMainAction;
import it.polimi.ingsw.LM_Barletta_Cof.Player;
import it.polimi.ingsw.LM_Barletta_Cof.PoliticsCard;
import it.polimi.ingsw.LM_Barletta_Cof.QuickAction;
import it.polimi.ingsw.LM_Barletta_Cof.SendAssistantToElectCouncillor;

public class ClientGUI {
	private static final Logger log= Logger.getLogger( ClientGUI.class.getName() );
	private Socket s;
	private RmiServerInt server;
	private BufferedReader dataIn;
	private PrintWriter dataOut;
	private CliInterface cli;
	public ObjectInputStream objIn;
	public ObjectOutputStream objOut;
	private Map map;
	private int playerId;
	private Player player;
	private GamePanel gamePanel;
	private Gui gui;
	private Thread play;
	private Thread sell;
	RmiServerInt game;
	private boolean listen=true;
	boolean extraAction=false;
	private String ip=System.getProperty("myapplication.ip");
	public static void main(String[] args){
		
		First select= new First();
		select.setVisible(true);
		ClientGUI clientGui= new ClientGUI(select);
		
		
	}
	
	
	
	public ClientGUI(First select) {
		while(select.getChoosen()==0) {
			try {
				Thread.sleep(200);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				log.log( Level.WARNING, e.toString(), e );
			}
		}
		select.getBtnEnter().setEnabled(false);
		select.dispose();
		if(select.getChoosen()==1){
			connectSocket();
			loginSocket();
			setMapAndPlayer();
			}
		else
			try {
				connectRMI();
				// che ritorna nome e setta atLeastOneGUI a true
			} catch (RemoteException e) {
				log.log( Level.WARNING, e.toString(), e );
			}
	
		
	}
		
	public  void connectSocket(){
		   try {
			s= new Socket(ip,3000);
			dataOut= new PrintWriter(s.getOutputStream(),true);
			dataIn=new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			
		} catch (UnknownHostException e) {
			log.log( Level.WARNING, e.toString(), e );
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
	 }	
	
	public void loginSocket(){
		boolean flag=true;
		String connect;
		try {
			while(!(dataIn.readLine().equals("Please, enter your username"))){
					toWait();
			}
			connect=JOptionPane.showInputDialog("Please, enter your username");
			dataOut.println(connect+"2");//client socket Gui
			objIn= new ObjectInputStream(s.getInputStream());			
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
	}
	

	public  void connectRMI() throws RemoteException {
		try{
			Registry registry=LocateRegistry.getRegistry();
			//Server già avviato
			game= (RmiServerInt) registry.lookup("Game");
			PlayInterface playInt=new GuiRmiPlay(this);
			game.login(this.gui,playInt,JOptionPane.showInputDialog("Insert your name"));
			game.clientTypeId();
		}catch(AccessException e){
			log.log( Level.WARNING, e.toString(), e );
		}catch(RemoteException e){
			log.log( Level.WARNING, e.toString(), e );
		}catch(NotBoundException e){
			log.log( Level.WARNING, e.toString(), e );
		}
		
		
		
	}

	public void setMapAndPlayer(){//per socket
	
		try {
			SocketGuiObject obj=new SocketGuiObject("",null);
			
			do{
				obj=(SocketGuiObject)objIn.readObject();
				toWait();
				}while(!obj.getHead().equals("Map"));
			map= (Map)obj.getContent();
			do{
				obj=(SocketGuiObject)objIn.readObject();
				
				toWait();
				}while(!obj.getHead().equals("Id"));
			playerId= (Integer)obj.getContent();
		} catch (IOException | ClassNotFoundException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
		
		try {
					
					objOut= new ObjectOutputStream(s.getOutputStream()); //sream per inviare mappa aggiornata al server
					objOut.flush();
					player=map.getPlayerlist()[playerId-1];

					
					createAndShowGame(map, playerId);
					System.out.println("Mappa ricevuta");
					startListeningAndPlay();
				
				} catch (IOException e) {
					log.log( Level.WARNING, e.toString(), e );
				}
				

		
	}
	public void createAndShowGame(Map map, int playerId){ 
		
		 gamePanel=new GamePanel(map,playerId);							
		 gamePanel.setup();
		 map.getPlayerlist()[playerId-1].setCli(new Gui(gamePanel)); //setting the gui to the player
		 gui= new Gui(gamePanel);

	}
	
	public void setMap(Map map) {
		this.map = map;
	}



	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}



	public void setPlayer(Player player) {
		this.player = player;
	}



	public void startListeningAndPlay(){
		while(listen){
			
			SocketGuiObject str=null;
			try {
				str =(SocketGuiObject) objIn.readObject();
				Thread.sleep(200);
			} catch (IOException | ClassNotFoundException e1) {
				log.log( Level.WARNING, e1.toString(), e1 );
			} catch (InterruptedException e) {
				log.log( Level.WARNING, e.toString(), e );
				Thread.currentThread().interrupt();
			}
			if(str!=null){
			switch(str.getHead()){
			case "You are playing": playTurn(map,this.playerId); break;
			case "You're offline!": gui.printString(str.getHead());break;
			case "Your turn to sell": marketSell(str);break;
			case "Map": {
				Map test=(Map)str.getContent();
				this.map= (Map)test;
				System.out.println("mappa back");
				gamePanel.update(map);//poi chiama metodo che fa update di panelgioco
			}break;
			
			case "Item bought successfully": gui.printString(str.getHead());break;
			case "Exchange completed successfully": gui.printString(str.getHead());break;
			case "Close connection": end(); break;
			default : if(str.getContent()==null)
							gui.printString(str.getHead());break;
	
			
		}
			}
		}}

	public Map playTurn(Map m,int id) {
		this.map=m;
		int actionCode;
		String action;
		MainAction mainAction=null;
		Player p=map.getPlayerlist()[playerId-1];
		p.setCli(gui);
		boolean quickActionPerformed=false;
		PoliticsCard card=new PoliticsCard();
		p.getPoliticsCard().add(card);
		if(gui.wantToPerformQuickAction()){
			
				actionCode=gui.askForTipeOfAction();
		   switchQuickAction(actionCode);
		   quickActionPerformed=true;
		   
		}
		do{
			
				actionCode=gui.askForTipeOfMainAction();
			
			switch (actionCode){
				case 1 :mainAction=new ElectCouncillor(p,map,gui);
						action=("Elected councillor");break;
				case 2 :mainAction=new BuildWithHelpOfKing(p,map,gui);
						action=("Built an emporium with the help of the king");break;
				case 3 :mainAction=new BuildWithPermitCard(p,map,gui);
						action=("Built an emporium with a Permit Card");break;
				case 4 :mainAction=new AcquirePermitCard(p,map,gui);
						action=("Aquired Permit Card");break;
				default: action=("Did nothing");}
			if(mainAction!=null&&mainAction.doAction())
				messageToAllPlayers(player.getUsername()+" " +action);
			else
				messageToAllPlayers("Something went wrong");
			extraAction=false;
		
		
		if(!quickActionPerformed){
			do{
				actionCode=gui.askForTipeOfAction();
			}while(actionCode<1||actionCode>4);
				switchQuickAction(actionCode);
				quickActionPerformed=true;
	}
		}
		while (extraAction);
		gui.printString("Turn terminated");
		try {
			if(s!=null){
			SocketGuiObject mapBack=new SocketGuiObject("Map",map);
			objOut.reset();
			objOut.flush();
			objOut.writeObject(mapBack);
			objOut.flush();
			}
			
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
		
		return map;
	}	
	private void switchQuickAction(int i){
		String action;
		QuickAction quickAction=null;
		switch (i){
		case 1 :quickAction=new EngageAssistant(player,map);
							action=(" engaged an assinstant");break;
		case 2 :quickAction=new ChangePermitCards(player,map,gui);
							action=(" changed the Permit Cards of a region");break;
		case 3 :quickAction=new SendAssistantToElectCouncillor(player,map,gui);
							action=(" sent an assistant to elect a councillor");break;
		case 4 :quickAction=new PerformAdditionalMainAction(player,map);
								extraAction=PerformAdditionalMainAction.checkAssistantsAvailability(player);
								action=(" bought an extra action");
								break;
		default: action=" did nothing";}
	
	if(quickAction!=null&&quickAction.doAction())
		messageToAllPlayers(player.getUsername() +action);
		}
	
	
	public void messageToAllPlayers(String s){
		if(this.objOut!=null){
		try {
			this.objOut.writeObject(new SocketGuiObject(s,null));
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
		}
		else if(game!=null){
			try {
				game.messageToAllPlayers(s);
			} catch (RemoteException e) {
				log.log( Level.WARNING, e.toString(), e );
			}
		}
			
	}
	
	public void marketSell(SocketGuiObject marketServer){
		sell= new Thread(){
			@Override
			public void run(){
				Market market;
				try {
					
					market= (Market) marketServer.getContent();
					market.marketTurn(player, gui, map);
					marketServer.setHead("Market back");
					marketServer.setContent(market);
					objOut.reset();
					objOut.flush();
					objOut.writeObject(marketServer);
					objOut.flush();
					SocketGuiObject buy=null;
					do{
						buy=(SocketGuiObject)objIn.readObject();
					}while(!buy.getHead().equals("Your turn to buy"));
					market= (Market) buy.getContent();
					int n=gui.selectBuyAction();
					if(n==3){
						buy.setHead("Nothing to do");
					}
					else if(n==1){
						if(!market.getItemList().isEmpty()){
							gui.printItemList(market.getItemList());
							n=gui.askForItemToChoose();
							buy.setHead("Item back");
							buy.setContent(n);
//							buy.setContent(market.getItemList().get(n-1));
						}
						else
							buy.setHead("Nothing to do");
					}
					else {
						if(!market.getExchangeList().isEmpty()){
						gui.printExchangeList(market.getExchangeList());
						n=gui.askForExchangeToChoose();
						buy.setHead("Exchange back");
						buy.setContent(n);
//						buy.setContent(market.getExchangeList().get(n-1));
						}
						else
							buy.setHead("Nothing to do");
					}
					objOut.reset();
					objOut.flush();
					objOut.writeObject(new SocketGuiObject(buy.getHead(),buy.getContent()));
					objOut.flush();
				} catch (ClassNotFoundException e) {
					log.log( Level.WARNING, e.toString(), e );
				} catch (IOException e) {
					log.log( Level.WARNING, e.toString(), e );
				}
			}
		};sell.start();
		try {
			sell.join();
		} catch (InterruptedException e) {
			log.log( Level.WARNING, e.toString(), e );
			Thread.currentThread().interrupt();
		}
		
	}
	public void toWait(){
		try {
			Thread.sleep(200);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			log.log( Level.WARNING, e.toString(), e );
		}
		
	}
	
	public void end(){
		try {
			this.dataIn.close();
			this.objIn.close();
			this.dataOut.close();
			this.objOut.close();
			this.s.close();
			listen=false;
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
		
	}
	
	
	
	////////////////////////////////////////////////////////////RMI methods
	
		
	public Market marketSell(Market market){
					market.marketTurn(player, gui, map);
				
			return market;	
	}
	public SocketGuiObject marketBuy(Market market){
	
		int n=gui.selectBuyAction();
		SocketGuiObject selected=new SocketGuiObject("",null);
		if(n==3){
			
		}
		else if(n==1&&!market.getItemList().isEmpty()){
			gui.printItemList(market.getItemList());
			n=gui.askForItemToChoose();
				selected.setHead("Item");
				selected.setContent(n);
		}
		else {
			if(!market.getExchangeList().isEmpty()){
			gui.printExchangeList(market.getExchangeList());
			selected.setHead("Exchange");
			selected.setContent(n);
		}}
		return selected;
	
	
	}



	public GamePanel getGamePanel() {
		return gamePanel;
	}



	public Gui getGui() {
		return gui;
	}
	
		
}	
	

	
	

	
	
	

