package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.Market;
import it.polimi.ingsw.LM_Barletta_Cof.views.SocketGuiObject;

public class GuiRmiPlay extends UnicastRemoteObject implements PlayInterface {
	private transient ClientGUI client;
	protected GuiRmiPlay(ClientGUI client) throws RemoteException {
		super();
		this.client=client;
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Map play(Map m,int id) throws RemoteException{
		return client.playTurn(m,id);
		// TODO Auto-generated method stub

	}

	@Override
	public Market sell(Market market)throws RemoteException {
		return client.marketSell(market);

	}

	@Override
	public SocketGuiObject buy(Market market) throws RemoteException{
		return client.marketBuy(market);

	}
	@Override
	public void setupPanel (Map m,int id) throws RemoteException{
		client.setMap(m);
		client.setPlayerId(id);
		client.setPlayer(m.getPlayerlist()[(id-1)]);
		client.createAndShowGame(m, id);
		// TODO Auto-generated method stub

	}
	@Override
	public void updatePanel (Map m) throws RemoteException{
		client.setMap(m);
		client.getGamePanel().update(m);
		// TODO Auto-generated method stub

	}
	@Override
	public void printString (String s) throws RemoteException{
		if(client.getGui()!=null)
			client.getGui().printString(s);
		// TODO Auto-generated method stub

	}

}
