package it.polimi.ingsw.LM_Barletta_Cof.networking;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.LM_Barletta_Cof.Councillor;

public class CliRmiHandler extends UnicastRemoteObject implements CliInterface {
	
	
	public CliRmiHandler() throws RemoteException {
		
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; //da vedere!
	private static final Logger log= Logger.getLogger( CliRmiHandler.class.getName() );
	@Override
	public void printString(String s)  {
		try{
			
			System.out.println(s);
		}
		catch(Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}

	}

	@Override
	public int getInteger() {
		int value=0;
		BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
		boolean ok;//if the input value is not valid it enters the loop

		do{
		try{
			value= Integer.parseInt(read.readLine());
			ok=true;
			}
		catch (Exception e){
			ok=false;
			log.log( Level.WARNING, e.toString(), e );
			
		}
		}while(!ok);
		
		return value;
	}

	@Override
	public String getString() {
		String string="";
		BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
		try{
		string= read.readLine();		
		}
		catch (Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}
		return string;
	}

	@Override
	public char getChar() {
		char[] buffer=new char[1];
		InputStreamReader buf=new InputStreamReader(System.in);
		char output='n';
		try {
			buf.read(buffer, 0, 1);
			output=buffer[0];
			}
		catch (Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}
		return output;
	}

	@Override
	public void printInteger(int i) { 
		try{
			
			System.out.println(i);
		}
		catch(Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}
	}

}
