package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;

import it.polimi.ingsw.LM_Barletta_Cof.*;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.views.SocketGuiObject;

import java.util.*;


public class Acceptor implements Runnable{
	private static final Logger log= Logger.getLogger( Acceptor.class.getName() );
	private int port;
	private String address;
	private ServerSocket serverSocket;
	private boolean listening;
	private String status;
	private ArrayList<PlayerHandler> handlers= new ArrayList<PlayerHandler>();
	private Map map;
	private int i=1;
	private Timer t;
	private Thread broadcast;
	private boolean stopSocket;
	boolean atLeastOneGui=false;
	private Market market;
	private boolean extraAction;
	private ArrayList<Player> buyingPlayers=new ArrayList<Player>();
	private ArrayList<Integer> itemIndex=new ArrayList<Integer>();
	private ArrayList<Integer> exchangeIndex=new ArrayList<Integer>();
	//Field constructor 
	//If the server has already been started 
	public Acceptor(ServerSocket serverSocket,Registry registry){
		super();
		this.serverSocket=serverSocket;
		this.address=System.getProperty("myapplication.ip");
		this.port=serverSocket.getLocalPort();
		listening=true;
		status="Created";
		try{
			RmiServerInt b=new RmiServer(this);
			System.out.println("RMI Server is created");
			registry.rebind("Game", b);
			System.out.println("RMI Server is ready.");
		}catch(IOException e){
			status= "Error:"+e.getMessage();
			log.log( Level.WARNING, e.toString(), e );
		}
	}
	
	
	@Override
	public void run() {
		try{
			startListening();
			askForLogin();
			askForConfiguration();
			match();
			messageToAllPlayers("Game Ended!");
			getTheWinner(); //metodo che calcola punteggio ed elegge vincitore 
			end();
		}catch(IOException e){
			status= "Error:"+e.getMessage();
			log.log( Level.WARNING, e.toString(), e );
		}
		
		
	}

	public String getStatus() {
		
		return this.status;
	}

	public int getPort() {
		
		return this.port;
	}
	
	//Method that puts the server in a listening state,
		//preparing it to accept incoming connections
		//@throwsIO exception if the server can't be instantiated
	public void startListening() throws IOException {
		Socket s=null;
	
		while(listening){
			s = serverSocket.accept();
			//A connection has been accepted
			if(!stopSocket){
				PlayerHandler ph= new PlayerHandler(s,i);
				handlers.add(ph);
				messageToAllPlayers("Player "+i+" connected");
				System.out.println("Player "+i+" connected using socket");
			}
			if(i==2){
				connectionTimer();}
			i++;
		}													
		}
	
	public void connectRmiClient(CliInterface cli){
		PlayerHandler ph= new PlayerHandler(cli,i);
		handlers.add(ph);
		messageToAllPlayers("Player "+i+" connected");
		System.out.println("Player "+i+" connected using RMI");
		if(i==2){
			connectionTimer();		
		}
		i++;
	}
	public void connectRmiGuiClient(CliGui cli,PlayInterface play,String name){
		PlayerHandler ph= new PlayerHandler(cli,i);
		ph.setPlay(play);
		ph.setClientGui(true);
		ph.setUsername(name);
		handlers.add(ph);
		messageToAllPlayers("Player "+i+" connected");
		System.out.println("Player "+i+" connected using RMI and GUI");
		if(i==2){
			connectionTimer();		
		}
		i++;
	}
								
	public void connectionTimer(){
		t= new Timer();
		t.schedule(new TimerTask() {
			@Override
			public void run(){
				stopSocket=true;
				listening=false;
				   try {
					   Socket s= new Socket(address,3000);
						   s.close();}
						   catch(SocketException e){
							   log.log( Level.WARNING, e.toString(), e );
						   }
					 catch (UnknownHostException e) {
						
						log.log( Level.WARNING, e.toString(), e );
					} catch (Exception e) {
						
						log.log( Level.WARNING, e.toString(), e );
					}

				 }
			
		},20000);	
		
	}
		public boolean askForLogin(){
			String username="";
			String n="0";
			for(PlayerHandler ph: handlers){
				if(!ph.isClientGui()){ 
				username=ph.getCli().askForLogin();
				ph.setUsername(username);}
				if(ph.getSocket()!=null){
					n=username.charAt(username.length()-1)+"";
					username= username.substring(0, username.length()-1);
					ph.setUsername(username);
				}
	
				if(n.equals("2")){
					ph.setClientGui(true);
					n="0";
					try {
						ph.setObjOut(new ObjectOutputStream(ph.getSocket().getOutputStream()));
						ph.getObjOut().flush();
					} catch (IOException e) {
						log.log( Level.WARNING, e.toString(), e );
					}
					atLeastOneGui=true;}
				
			
		}
			return atLeastOneGui;
		}
		public void askForConfiguration(){
			int read=0;
			String c;
			
			int numOfPlayers=handlers.size();
			//ricopio i playerHandler  in un array di players da passare alla mappa
			Player[] pl= new Player[numOfPlayers];
			for(int i=0;i<numOfPlayers;i++)
				pl[i]=handlers.get(i).getP();
			
			if(!atLeastOneGui){ //se non ci sono client Gui chiedi configurazione
			while(!(read==1) && !(read==2)){
				handlers.get(0).getCli().printString("Do you want to use a default configuration file or a custom one?");
				handlers.get(0).getCli().printString("1 - Default File");
				handlers.get(0).getCli().printString("2 - Customized File");
	  			read = handlers.get(0).getCli().getInteger();
	  			if(!(read==1) && !(read==2))
	  				handlers.get(0).getCli().printString("You typed the wrong command!");
	  		}
			if(read==1)
				c=Configuration.chooseOneFile();
					 
			else{
				c=Configuration.createNewPath();
				ConfigureFile.createNewFile(c,(Cli) handlers.get(0).getCli()); 
			}
		}else c=Configuration.chooseOneFile();
			
			Configuration config= new Configuration(c,numOfPlayers);
			this.map=new Map(pl,config);
			this.market=new Market(map);
			
			//serializzazione mappa
			mapToAllPlayersSocket(map);
			
			//prova apertura buffer
			openTheObj();
			System.out.println("Buffer aperto");
			
		}
		
		public void openTheObj(){
			for(PlayerHandler ph: this.handlers)
				if(ph.getObjOut()!=null)
					try {
						ph.setObjIn(new ObjectInputStream(ph.getSocket().getInputStream()));
					} catch (IOException e) {
						log.log( Level.WARNING, e.toString(), e );
					}
		}
		/** Gestione dei turni e del timer per la mossa.
		 * Se player conclude turno prima dello scadere del timer, thead turn muore e viene preso dalla join
		 * e il timer viene cancellato, altrimenti se il player non conclude entro tot tempo la mossa
		 *viene settato come OffLine e non partecierà più alla partita
		 *Se ci sono meno di due players onLine allora la partita si conclude. 
		 *
		 */		
		
		public boolean match(){ 
			boolean inMatch=true;
			int k;
			Timer timer;
			Thread turn ;
			while(inMatch){
			for(k=0; k<handlers.size()&& !(handlers.get(k).getP().hasFinished()); k++){
				if(!handlers.get(k).isOffLine()){
				final int j=k;
				turn= new Thread(){
					
					@Override
					public void run(){
						playerPlaysTurn(handlers.get(j));
						}
					};
				turn.start();
				
					timer= new Timer();
						timer.schedule(new TimerTask() {
				@Override
				public void run(){
					handlers.get(j).setOffLine(true);
					if(!handlers.get(j).isClientGui())
						handlers.get(j).getCli().printString("You're offline!");
					else if(handlers.get(j).isClientGui()&&handlers.get(j).getSocket()!=null){
						try {
							handlers.get(j).getObjOut().reset();
							handlers.get(j).getObjOut().flush();
							handlers.get(j).getObjOut().writeObject(new SocketGuiObject("You're offline!", null));
							handlers.get(j).getObjOut().flush();
							broadcast.interrupt();
						} catch (IOException e) {
							log.log( Level.WARNING, e.toString(), e );
						}
						
					}
					else if(handlers.get(j).getPlay()!=null){
						try {
							handlers.get(j).getPlay().printString("You're offline!");
						} catch (RemoteException e) {
							log.log( Level.WARNING, e.toString(), e );
						}
					}
				}
				},180000);
				try{
					turn.join(180000);
					timer.cancel();
					if(broadcast!=null){
						broadcast.join();
						this.mapToAllPlayersSocketBis(map);
					}
				}
				catch(InterruptedException e){
					Thread.currentThread().interrupt();
					log.log( Level.WARNING, e.toString(), e );
				}
				}
			
				}
			if(handlers.get(k-1).getP().hasFinished()|| !areThereAtLeastTwoPlayersOnLine())
				inMatch=false;
			///////////////////////////
			/*Market starts here*/
			
			//market to all client socket gui
			
			for(k=0; k<handlers.size() && !(handlers.get(k).getP().hasFinished())&&areThereAtLeastTwoPlayersOnLine(); k++){
				market.setMap(map);
				if(!handlers.get(k).isClientGui())
					market.marketTurn(handlers.get(k).getP(),handlers.get(k).getCli(),map);
				else
					if(handlers.get(k).getSocket()!=null){
						try {
							handlers.get(k).getObjOut().reset();
							handlers.get(k).getObjOut().flush();
							handlers.get(k).getObjOut().writeObject(new SocketGuiObject("Your turn to sell",market));
							handlers.get(k).getObjOut().flush();
						} catch (IOException e1) {
							log.log( Level.WARNING, e1.toString(), e1 );
						}
						SocketGuiObject hear=new SocketGuiObject("",null);
						do{
							try {
								hear=(SocketGuiObject)handlers.get(k).getObjIn().readObject();
							} catch (ClassNotFoundException | IOException e1) {
								log.log( Level.WARNING, e1.toString(), e1 );
							}
							try {
								Thread.sleep(200);
							}
							catch (InterruptedException e) {
								Thread.currentThread().interrupt();
								log.log( Level.WARNING, e.toString(), e );
							}
						}while (!hear.getHead().equals("Market back"));
							market= (Market)hear.getContent();
							map=market.getMap();
					}
					else{
						try {
							if(handlers.get(k).getPlay()!=null){
							market=handlers.get(k).getPlay().sell(market);
							this.map=market.getMap();
							handlers.get(k).getPlay().updatePanel(map);
							}
						} catch (RemoteException e) {
							log.log( Level.WARNING, e.toString(), e );
						}
					}
				map=market.getMap();
			}
			////////end of sequential selling part of the market
			////////start of parallel buying part
			
				buyingPlayers=new ArrayList<Player>();
				itemIndex=new ArrayList<Integer>();
				exchangeIndex=new ArrayList<Integer>();
			
			ArrayList<Thread> marketThread=new ArrayList<Thread>(handlers.size());
			for(PlayerHandler plh:handlers){
				if(!plh.getP().hasFinished()&&areThereAtLeastTwoPlayersOnLine()){
					Thread buyTime=new Thread(){
					@Override
						public void run(){
						buyTurn(plh);
					}
					};
					marketThread.add(buyTime);
					buyTime.start();
				}
			}
			for(Thread buy:marketThread){
				try{
					buy.join(20000);/*I have to wait for all threads to end, otherwise i could implement a timer*/
					}
				catch(InterruptedException e){
					Thread.currentThread().interrupt();
					log.log( Level.WARNING, e.toString(), e );
				}
			}
			ArrayList<Item> deletedItems=new ArrayList<Item>();
			ArrayList<Exchange> deletedExchanges=new ArrayList<Exchange>();
			ArrayList<Player> successItem=new ArrayList<Player>();
			ArrayList<Player> successExchange=new ArrayList<Player>();
			
			/*added on July 10 2016*/       //this code should do the buying process
			for(int i=0;i<buyingPlayers.size();i++){
				if(itemIndex.get(i)>-1&&!deletedItems.contains(market.getItemList().get(itemIndex.get(i)))){
					deletedItems.add(market.getItemList().get(itemIndex.get(i)));
					successItem.add(buyingPlayers.get(i));
				}
				else if(exchangeIndex.get(i)>-1&&!deletedExchanges.contains(market.getExchangeList().get(exchangeIndex.get(i)))){
					deletedExchanges.add(market.getExchangeList().get(exchangeIndex.get(i)));
					successExchange.add(buyingPlayers.get(i));
				}	
			}
			for(int i=0;i<successItem.size();i++){
			market.buyItem(successItem.get(i), deletedItems.get(i));
			}
			for(int i=0;i<successExchange.size();i++){
			market.acceptExchange(successExchange.get(i), deletedExchanges.get(i));
			}
			map=market.getMap();
			mapToAllPlayersSocketBis(map);
			
			/*Market ends here*/
			//////////////////////////////////////////////
			
			}
			return inMatch;
		
				
			}
		public void buyTurn(PlayerHandler ph){
			if(!ph.isClientGui()){
				ph.getCli().printString("Type 1 if you want to buy an item, 2 if you want to accept an exchange, any other number if you want to exit the market");
				int chosen=ph.getCli().getInteger();
				int cont=0;
				if(chosen==1&&!market.getItemList().isEmpty()){
					ph.getCli().printItemList(market.getItemList());
					int choosen;
					do{
						choosen=ph.getCli().askForItemToChoose();
						cont++;
					}while(choosen>market.getItemList().size()||choosen<1&&cont<2);
					
					buyingPlayers.add(ph.getP());
					itemIndex.add(choosen-1);
					exchangeIndex.add(-1);
//					market.buyItem(ph.getP(), market.getItemList().get(choosen-1));
//					ph.getCli().printString("Item bought successfully");
//					ph.getCli().printPlayersCardList(ph.getP());
				}
				if(chosen==2&&!market.getExchangeList().isEmpty()){
					ph.getCli().printExchangeList(market.getExchangeList());
					do{
						chosen=ph.getCli().askForExchangeToChoose();
						cont++;
					}while(chosen>market.getExchangeList().size()||chosen<1&&cont<2);
					buyingPlayers.add(ph.getP());
					exchangeIndex.add(chosen-1);
					itemIndex.add(-1);
//					market.acceptExchange(ph.getP(),market.getExchangeList().get(chosen-1));
//					ph.getCli().printString("Exchange completed successfully");
//					ph.getCli().printPlayersCardList(ph.getP());
				}
			}
			else{
				if(ph.getSocket()!=null){
					SocketGuiObject marketToAll=new SocketGuiObject("Your turn to buy",market);
					try {
						ph.getObjOut().reset();
						ph.getObjOut().flush();
						ph.getObjOut().writeObject(marketToAll);
						System.out.println("Market inviato");
						ph.getObjOut().flush();
					} catch (IOException e) {
						log.log( Level.WARNING, e.toString(), e );
					}
					String s="";
					SocketGuiObject read=new SocketGuiObject("",null);
					while(!s.equals("Item back")&&!s.equals("Exchange back")&&!s.equals("Nothing to do")){
						try {
							Thread.sleep(200);
						}
						catch (InterruptedException e) {
							Thread.currentThread().interrupt();
							log.log( Level.WARNING, e.toString(), e );
						}
						
						try {
							read=(SocketGuiObject)ph.getObjIn().readObject();
						} catch (ClassNotFoundException e) {
							log.log( Level.WARNING, e.toString(), e );
						} catch (IOException e) {
							log.log( Level.WARNING, e.toString(), e );
						}
						s=read.getHead();
					} 
					System.out.println(s);
						int item;
						int exchange;
						if(s.equals("Item back")){
							item= (Integer) read.getContent();
							buyingPlayers.add(ph.getP());
							itemIndex.add(item-1);
							exchangeIndex.add(-1);
//								try{
//								market.buyItem(ph.getP(), item);
//								}catch (Exception e){
//									log.log( Level.WARNING, e.toString(), e );	
//							}
						}
						else if(s.equals("Exchange back")){
							exchange=(Integer) read.getContent();
							buyingPlayers.add(ph.getP());
							exchangeIndex.add(exchange-1);
							itemIndex.add(-1);
//								try{
//								market.acceptExchange(ph.getP(), exchange);
//								}catch(Exception e){
//									log.log( Level.WARNING, e.toString(), e );
//								}
						}
					
					}
				else{
					try {
						if(ph.getPlay()!=null){
							
							SocketGuiObject received=ph.getPlay().buy(market);
							if(received.getHead().equals("Item")){
								buyingPlayers.add(ph.getP());
								itemIndex.add((Integer)received.getContent()-1);
								exchangeIndex.add(-1);
								
							}
							else if(received.getHead().equals("Exchange")){
								buyingPlayers.add(ph.getP());
								exchangeIndex.add((Integer)received.getContent()-1);
								itemIndex.add(-1);
							}
						}
					} catch (RemoteException e) {
						log.log( Level.WARNING, e.toString(), e );
					}//Gui rmi client buy turn
				}
			}
		}
		/**This code is commented just because we wanted to update all the clients about
		 * the moves of the other clients during the match through a log of events in the GamePanel
		 *but we did't have enough time just to finish this implementation.
		 */
		public void messageToAllPlayers(String s) {
			for(PlayerHandler ph: handlers)
				if(!ph.isClientGui())
				ph.getCli().printString(s); 
//				else if(ph.isClientGui()&&ph.getPlay()!=null){
//					try {
//					
//						ph.getPlay().printString(s);
//					} catch (RemoteException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//				else if(ph.isClientGui()&&ph.getSocket()!=null&&ph.getObjOut()!=null){
//					try {
//						ph.getObjOut().reset();
//						ph.getObjOut().flush();
//						ph.getObjOut().writeObject(new SocketGuiObject(s,null));
//						ph.getObjOut().flush();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
		}
		
		public void mapToAllPlayersSocket(Map mapa){
			SocketGuiObject mappa=new SocketGuiObject("Map",mapa);
			SocketGuiObject id=new SocketGuiObject("Id",null);
			for(PlayerHandler ph: handlers){
					try {
						if(ph.isClientGui()&&ph.getSocket()!=null&&ph.getObjOut()!=null){
						ph.smartSend();
						ph.getObjOut().flush();
						ph.getObjOut().writeObject(mappa);
						ph.getObjOut().flush();
						id.setContent(ph.getP().getId());
						ph.smartSend();
						ph.getObjOut().flush();
						ph.getObjOut().writeObject(id);
						ph.getObjOut().flush();
						}
						else
							if(ph.isClientGui()&&ph.getPlay()!=null)
								ph.getPlay().setupPanel(map, ph.getP().getId());
					} catch (IOException e) {
						log.log( Level.WARNING, e.toString(), e );
					}
			}
		}
		public void mapToAllPlayersSocketBis(Map mapa){
			SocketGuiObject mappa=new SocketGuiObject("Map",mapa);
			for(PlayerHandler ph: handlers){
					try {
						if(ph.isClientGui()&&ph.getSocket()!=null&&ph.getObjOut()!=null){
						ph.smartSend();
						ph.getObjOut().flush();
						ph.getObjOut().writeObject(mappa);
						ph.getObjOut().flush();
						}
						else if(ph.isClientGui()&&ph.getPlay()!=null)
							ph.getPlay().updatePanel(map);
					} catch (IOException e) {
						log.log( Level.WARNING, e.toString(), e );
					}
			}
		}
		
		public Map getMap() {
			return map;
		}

		public void playerPlaysTurn(PlayerHandler ph) {
			messageToAllPlayers("Player "+ph.getP().getId()+" is playing");
			if(!ph.isClientGui())
				playTurn(ph);//warning:when a socket client is closed server enters an infinite loop of warnings for missing input
			else if(ph.getSocket()!=null){//client gui socket
					SocketGuiObject play=new SocketGuiObject("You are playing",null);
					System.out.println("A client is playing");
					try {
						ph.getObjOut().writeObject(play);
					} catch (IOException e) {
						log.log( Level.WARNING, e.toString(), e );
					}
					playSocketGui(ph);
			}	try {
				if(ph.getPlay()!=null){
				map=ph.getPlay().play(map,ph.getP().getId());
				ph.getPlay().updatePanel(map);
				}
			} catch (Exception e) {
				ph.setOffLine(true);
				log.log( Level.WARNING, e.toString(), e );
				////to be logged
			}
			
		}
		
		public void playSocketGui(PlayerHandler ph){
			
			 broadcast= new Thread(){
					@Override
					public void run() {
						while(!this.isInterrupted()){
							SocketGuiObject str=null;
							try {
								str = (SocketGuiObject)ph.getObjIn().readObject();
							
							} catch (Exception e) {
								interrupt();
								 ph.setOffLine(true);
								 ph.setObjOut(null);
								 log.log( Level.WARNING, e.toString(), e );
							}
							if(str!=null)
							switch(str.getHead()){
		                        case "Map": { Map mapReceived;
								mapReceived=(Map)str.getContent();
		                            map=mapReceived;
								System.out.println("!!!!!!!!!!!!!!!!!!!!!!");
								this.interrupt();
		                       
							}				
							default : messageToAllPlayers(str.getHead());
				
						}
						}
							
					}
					}; broadcast.start();
						try {
							broadcast.join();
							mapToAllPlayersSocketBis(map);
						} catch (InterruptedException e) {
							log.log( Level.WARNING, e.toString(), e );
							Thread.currentThread().interrupt();
						}
					
						
							
		
		}
		
		

		public void playTurn(PlayerHandler ph) {
			int actionCode;
			String action;
			MainAction mainAction=null;
			
			Player p=ph.getP();
			boolean quickActionPerformed=false;
			PoliticsCard card=new PoliticsCard();
			p.getPoliticsCard().add(card);
			if(ph.getCli().wantToPerformQuickAction()){
				
					actionCode=ph.getCli().askForTipeOfAction();
			   switchQuickAction(actionCode,ph);
			   quickActionPerformed=true;
			   
			}
			do{
				
					actionCode=ph.getCli().askForTipeOfMainAction();
				
				switch (actionCode){
					case 1 :mainAction=new ElectCouncillor(p,map,ph.getCli());
							action=("Elected councillor");break;
					case 2 :mainAction=new BuildWithHelpOfKing(p,map,ph.getCli());
							action=("Built an emporium with the help of the king");break;
					case 3 :mainAction=new BuildWithPermitCard(p,map,ph.getCli());
							action=("Built an emporium with a Permit Card");break;
					case 4 :mainAction=new AcquirePermitCard(p,map,ph.getCli());
							action=("Aquired Permit Card");break;
					default: action=("Did nothing");}
				if(mainAction!=null&&mainAction.doAction())// da rimpiazzare con ciclo che richiede la mossa
					messageToAllPlayers("Player "+ph.getP().getId()+" ("+ph.getUsername()+") "+action);
				else
					messageToAllPlayers("Something went wrong");
				extraAction=false;
			
			
			if(!quickActionPerformed){
				do{
					actionCode=ph.getCli().askForTipeOfAction();
				}while(actionCode<1||actionCode>4);
					switchQuickAction(actionCode, ph);
					quickActionPerformed=true;
		}
			}
			while (extraAction);
			ph.getCli().printString("Turn terminated");
			
		}	
		private void switchQuickAction(int i, PlayerHandler ph){
			String action;
			
			Player p=ph.getP();
			QuickAction quickAction=null;
			switch (i){
			case 1 :quickAction=new EngageAssistant(p,map);
								action=(" engaged an assinstant");break;
			case 2 :quickAction=new ChangePermitCards(p,map,ph.getCli());
								action=(" changed the Permit Cards of a region");break;
			case 3 :quickAction=new SendAssistantToElectCouncillor(p,map,ph.getCli());
								action=(" sent an assistant to elect a councillor");break;
			case 4 :quickAction=new PerformAdditionalMainAction(p,map);
									extraAction=PerformAdditionalMainAction.checkAssistantsAvailability(p);
									action=(" bought an extra action");
									break;
			default: action=" did nothing";}
		
		if(quickAction!=null&&quickAction.doAction())
			messageToAllPlayers("Player "+ph.getP().getId()+" ("+ph.getUsername()+") "+action);
			}
		
		

		public boolean isListening() {
			return listening;
		}
		
 public boolean areThereAtLeastTwoPlayersOnLine(){
	 int n=0;
	 for(PlayerHandler ph: this.handlers)
		 if(!ph.isOffLine())
			 n++;
	 if(n>=2)
		 return true;
	 return false;
 }


		//Method that stops the server from Listening
			//closes all the open sockets
			//@throws the IOEXC if some socket can't be closed

		public ArrayList<PlayerHandler> getHandlers() {
	return handlers;
}


		public void setMap(Map map) {
	this.map = map;
}


public void setMarket(Market market) {
	this.market = market;
}


		public void stopListening() throws IOException{
			
				for(PlayerHandler ph: this.handlers)
					ph.end();
			
				status="Closed.";
			}
		
		/**
		 * Notifies to all players who is the winner setting all the points
		 *
		 */	
				public void getTheWinner(){
					int[] positions = {1,1}; //numero di players in prima pos e numero di player in seconda pos
					int i,k=0, max=0;
					
					this.handlers.sort(PlayerHandler.orderByNobility); //classifica players per nobility points
					for(i=0;i<handlers.size()-1 && k<2;i++){ //conta quanti players sono in prima pos e quanti in seconda
						if(handlers.get(i).getP().get_nobilityPoint()==handlers.get(i+1).getP().get_nobilityPoint())
							positions[k]++;
							else  k++;
								
		 		}
					//tutti quelli in prima pos guadagnano 5 vicoryPoints
					for(i=0;i<positions[0];i++)
						handlers.get(i).getP().set_victoryPoint(handlers.get(i).getP().get_victoryPoint()+5);
					//tutti quelli in seconda pos guadagnano 2 vicorty points solo se in prima pos c'è un solo player	
					if(positions[0]==1)
						for(i=1;i<positions[1];i++)
							handlers.get(i).getP().set_victoryPoint(handlers.get(i).getP().get_victoryPoint()+2);
					
					//player con più permit card guadagna 3 victory points
					this.handlers.sort(PlayerHandler.orderByPermit);	
					this.handlers.get(0).getP().set_victoryPoint(this.handlers.get(0).getP().get_victoryPoint()+3);
					
					//player con maggior numero di victory points vince 
					this.handlers.sort(PlayerHandler.orderByVictory);
				 //se ci sono più player con gli stessi victory points
				//devo controllare chi ha più assistenti
					max=this.handlers.get(0).getP().get_victoryPoint();
					if(max==this.handlers.get(1).getP().get_victoryPoint()){
						this.handlers.sort(PlayerHandler.orderByAssistants);
						max=this.handlers.get(0).getP().get_numAssistants(); //se ci sono più player con gli stessi assistenti
				if(max==this.handlers.get(1).getP().get_numAssistants()){ //devo controllare chi ha più politics cards
							this.handlers.sort(PlayerHandler.orderByPolitics);
							messageToAllPlayersBis("Player " +this.handlers.get(0).getP()+" won with "+max+" Permit Card!");}
					
						else messageToAllPlayersBis("Player " +this.handlers.get(0).getP().getId()+" won with "+max+" assistants!");
					}
					else messageToAllPlayersBis("Player " +this.handlers.get(0).getP().getId()+" won with "+max+" victory points!");
		 		
		
			messageToAllPlayers("Close connection");												
					}
				public void messageToAllPlayersBis(String s) {
					for(PlayerHandler ph: handlers)
						if(!ph.isClientGui())
						ph.getCli().printString(s); 
						else if(ph.isClientGui()&&ph.getPlay()!=null){
							try {
							
								ph.getPlay().printString(s);
							} catch (RemoteException e) {
								log.log( Level.WARNING, e.toString(), e );
							}
						}
						else if(ph.isClientGui()&&ph.getSocket()!=null&&ph.getObjOut()!=null){
							try {
								ph.getObjOut().reset();
								ph.getObjOut().flush();
								ph.getObjOut().writeObject(new SocketGuiObject(s,null));
								ph.getObjOut().flush();
							} catch (IOException e) {
								log.log( Level.WARNING, e.toString(), e );
							}
						}
				}
				public void end(){
					for(PlayerHandler ph:this.handlers){
						try {
							ph.end();
						} catch (IOException e) {
							log.log( Level.WARNING, e.toString(), e );
						}
					}
				}
						
		}
		

