package it.polimi.ingsw.LM_Barletta_Cof.networking;
import it.polimi.ingsw.LM_Barletta_Cof.networking.*;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
	private static final Logger log= Logger.getLogger( Client.class.getName() );
	private CliInterface cli;
	private RmiServerInt server;
	private Socket s;
	private BufferedReader dataIn;
	private PrintWriter dataOut;
	private BufferedReader stdIn;
	private String ip=System.getProperty("myapplication.ip");
	private boolean inGame=true;
	private Thread listen;
	
	
	public  void connectSocket(){
		   try {
			s= new Socket(ip,3000);
			dataOut= new PrintWriter(s.getOutputStream(),true);
			dataIn=new BufferedReader(new InputStreamReader(s.getInputStream()));
			stdIn= new BufferedReader(new InputStreamReader(System.in));
			
			Input input= new Input(dataIn,inGame);
			listen= new Thread(input);
			listen.start();

		} catch (Exception e) {
			log.log( Level.WARNING, e.toString(), e );
		}
			

	 }
    public static void main(String[] args) throws IOException, InterruptedException {
    	Client client = new Client();
       
      //Which network interface must be used?
      		String read="";
      		while(!(read.equals("1") ) && !(read.equals("2"))){
      			System.out.println("Choose your network interface:");
      			System.out.println("1 - Socket");
      			System.out.println("2 - RMI");
      			read = readL("\n");
      			if(!read.equals("1")  && !read.equals("2"))
      				System.out.println("You typed the wrong command!");
      		}
      		
      		if(read.equals("1")){ 
      			client.connectSocket();
      			client.login();
      			client.playGame();}
      		else 
      			client.connectRMI(); 
    	
 
        
    }
    
	 
    	
    
    
    
   public  void connectRMI() throws RemoteException {
		try{
			Registry registry=LocateRegistry.getRegistry();
			//Server già avviato
			RmiServerInt game= (RmiServerInt) registry.lookup("Game");
			cli= new CliRmiHandler();
			game.login(cli);
		}catch(AccessException e){
			log.log( Level.WARNING, e.toString(), e );
		}catch(RemoteException e){
			log.log( Level.WARNING, e.toString(), e );
		}catch(NotBoundException e){
			log.log( Level.WARNING, e.toString(), e );
		}
		
	}
   public void login(){
	   String userInput;
		boolean flag=true;  
		  
	   while(flag){
		   try {
				userInput=stdIn.readLine();
			   dataOut.println(userInput+1); //client CLI
			   if(userInput!="")
				   flag=false;
			
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
	   }
   }
   public void playGame() throws IOException{
	   String userI;
	  
	  
	   while(inGame){
		   while((userI=stdIn.readLine())!=null){
		   dataOut.println(userI);
		   }
		   
	   }
	   dataOut.close();
	   dataIn.close();
	   s.close();
   }
   
  
   
	private static String readL(String format, Object... args)
			throws IOException {
		if (System.console() != null) {
			return System.console().readLine(format, args);
		}
		System.out.print(String.format(format, args));

		BufferedReader br = null;
		InputStreamReader isr = null;
		String read = null;

		isr = new InputStreamReader(System.in);
		br = new BufferedReader(isr);
		read = br.readLine();

		return read;
	}
	public static void setInGame(boolean inGame) {
		inGame = inGame;
	}


}

//Thread per mettere il clientSocket sempre in ascolto del server
class Input implements Runnable {
	private static final Logger log= Logger.getLogger( Input.class.getName() );
	BufferedReader in;
	boolean inGame;
	public Input(BufferedReader in,boolean inGame){
		this.in=in;
		this.inGame=inGame;
	}
	

	@Override
	public void run() {
		String s="";
		while(inGame){
			
			try {
				s=in.readLine();
				System.out.println(s);
				if(s.equals("Close connection")){
					inGame=false;
				}
					
			} catch (IOException e) {
				log.log( Level.WARNING, e.toString(), e );
			}
		
		}
		
	}
	
			
		
	
	
}
