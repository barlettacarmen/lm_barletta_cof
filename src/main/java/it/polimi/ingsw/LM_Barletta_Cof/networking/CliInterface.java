package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.io.IOException;
import java.io.Serializable;
import java.net.SocketException;
import java.rmi.*;

public interface CliInterface extends Serializable,Remote{
	
	
	public  void printString(String s) throws RemoteException ;
	public void printInteger(int i) throws RemoteException; //da implementare anche lato rmi
	public  int getInteger() throws RemoteException,SocketException;
	public  String getString() throws RemoteException;
	public char getChar() throws RemoteException;
	
	

}
