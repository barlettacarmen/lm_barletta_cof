package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import it.polimi.ingsw.LM_Barletta_Cof.CliGui;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.Market;
import it.polimi.ingsw.LM_Barletta_Cof.Player;

public class RmiServer extends UnicastRemoteObject implements RmiServerInt{
	transient Acceptor acceptor;
	public RmiServer(Acceptor acc) throws RemoteException {
		super();
		 acceptor=acc;
	
	}
	
	public RmiServer() throws RemoteException{}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean login(CliInterface a) throws RemoteException {
		acceptor.connectRmiClient(a);
		return true;
	}
	@Override 
	public void clientTypeId(){
		acceptor.atLeastOneGui=true;  //client GUI RMI
	}

	@Override
	public boolean disconnect() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean login(CliGui a,PlayInterface p,String name) throws RemoteException {
		acceptor.connectRmiGuiClient(a,p ,name);
		return true;
	}
	@Override
	public void updateMap(Map m)throws RemoteException{
		acceptor.setMap(m);
		
	}
	@Override
	public void updateMarket(Market m)throws RemoteException{
		acceptor.setMarket(m);
		
	}
	@Override
	public void setGui(CliGui cli,Player p)throws RemoteException{
		for(PlayerHandler ph:acceptor.getHandlers()){
			if (ph.getP().equals(p))
				ph.setCli(cli);
		}
			
		
	}
	@Override
	public void messageToAllPlayers(String s)throws RemoteException{
		acceptor.messageToAllPlayers(s);
			
	}
}
