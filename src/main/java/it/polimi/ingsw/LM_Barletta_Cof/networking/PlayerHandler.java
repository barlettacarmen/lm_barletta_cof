package it.polimi.ingsw.LM_Barletta_Cof.networking;
import it.polimi.ingsw.LM_Barletta_Cof.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.LM_Barletta_Cof.*;

public class PlayerHandler{
	
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private Player p;
	private CliGui cli;
	private ObjectOutputStream objOut;
	private ObjectInputStream objIn;
	private boolean offLine=false;
	private boolean clientGui=false;
	private PlayInterface play;
	
	private static final Logger log= Logger.getLogger( PlayerHandler.class.getName() );
	
	public PlayerHandler (Socket socket, int number){
		
		try{
			
			this.socket=socket;
			 
			p= new Player(number);
			//Setting the input and output stream
			OutputStream o= socket.getOutputStream();
			InputStream i=socket.getInputStream();
			this.out= new PrintWriter(o,true);
			this.in= new BufferedReader(new InputStreamReader(i));
			this.cli= new Cli(in,out);
			p.setCli(this.cli);
		
			
		}catch(Exception e){
		log.log( Level.WARNING, e.toString(), e );
		}
	
	}
	public PlayerHandler (CliInterface cli, int number){
		
		try{		 
			p= new Player(number);

			this.cli= new Cli(cli);
			p.setCli(this.cli);
			
		}catch(Exception e){
		log.log( Level.WARNING, e.toString(), e );
		}
	
	}
	public PlayerHandler (CliGui cli, int number){
		
		try{		 
			p= new Player(number);

			this.cli= cli;
			p.setCli(this.cli);
			
		}catch(Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}
	
	}
	public void setObjOut(ObjectOutputStream objOut) {
		this.objOut = objOut;
	}
	public ObjectOutputStream getObjOut() {
		return this.objOut;
	}
	public void smartSend(){
		ObjectOutputStream objectus=(ObjectOutputStream)this.objOut;
		try {
			objectus.reset();
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
		
	}
	
	public ObjectInputStream getObjIn() {
		return this.objIn;
	}
	public void setObjIn(ObjectInputStream objIn) {
		this.objIn = objIn;
	}
	public boolean isClientGui() {
		return clientGui;
	}
	public void setClientGui(boolean clientGui) {
		this.clientGui = clientGui;
	}
	public String getUsername() {
		return p.getUsername();
	}
	public void setUsername(String username) {
		p.setUsername(username);
		
	}
	public boolean isOffLine() {
		return offLine;
	}
	public void setOffLine(boolean offLine) {
		this.offLine = offLine;
	}
	public Player getP() {
		return p;
	}
	
	

	public void setCli(CliGui cli) {
		this.cli = cli;
	}
	public CliGui getCli() {
		return cli;
	}
	/**
	 * Method that cleans the resources used by the SocketHandler.
	 * @throws IOException if something cannot be closed.
	 */
	public boolean sochetIsAlive(){
		return this.socket.isConnected();
	}
	
	public Socket getSocket() {
		return socket;
	}
	public void end() throws IOException{
		if(socket!=null){
			in.close();
			out.close();
			if(this.objIn!=null){
				this.objIn.close();
				this.objOut.close();
			}
		socket.close();
		}
	}
	
	

	public BufferedReader getIn() {
		return in;
	}



	public PlayInterface getPlay() {
		return play;
	}
	public void setPlay(PlayInterface play) {
		this.play = play;
	}



	static Comparator<PlayerHandler> orderByNobility = (first,second)-> -(Integer.compare(first.getP().get_nobilityPoint(), second.getP().get_nobilityPoint()));
	static Comparator<PlayerHandler> orderByVictory = (first,second)-> -(Integer.compare(first.getP().get_victoryPoint(), second.getP().get_victoryPoint()));
	static Comparator<PlayerHandler> orderByAssistants = (first,second)-> -(Integer.compare(first.getP().get_numAssistants(), second.getP().get_numAssistants()));
	static Comparator<PlayerHandler> orderByPolitics = (first,second)-> -(Integer.compare(first.getP().getPoliticsCard().size(), second.getP().getPoliticsCard().size()));
	static Comparator<PlayerHandler> orderByPermit = (first,second)-> -(Integer.compare(first.getP().getPermitCard().size(), second.getP().getPermitCard().size()));

}
