package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.LM_Barletta_Cof.CliGui;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.Market;
import it.polimi.ingsw.LM_Barletta_Cof.Player;

public interface RmiServerInt extends Remote{
	
	public boolean login(CliInterface cli)throws RemoteException;
	boolean disconnect()throws RemoteException;
	public void clientTypeId() throws RemoteException;
	public boolean login(CliGui a,PlayInterface p, String name) throws RemoteException;
	public void updateMap(Map m)throws RemoteException;
	public void updateMarket(Market market)throws RemoteException;
	public void setGui(CliGui cli,Player p)throws RemoteException;
	public void messageToAllPlayers(String s)throws RemoteException;
}
