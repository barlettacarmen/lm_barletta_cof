package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.io.*;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CliSocketHandler implements CliInterface{
	private static final Logger log= Logger.getLogger( CliSocketHandler.class.getName() );
	private  transient PrintWriter out = null;
    private	transient BufferedReader in = null;
    
    public CliSocketHandler(BufferedReader in,PrintWriter out){
    	this.out=out;
    	this.in=in;
    }
    @Override
	public  void printString(String s){
			//out.flush();
    	if(out!=null){
			out.println(s);
    	}
		
	}
    @Override 
    public void printInteger(int i){
    	out.println(i);
    }
    @Override
	public  int getInteger() throws SocketException{//Can only return an integer, reads from buffer until it finds a valid input 
		int value=0;
		boolean ok=false;//if the input value is not valid it enters the loop
		boolean exit=false;
		if(in!=null){
		do{
		try{
			
				value= Integer.parseInt(in.readLine());
				
			ok=true;
			exit=true;
			}
		catch (SocketException e){
			exit=true;
			ok=true;
			try {
				in.close();
				in=null;
				out.close();
				out=null;
			} catch (IOException e1) {
			log.log( Level.WARNING, e.toString(), e );
			log.log( Level.WARNING, e1.toString(), e1 );
			//throw e;
			}
		}
		catch (IOException e){
			ok=false;
			log.log( Level.WARNING, e.toString(), e );
			
		}
		}while(!ok&&!exit);
		}try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			log.log( Level.WARNING, e.toString(), e );
			Thread.currentThread().interrupt();
				
		}
		return value;
	}
    @Override
	public  String getString(){
		String string="";
		try{
		string= in.readLine();		
		}
		catch (IOException e){
			log.log( Level.WARNING, e.toString(), e );
		}
		return string;
	}
    @Override
	public  char getChar(){
		char[] buffer=new char[1];
		char output=' ';
		
		try {
			in.read(buffer, 0, 1);
			String c=in.readLine();
			output=buffer[0];
			}
		catch (IOException e){
			log.log( Level.WARNING, e.toString(), e );
		}
		
		return output;
		
	}
}