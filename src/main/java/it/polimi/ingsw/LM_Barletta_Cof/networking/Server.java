package it.polimi.ingsw.LM_Barletta_Cof.networking;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import it.polimi.ingsw.LM_Barletta_Cof.*;

public class Server {
	private static final Logger log= Logger.getLogger( Server.class.getName() );
	public static final boolean serverListening=true;
	public   String status="Listening...";
	public   ServerSocket serverSocket;
	public static final int port=3000;
	public  String address=System.getProperty("myapplication.ip");
	public  ArrayList<Thread> tp;
	
	public  void startListening() throws IOException{
			//Creating server 
			serverSocket= new ServerSocket(port);
			tp= new ArrayList<Thread>();
//	}
	}
	
	public  void endListening() throws IOException{
		
			serverSocket.close();
			status="Closed";
			
	}
	
	/**
	 * Questa funzione cerca prima un registro locale. Nel caso non sia inizializzato, prova
	 * a crearlo da se. Nel caso non sia possibile crearlo il programma termina
	 * @return Il registro locale ottenuto o creato.
	 */
	private static Registry errorCheckedGetRegistry() {
		Registry registry = null;
		
		// Il registro non esiste ancora
			System.out.println("Il registro non è ancora avviato. Provo a crearlo..");
			try {
				registry = LocateRegistry.createRegistry(1099);
			}
			catch (RemoteException e2) {
				System.out.println("Impossibile creare il registro.");
				log.log( Level.WARNING, e2.toString(), e2 );
				System.exit(1);
			}
		
		return registry;
	}



	
	public static void main(String[] args) throws IOException{
		System.out.println("Hello welcome to the Council of Four! ");

		Server serv=new Server();
		
	try {	//Starting RMI registry
		   
			Registry registry = errorCheckedGetRegistry();
			

			//Starting server
		System.out.println("Starting server...");
		serv.startListening();
		//Starting ServerSocketAcceptor 
		System.out.println("\nServerSocket waiting for client on port " +  serv.serverSocket.getLocalPort());
		while(serv.status.equals("Listening...")){ 
			System.out.println("Starting new game...");
			//Starting a different thread (uno per ogni partita)
			Acceptor server= new Acceptor(serv.serverSocket,registry);
			Thread t = new Thread(server);
			serv.tp.add(t);
			t.start();
			while(server.isListening()) {
				try {
					Thread.sleep(200);
				}
				catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					log.log( Level.WARNING, e.toString(), e );
				}
			}
			
			}
	}catch(Exception e){
		log.log( Level.WARNING, e.toString(), e );
		try{ serv.endListening();}
		catch(Exception ex){
			log.log( Level.WARNING, ex.toString(), ex );
		}
			
	}
	
	}
}

	