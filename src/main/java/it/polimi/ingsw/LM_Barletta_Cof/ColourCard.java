package it.polimi.ingsw.LM_Barletta_Cof;

public class ColourCard implements Card {
	
	private String colour;
	private VictoryPointBonus bonus;
	private boolean available;
	
	public ColourCard(String colour, Configuration config){
		this.colour=colour;
		this.available=true;
		this.bonus= new VictoryPointBonus(config);
	}

	public String getColour() {
		return colour;
	}


	public VictoryPointBonus getBonus() {
		return bonus;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	
	

}
