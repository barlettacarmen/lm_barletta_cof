package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configuration implements Serializable {
	private String filepath;
	private int numOfPlayers;
	private int numberOfCitiesForEachRegion;
	private int numberOfBonusForEachCity;
	private int maxNumberOfCitiesForEachPermitCard;
	private int maxNumberOfBonusForEachPermitCard;
	private int maxValueOfVictoryPointBonus;
	private int maxValueOfCoinBonus;
	private int maxNumOfAssistantsBonus;
	private int maxNumOfBonusNobilityCell;
	private String kingsInitialPosition;
	private ArrayList<String> cityColours= new ArrayList<String>(Constants.NUM_OF_COLOURS);
	private ArrayList<RegionConfig> region1;
	private ArrayList<RegionConfig> region2;
	private ArrayList<RegionConfig> region3;
	private static final Logger log= Logger.getLogger( Configuration.class.getName() );
	private static int mapId;
	
	public static int getMapId() {
		return mapId;
	}

	public Configuration(String path, int numOfPlayers){
		this.filepath=path;
		this.numOfPlayers=numOfPlayers;
	
		String city;
		String colour;
		
		try{
			File file= new File(path);
			FileReader fr=new FileReader(file);
			BufferedReader br= new BufferedReader(fr);
			
			if(br.readLine().compareTo("NUMBER_OF_CITIES_FOR_EACH_REGION")==0)
				this.numberOfCitiesForEachRegion=readInteger(br.readLine());
			if(br.readLine().compareTo("NUMBER_OF_BONUS_FOR_EACH_CITY")==0)
				this.numberOfBonusForEachCity=readInteger(br.readLine());
			if(br.readLine().compareTo("MAX_NUMBER_OF_CITIES_FOR_EACH_PERMIT_CARD")==0)
				this.maxNumberOfCitiesForEachPermitCard=readInteger(br.readLine());
			if(br.readLine().compareTo("MAX_NUMBER_OF_BONUS_FOR_EACH_PERMIT_CARD")==0)
				this.maxNumberOfBonusForEachPermitCard=readInteger(br.readLine());
			if(br.readLine().compareTo("MAX_VALUE_OF_VICTORY_POINT_BONUS")==0)
				this.maxValueOfVictoryPointBonus=readInteger(br.readLine());
			if(br.readLine().compareTo("MAX_VALUE_OF_COIN_BONUS")==0)
				this.maxValueOfCoinBonus=readInteger(br.readLine());
			if(br.readLine().compareTo("MAX_NUM_OF_ASSISTANTS_BONUS")==0)
				this.maxNumOfAssistantsBonus=readInteger(br.readLine());
			if(br.readLine().compareTo("MAX_NUM_OF_BONUS_NOBILITY_CELL")==0)
				this.maxNumOfBonusNobilityCell=readInteger(br.readLine());
			if(br.readLine().compareTo("REGION_1")==0){
				this.region1= new ArrayList<RegionConfig>(this.numberOfCitiesForEachRegion);
				for(int i=0;i<numberOfCitiesForEachRegion;i++){
					city=br.readLine();
					colour=br.readLine();
					RegionConfig r=new RegionConfig(city,colour);
					this.region1.add(r);
					set_cityColour(colour);}}
			if(br.readLine().compareTo("REGION_2")==0){
				this.region2=new ArrayList<RegionConfig>(this.numberOfCitiesForEachRegion);
				for(int i=0;i<numberOfCitiesForEachRegion;i++){
					city=br.readLine();
					colour=br.readLine();
					RegionConfig r=new RegionConfig(city,colour);
					this.region2.add(r);
					set_cityColour(colour);}}
			if(br.readLine().compareTo("REGION_3")==0){
				this.region3=new ArrayList<RegionConfig>(this.numberOfCitiesForEachRegion);
				for(int i=0;i<numberOfCitiesForEachRegion;i++){
					city=br.readLine();
					colour=br.readLine();
					RegionConfig r=new RegionConfig(city,colour);
					this.region3.add(r);
					set_cityColour(colour);}}
			if(br.readLine().compareTo("KING_INITIAL_POSITION")==0)
				this.kingsInitialPosition=br.readLine();
			
			br.close();
	
			
			
		}catch(IOException e){
				//e.printStackTrace();
				log.log( Level.WARNING, e.toString(), e );
			}
		
		
	}
	
	
	public int readInteger(String line){
		String num="";
		boolean flag=true;
		for(int i=0;i<line.length() && flag;i++)
			if(Character.isDigit(line.charAt(i)))
				  num+=line.charAt(i);
			else flag=false;
		return Integer.parseInt(num);
	}
	
	public void set_cityColour(String colour){
		if(this.cityColours.contains(colour)==false)
			this.cityColours.add(colour);
		
		
	}

	public ArrayList<String> getCityColours() {
		return cityColours;
	}
	
	public String getKingsInitialPosition() {
		return kingsInitialPosition;
	}

	public void setKingsInitialPosition(String kingsInitialPosition) {
		this.kingsInitialPosition = kingsInitialPosition;
	}

	public int getNumberOfCitiesForEachRegion() {
		return numberOfCitiesForEachRegion;
	}

	public void setNumberOfCitiesForEachRegion(int numberOfCitiesForEachRegion) {
		this.numberOfCitiesForEachRegion = numberOfCitiesForEachRegion;
	}

	public int getNumberOfBonusForEachCity() {
		return numberOfBonusForEachCity;
	}

	public void setNumberOfBonusForEachCity(int numberOfBonusForEachCity) {
		this.numberOfBonusForEachCity = numberOfBonusForEachCity;
	}

	public int getMaxNumberOfCitiesForEachPermitCard() {
		return maxNumberOfCitiesForEachPermitCard;
	}

	public void setMaxNumberOfCitiesForEachPermitCard(int maxNumberOfCitiesForEachPermitCard) {
		this.maxNumberOfCitiesForEachPermitCard = maxNumberOfCitiesForEachPermitCard;
	}

	public int getMaxNumberOfBonusForEachPermitCard() {
		return maxNumberOfBonusForEachPermitCard;
	}

	public void setMaxNumberOfBonusForEachPermitCard(int maxNumberOfBonusForEachPermitCard) {
		this.maxNumberOfBonusForEachPermitCard = maxNumberOfBonusForEachPermitCard;
	}

	public int getMaxValueOfVictoryPointBonus() {
		return maxValueOfVictoryPointBonus;
	}

	public void setMaxValueOfVictoryPointBonus(int maxValueOfVictoryPointBonus) {
		this.maxValueOfVictoryPointBonus = maxValueOfVictoryPointBonus;
	}

	public int getMaxValueOfCoinBonus() {
		return maxValueOfCoinBonus;
	}

	public void setMaxValueOfCoinBonus(int maxValueOfCoinBonus) {
		this.maxValueOfCoinBonus = maxValueOfCoinBonus;
	}

	public int getMaxNumOfAssistantsBonus() {
		return maxNumOfAssistantsBonus;
	}

	public void setMaxNumOfAssistantsBonus(int maxNumOfAssistantsBonus) {
		this.maxNumOfAssistantsBonus = maxNumOfAssistantsBonus;
	}

	public int getMaxNumOfBonusNobilityCell() {
		return maxNumOfBonusNobilityCell;
	}

	public void setMaxNumOfBonusNobilityCell(int maxNumOfBonusNobilityCell) {
		this.maxNumOfBonusNobilityCell = maxNumOfBonusNobilityCell;
	}

	public ArrayList<RegionConfig> getRegion1() {
		return region1;
	}

	public void setRegion1(ArrayList<RegionConfig> region1) {
		this.region1 = region1;
	}

	public ArrayList<RegionConfig> getRegion2() {
		return region2;
	}

	public void setRegion2(ArrayList<RegionConfig> region2) {
		this.region2 = region2;
	}

	public ArrayList<RegionConfig> getRegion3() {
		return region3;
	}

	public void setRegion3(ArrayList<RegionConfig> region3) {
		this.region3 = region3;
	}

	public int getNumOfPlayers() {
		return numOfPlayers;
	}

	public void setNumOfPlayers(int numOfPlayers) {
		this.numOfPlayers = numOfPlayers;
	}

	
public String getFilepath() {
		return filepath;
	}



public static String chooseOneFile(){
	int n=RandomGenerator.choose_NumOfBonus(1, 9);
	mapId=n;
	return ("Config/file_config"+n+".txt");
}
public static String createNewPath(){
	int n=RandomGenerator.choose_NumOfBonus(9, 256);
	return ("file_config1.txt");
}
public static void main(String[] args){
	System.out.println(Configuration.chooseOneFile().length());
	System.out.println(Configuration.chooseOneFile().charAt(18));
	
}
}
