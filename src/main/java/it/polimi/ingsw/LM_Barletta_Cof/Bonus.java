package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;


public interface Bonus extends Serializable{
	public void useBonus(Player p, Map m);
	public int getId();
	
	public static  Bonus setBonus(int n, Configuration config){
		switch(n){
		case Constants.VICTORY_POINT_BONUS:
			return new VictoryPointBonus(config);
			
		case Constants.NOBILITY_POINT_BONUS:
			return new NobilityBonus();
			
		case Constants.COIN_BONUS:
			return new CoinBonus(config);
	
		case Constants.ASSISTANT_BONUS:
			return new AssistantBonus(config);
			
		case Constants.STAR_BONUS:
			return new StarBonus();
			
		case Constants.POLITICS_CARD_BONUS:
			return new PoliticsCardBonus();
			
		case Constants.BONUS_FROM_COLONIZED_CITY_BONUS:
			return new BonusFromColonizedCityBonus();
			
		case Constants.BONUS_FROM_YOUR_PERMIT_CARD_BONUS:
			return new BonusFromYourPermitCardBonus();
		 
		//	Constants.PERMIT_CARD_GRATIS_BONUS:
		default:	return new PermitCardGratisBonus();
			
		}
	}

}
