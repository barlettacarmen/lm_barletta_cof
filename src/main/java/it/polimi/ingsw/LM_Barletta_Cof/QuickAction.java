package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

@FunctionalInterface
public interface QuickAction extends Serializable{
	public boolean doAction();
}
