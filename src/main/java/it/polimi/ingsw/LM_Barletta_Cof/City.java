package it.polimi.ingsw.LM_Barletta_Cof;
import java.io.Serializable;
import java.util.ArrayList;

public class City implements Serializable {
	private String name;
	private String colour;
	private ArrayList<City> nextCities;
	private ArrayList<Emporium> emporium;
	private ArrayList<Bonus> bonus;
	private Region region;
	public Region getRegion() {
		return region;
	}


	public void setRegion(Region region) {
		this.region = region;
	}


	public City(String name, String colour, Configuration config,Region region){
		this.name=name;
		this.colour=colour;
		this.nextCities= new ArrayList<City>(); 
		this.emporium= new ArrayList<Emporium>(config.getNumOfPlayers());
		this.region=region;
		putBonus(config);
	}
	
		
	public void putBonus(Configuration config){
		
		RandomGenerator rand= new RandomGenerator(Constants.MAX_TYPE_OF_BONUS);
		int numOfBonus=RandomGenerator.choose_NumOfBonus(1, config.getNumberOfBonusForEachCity()+1);
		this.bonus= new ArrayList<Bonus>(numOfBonus); 
		for(int i=0;i<numOfBonus;i++){
			int b=rand.nextInt();
			this.bonus.add(Bonus.setBonus(b, config));
			
		}	
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public ArrayList<City> getNextCities() {
		return nextCities;
	}

	public void setNextCities(ArrayList<City> nextCities) {
		this.nextCities = nextCities;
	}



	public ArrayList<Bonus> getBonus() {
		return bonus;
	}


	public ArrayList<Emporium> getEmporium() {
		return emporium;
	}
	
	public int getEmporiumsBuilt(){
		int i=0;
		for(Emporium e:emporium){
			if(e.getBuilt()){
				i++;
			}
		}
		return i;
	}
	
	public boolean isColonizedByMe(Player p){
		if(!emporium.isEmpty()){
		for(int i=0; i<emporium.size();i++){
			if(emporium.get(i).getBuilt()&&emporium.get(i).getPlayer().getId()==p.getId()){
				return true;
			}
		}
		}
		return false;
	}

	
	
}
