package it.polimi.ingsw.LM_Barletta_Cof;

public class Constants {
	public static final int MAX_NUM_OF_BONUS=8;
	public static final int MAX_TYPE_OF_BONUS=9;
	public static final int NUM_OF_REGIONS=3;
	public static final int MAX_VALUE_OF_VICTORY_POINT_BONUS=20;
	public static final int MAX_VALUE_OF_COIN_BONUS=10;
	public static final int MAX_NUM_OF_ASSISTANTS_BONUS=3;
	public static final int NUM_OF_KING_CARD=5;
	public static final int NUM_OF_COUNCILLORS=24;
	public static final int NUM_OF_COLOURS=4;
	public static final int NOBILITY_TRACK_LENGTH=21;
	public static final int MAX_NUM_OF_BONUS_NOBILITY_CELL=4;
	public static final int INITIAL_NUM_OF_POLITICS_CARD_PLAYER=6;
	public static final int EMPORIUM_PER_PLAYER=10;
	public static final int VICTORY_POINT_BONUS=1;
	public static final int NOBILITY_POINT_BONUS=2;
	public static final int COIN_BONUS=3;
	public static final int ASSISTANT_BONUS=4;
	public static final int STAR_BONUS=5;
	public static final int POLITICS_CARD_BONUS=6;
	public static final int BONUS_FROM_COLONIZED_CITY_BONUS=7;
	public static final int BONUS_FROM_YOUR_PERMIT_CARD_BONUS=8;
	public static final int PERMIT_CARD_GRATIS_BONUS=9;
	
	
	private Constants(){}

}
