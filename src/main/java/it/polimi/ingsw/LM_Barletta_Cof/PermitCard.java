package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
import java.util.ArrayList;

public class PermitCard implements Card{
	private Region region;
	private ArrayList<String> citiesPermitted;
	private ArrayList<Bonus> bonus;
	private boolean available;
	
	public PermitCard(Region region, Configuration config){
		this.region=region;
		this.available=true;
		set_CitiesPermitted(region,config);
		putBonus(config);
		
	}
//viene scelto random il numero di città consentite da mettere sulla carta
//viene creato l'array di interi che va da 1 al numero totale di città
//vengono estratti tanti numeri quante sono le città consentite da mettere
//l'indice estratto vine diminuito di 1 e si cerca nell'arrayList di cities la città corrisp a quell'indice,
// tale città viene inserita 
	public void set_CitiesPermitted(Region region,Configuration config){
		
		int numOfCitiesPermitted=RandomGenerator.choose_NumOfBonus(1, config.getMaxNumberOfCitiesForEachPermitCard()+1);
		citiesPermitted= new ArrayList<String>(numOfCitiesPermitted);
		RandomGenerator random=new RandomGenerator(config.getNumberOfCitiesForEachRegion());
		
		for(int i=0;i<numOfCitiesPermitted;i++)
			this.citiesPermitted.add((region.getCities().get(random.nextInt()-1).getName()));
	}
	
	public void putBonus(Configuration config){

		RandomGenerator rand= new RandomGenerator(Constants.MAX_TYPE_OF_BONUS);
		int numOfBonus=RandomGenerator.choose_NumOfBonus(1, config.getMaxNumberOfBonusForEachPermitCard()+1);
		bonus= new ArrayList<Bonus>(numOfBonus);
		for(int i=0;i<numOfBonus;i++){
			int n=rand.nextInt();
			this.bonus.add(Bonus.setBonus(n, config));
			
		}


}
	public ArrayList<Bonus> getBonus() {
		return bonus;
	}

	public ArrayList<String> getCitiesPermitted() {
		return citiesPermitted;
	}

}


