package it.polimi.ingsw.LM_Barletta_Cof;

public class EngageAssistant implements QuickAction {
	Player player;
	Map map;
	public EngageAssistant(Player p,Map m){
		player=p;
		map=m;
	}
	private boolean payCoin(){
		boolean ok=false;
		if(player.getCoin()>=3){
			player.setCoin(player.getCoin()-3);//3 coins is the price of an assistant
			ok=true;
		}
		return ok;
	}
	@Override
	public boolean doAction() {
		if(payCoin()){
			player.set_numAssistants(player.get_numAssistants()+1);
			return true;
		}
		return false;
		// TODO Auto-generated method stub

	}

}
