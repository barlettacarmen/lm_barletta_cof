package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class CoinBonus implements Bonus ,Serializable{
	private int coin;
	private int id=Constants.COIN_BONUS;
	public CoinBonus(Configuration config){
		this.coin=RandomGenerator.choose_NumOfBonus(1, config.getMaxValueOfCoinBonus()+1);
	}
	
	@Override
	public void useBonus(Player p, Map m){
		int n= p.getCoin()+coin;
		p.setCoin(n);
		m.updateCoinsTrackStatus(p);
	}
	
	@Override
	public int getId() {
		return id;
	}

	public int getCoin() {
		return coin;
	}

	
}
