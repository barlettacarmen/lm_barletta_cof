package it.polimi.ingsw.LM_Barletta_Cof;

public class SendAssistantToElectCouncillor implements QuickAction {
	Player player;
	Map map;
	private CliGui cli;
	public SendAssistantToElectCouncillor (Player p,Map m,CliGui cli){
		player=p;
		map=m;
		this.cli=cli;
	}
		
	public boolean doAction(){
	boolean	flag= false;
		if(player.get_numAssistants()>0){
			ElectCouncillor e= new ElectCouncillor(player, map, cli);
			if(e.tryToElect()){
				player.set_numAssistants(player.get_numAssistants()-1);
				flag=true;} 
			}
		return flag;

}

	}