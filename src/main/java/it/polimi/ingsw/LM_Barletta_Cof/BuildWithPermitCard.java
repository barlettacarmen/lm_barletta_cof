package it.polimi.ingsw.LM_Barletta_Cof;

public class BuildWithPermitCard extends BuildEmporium{
	Player p;
	Map map;
	private CliGui cli;
	public BuildWithPermitCard (Player p, Map m,CliGui cli){
		super(p,m,cli);
		this.p=p;
		this.map=m;
		this.cli=cli;
	}
	public void discardPermitCard (PermitCard c){
		p.getPermitCard().remove(c);
	}
	public PermitCard drawPermitCard (){
		if(!p.getPermitCard().isEmpty())
			return p.getPermitCard().get(cli.askForPermitCard(p.getPermitCard())-1);
		else
			return null;
	}
	public City chooseCityToColonize(PermitCard c) {
		 String city=cli.askForCityToColonize(c.getCitiesPermitted());
		 return map.getCityPointer(city);
	}
	@Override
	public boolean doAction(){
		boolean ok=false;
		PermitCard card;
		card=drawPermitCard();
		if(card!=null){
		City c=chooseCityToColonize(card);
		discardPermitCard(card);
		int i=super.checkExistenceOfOtherEmporium(c);
		if(super.payAssistants(i)){
			super.placeEmporium(c);
			super.takeBonus(c);
			super.checkConnectedCitiesAlreadyColonizedByMeAndGetBonus(c);
		}
		ok=true;
		}
		return ok;
	}
}
