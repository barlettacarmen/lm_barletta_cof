package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
import java.util.ArrayList;

public class BonusFromColonizedCityBonus implements Bonus, Serializable{
	private int id=Constants.BONUS_FROM_COLONIZED_CITY_BONUS;
	@Override
	public void useBonus(Player p,Map m){
		
		City c=chooseAColonizedCity(p);
		if(c!=null){
		for(Bonus b: c.getBonus())
			if(b instanceof CoinBonus || b instanceof AssistantBonus || b instanceof VictoryPointBonus || 
					b instanceof PoliticsCardBonus || b instanceof PermitCardGratisBonus)
					b.useBonus(p, m);
	}
	}
	@Override
	public int getId() {
		
		return id;
	}
	
	public City chooseAColonizedCity(Player p){
		
		City city;
		ArrayList<Emporium> emp= new ArrayList<Emporium>();
		
		if(p.getBuiltEmporium()!=0){ //se hai costruito almeno un emporio
			for(int i=0;i<p.getEmporium().size();i++){
				if(p.getEmporium().get(i).getBuilt())
					emp.add(p.getEmporium().get(i));
			}
		}
			  city=p.getCli().askForColonizedCity(emp);
		return city;
		
		//chiede al player di scegliere una città colonizzata da cui prendere i bonus
		//e passa la città
		// se si la passa
	}


}