package it.polimi.ingsw.LM_Barletta_Cof;

public class VictoryPointBonus implements Bonus {
	
	private int victoryPoints;
	private int id=Constants.VICTORY_POINT_BONUS;
	public VictoryPointBonus(Configuration config){
		this.victoryPoints=RandomGenerator.choose_NumOfBonus(1, config.getMaxValueOfVictoryPointBonus()+1);
		
	}
	
	
	@Override
	public void useBonus(Player p, Map m){
		int n= p.get_victoryPoint()+victoryPoints;
		p.set_victoryPoint(n);
		m.updateVictoryTrackStatus(p);


	}
	public int getVictoryPoints() {
		return victoryPoints;
	}

	@Override
	public int getId() {
		return id;
	}


	



}
