package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class Cell implements Serializable{
	private int playerID;
	private int amount;
	
	public Cell(){
		this.playerID=0;
		this.amount=0;
	}
	public Cell(Player p, int amount){
		this.playerID=p.getId();
		this.amount=amount;
		}
	
	
	public void setPlayerID(int pId){
		this.playerID=pId;
	}
	
	public void setAmount(int am){
		this.amount=am;
	
	}
	
	public int getAmount(){
		return this.amount;
	}
	public int getPlayerID(){
		return this.playerID;
	}
	

}
