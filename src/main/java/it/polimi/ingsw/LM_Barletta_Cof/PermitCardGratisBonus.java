package it.polimi.ingsw.LM_Barletta_Cof;



public class PermitCardGratisBonus implements Bonus{
	private int id=Constants.PERMIT_CARD_GRATIS_BONUS;
	
	@Override
	public void useBonus(Player p,Map m){ 
		Region r=p.getCli().choosenRegion(m); //player sceglie la regione da cui pescare
		int n=p.getCli().choosenPermitCard(r); //player sceglie se prendere la cara permit 1 o 2 
		p.setPermitCard((r.getPermits(n))); //preleva la carta e la da al player
		r.setPermits();                     //setta un'altra carta al posto di quella prelavata
	
	}

	@Override
	public int getId() {
		
		return id;
	}
}
