package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class BonusFromYourPermitCardBonus implements Bonus, Serializable{
	private int id=Constants.BONUS_FROM_YOUR_PERMIT_CARD_BONUS;
	@Override
	public void useBonus(Player p,Map m){
		int n;
		PermitCard pc;
		if(!p.getPermitCard().isEmpty()){
		//chiede quale permitCard delle sue vuole usare (ricevo un intero e lo diminuisco di uno)
		n= p.getCli().askForPermitCard(p.getPermitCard());
		pc=p.getPermitCard().get(n-1);
		for(Bonus b: pc.getBonus()){
			//se il bonus che trovo e ancora un BonusFromYourPermitCardBonus non lo riapplico,
			// altrimenti riscchio di andare in loop
			if(!(b instanceof BonusFromYourPermitCardBonus)||!(b instanceof StarBonus) )
				b.useBonus(p, m);
		}
		}
	}

	@Override
	public int getId() {
		
		return id;
	}

}
