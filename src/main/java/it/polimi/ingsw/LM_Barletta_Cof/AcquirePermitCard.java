package it.polimi.ingsw.LM_Barletta_Cof;

public class AcquirePermitCard implements MainAction {
	private Player player;
	private Map map;
	private CliGui cli;
	private boolean[] cons={false,false,false,false};
	boolean[] playerCards;
	
	public AcquirePermitCard(Player p,Map m,CliGui cli){
		this.player=p;
		this.map=m;
		this.cli=cli;
		int cards=p.getPoliticsCard().size();
		playerCards=  new boolean[cards];
		for(int j=0;j<cards;j++)
			playerCards[j]=false;
	}
	private int selectCouncil(){
		int i;
		do{
		i=cli.askForRegion();
		}while(i<1||i>3);
		return i;
	}
	private Council returnCouncil(int i){
		return map.getRegions()[i-1].getCouncil();
	
	}
	public int satisfyCouncil(Council c){
		int i;
		cli.printPlayerStatus(player);
		cli.printPlayersCardList(player);
		int multicolour=cli.askForMulticolourCardNumber();
		do{
		i=cli.askForNumberOfPoliticsCard();
		}while((i+multicolour)<=0||(i+multicolour)>4);
		int usedCard=0;
		int price;
		int jolly=0;
		int others=0;
		
		for(int j=0;j<player.getPoliticsCard().size();j++){
			if("JOLLY".equals(player.getPoliticsCard().get(j).getPoliticsColour().toString()))
				jolly++;
			else others++;
		}
		 if(multicolour>jolly){
			multicolour=jolly;}
		 if(i>others){
			 i=others;
		 }
		 for(int k=0;k<c.getCouncil().size()&&usedCard<i;k++){
				if(checkAvailability(c.getCouncil().get(k).getColour(),k))
					usedCard++;
				
			}
		 if(usedCard<i){ //se non può soddisfare il consiglio ritorna -1 e quindi non fa spostare il re
			 player.getCli().printString("You can't satisfy the King's Council and so aquire a Permt Card");
			 return -1;}
		 if(usedCard==i&& usedCard!=0)
			 price=10-3*(usedCard-1);
		 else price=10-3*(multicolour-1);
		 	if(price==1)
				price=0;		
			price+=multicolour; 
		
		int cont=0;
		for(int x=0;x<player.getPoliticsCard().size()&&cont<multicolour;x++){
			if(player.getPoliticsCard().get(x).getPoliticsColour().equals(PoliticsColours.JOLLY)){
				player.getPoliticsCard().get(x).setUsed(true);
				cont++;
			}
		}
		
		for(int k=0;k<playerCards.length;k++){
			if(playerCards[k])
					player.getPoliticsCard().get(k).setUsed(true);
			}
	 
		return price;
	}
	
	public boolean checkAvailability(PoliticsColours colour, int i){
		for(int k=0;k<player.getPoliticsCard().size();k++){
			if(player.getPoliticsCard().get(k).getPoliticsColour().equals(colour)&&!player.getPoliticsCard().get(k).isUsed()&&!playerCards[k]){
				playerCards[k]=true;
				cons[i]=true;
				return true;
			}
				
		}
		return false;
	}
	
	public PermitCard drawPermitCard (Region r){
		int card= cli.askForPermitCard(r.getPermitsList())-1;
		PermitCard c=r.getPermits(card);
		for(int k=0;k<c.getBonus().size();k++){
			c.getBonus().get(k).useBonus(player, map);
		}
		r.getPermits(card); //rimuovo la carta 
		r.setPermits(); // ne setto un'altra

		return c;
		
	
	}
	
	@Override
	public boolean doAction(){
		boolean finish;
		int price;
		int i;
		i=selectCouncil();
		price=satisfyCouncil(returnCouncil(i));
		if(price!=-1){
			if(player.getCoin()>=price){
				player.setCoin(player.getCoin()-price);
				player.getPermitCard().add(drawPermitCard(map.getRegions()[i-1]));
				finish=true;}
			else{ player.getCli().printString("You don't have enought Coins");
					finish=false;}
		}else finish=false;
		
		return finish;
			
	}
	
}