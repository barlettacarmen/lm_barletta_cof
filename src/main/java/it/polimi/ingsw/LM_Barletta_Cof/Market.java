package it.polimi.ingsw.LM_Barletta_Cof;
import java.io.Serializable;
import java.util.ArrayList;
public class Market implements Serializable{
	ArrayList<Item> itemList = new ArrayList<Item>();
	ArrayList<Exchange> exchangeList = new ArrayList<Exchange>();
	Map map;
	//Cli cli;
	public Market(Map m){
		this.map=m;
	}
	
	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public void addItem(Player p,PermitCard c,int price){
		Item s=new Item(p,c,price);
		this.itemList.add (s);
}
	public void addItem(Player p,PoliticsCard c,int price){
		Item s=new Item(p,c,price);
		this.itemList.add (s);
}
	public void addExchange(Player p,PermitCard offered,String requested){
		Exchange ex=new Exchange(p,offered,requested);
		this.exchangeList.add (ex);
}
	public void addExchange(Player p,PoliticsCard offered,PoliticsColours requested){
		Exchange ex=new Exchange(p,offered,requested);
		this.exchangeList.add (ex);
}
	////////////////////////////used by acceptor
	//////////////////////////////////////////////////////////////////////
	public void marketTurn(Player p,CliGui cli,Map m){
		boolean done=false;
		int chosen;
		if(cli instanceof Cli){
			cli.printPlayersCardList(p);
			cli.printString("Type 1 if you want to sell an item, 2 if you want to add an exchange, any other number if you want to skip");
			chosen=cli.getInteger();
		}else chosen=cli.getInteger();
		
		if(chosen==1){
		int type=cli.askForTypeOfCard();
		if(type==1){
			if(!p.getPermitCard().isEmpty()){
				int selected=cli.askForPermitCard(p.getPermitCard());
				if(selected>=0&&selected<p.getPermitCard().size()){
					int price=cli.askForPrice();
					this.addItem(p,p.getPermitCard().get(selected), price);
					m.getPlayerlist()[p.getId()-1].getPermitCard().remove(selected);
					done=true;
		}}
		}
		else{
			int selected;
			if(!p.getPoliticsCard().isEmpty()){
			selected=cli.askForPoliticsCard(p.getPoliticsCard());
			if(selected>=0&&selected<p.getPoliticsCard().size()){
				int price=cli.askForPrice();
				this.addItem(p,p.getPoliticsCard().get(selected), price);
				m.getPlayerlist()[p.getId()-1].getPoliticsCard().remove(selected);
				done=true;
			}}
			}
		
		}
		
		if(chosen==2){
			int type=cli.askForTypeOfCard();
			if(type==1){
				if(!p.getPermitCard().isEmpty()){
					int selected=cli.askForPermitCard(p.getPermitCard());
					if(selected>=0&&selected<p.getPermitCard().size()){
						int region;
						do{
						region=cli.askForRegion();
						}while(region<1||region>3);
						City c=cli.askForCity(map.getRegions()[region-1]);
						this.addExchange(p,p.getPermitCard().get(selected),c.getName());
						m.getPlayerlist()[p.getId()-1].getPermitCard().remove(selected);
						done=true;
			}}
			}
			else{
				int selected;
				if(!p.getPoliticsCard().isEmpty()){
				selected=cli.askForPoliticsCard(p.getPoliticsCard());
				if(selected>=0&&selected<p.getPoliticsCard().size()){
					String colour=cli.askForColorOfCouncillor().toUpperCase();
					PoliticsColours chosenColour;
					if(colour.equals(PoliticsColours.PURPLE.toString())||
							colour.equals(PoliticsColours.WHITE.toString())||
							colour.equals(PoliticsColours.BLACK.toString())||
							colour.equals(PoliticsColours.CELESTIAL.toString())||
							colour.equals(PoliticsColours.PINK.toString())||
							colour.equals(PoliticsColours.ORANGE.toString())||
							colour.equals(PoliticsColours.JOLLY.toString())){
						chosenColour=Enum.valueOf(PoliticsColours.class,colour);//ValueOf to be checked
					this.addExchange(p,p.getPoliticsCard().get(selected), chosenColour);
					m.getPlayerlist()[p.getId()-1].getPoliticsCard().remove(selected);
					done=true;
					}
				}}
				}
		}
	map=m;
	}
//////////////////////////////////////////////////////////////////////////
	public Item selectItem(CliGui cli){
		cli.printItemList(itemList);
		int i=cli.askForItemToChoose();
		return itemList.get(i);
	}

	public Exchange selectExchange(CliGui cli){
		cli.printExchangeList(exchangeList);
		int i=cli.askForExchangeToChoose();
		return exchangeList.get(i);
	}

	public  void buyItem(Player p,Item i){
		p.setCoin(p.getCoin()-i.getPrice());
		this.map.getPlayerlist()[p.getId()-1].setCoin(p.getCoin());
		if(i.getPermitCard()!=null){
			map.getPlayerlist()[p.getId()-1].getPermitCard().add(i.getPermitCard());
		}
		if(i.getPoliticsCard()!=null){
			map.getPlayerlist()[p.getId()-1].getPoliticsCard().add(i.getPoliticsCard());
		}
		map.getPlayerlist()[i.getSeller().getId()-1].setCoin(i.getSeller().getCoin()+i.getPrice());
		this.itemList.remove(i);
}
	public  void acceptExchange(Player p,Exchange ex){//only accept exchanges between cards of the same type
		if(ex.getCardOffered()!=null){
			PermitCard requested=checkAvailabilityOfPermitCard(p,ex.getCardRequested());
			if (requested!=null){
				map.getPlayerlist()[p.getId()-1].getPermitCard().add(ex.getCardOffered());
				map.getPlayerlist()[ex.getSeller().getId()-1].getPermitCard().add(requested);
			map.getPlayerlist()[p.getId()-1].getPermitCard().remove(requested);
			}
		}
		if(ex.getPoliticsCardOffered()!=null){
			PoliticsCard polRequest=checkAvailabilityOfPoliticsCard(p,ex.getPoliticsCardRequested());
			if(polRequest!=null){
				map.getPlayerlist()[p.getId()-1].getPoliticsCard().add(ex.getPoliticsCardOffered());
				map.getPlayerlist()[ex.getSeller().getId()-1].getPoliticsCard().add(polRequest);
				map.getPlayerlist()[p.getId()-1].getPoliticsCard().remove(polRequest);
			}
		}//should be working, to be tested
		this.exchangeList.remove(ex);
}
	public ArrayList<Item> getItemList(){
		return itemList;
	}
	public ArrayList<Exchange> getExchangeList(){
		return exchangeList;
	}
	private PermitCard checkAvailabilityOfPermitCard(Player p,String name){
		for(int i=0;i<p.getPermitCard().size();i++){
			for(int k=0;k<p.getPermitCard().get(i).getCitiesPermitted().size();k++){
				if (p.getPermitCard().get(i).getCitiesPermitted().get(k)==name)
					return p.getPermitCard().get(i);
			}
				
		}
		return null;
	}
	private PoliticsCard checkAvailabilityOfPoliticsCard(Player p,PoliticsColours name){
		for(int i=0;i<p.getPoliticsCard().size();i++){
				if (p.getPoliticsCard().get(i).getPoliticsColour()==name)
					return p.getPoliticsCard().get(i);
				
		}
		return null;
	}
//	public boolean checkItemAvailable(Item i){
//		for(Item it:this.itemList){
//			if(it.getPoliticsCard()!=null&&it.getPoliticsCard().getPoliticsColour().equals(i.getPoliticsCard().getPoliticsColour()))
//				if(it.getPrice()==i.getPrice())
//					return true;
//			if(it.getPermitCard().equals(i.getPermitCard()))
//				if(it.getPrice()==i.getPrice())
//					return true;
//		}
//		return false;
//	}
}
