package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
import java.util.ArrayList;

import it.polimi.ingsw.LM_Barletta_Cof.views.Gui;

public class Player implements Serializable{
	private int id;
	private String username;
	private ArrayList<Emporium> emporium;
	private int builtEmporium;
	private int victoryPoint=0;
	private int nobilityPoint=0;
	private int coin; 
	private  ArrayList<PoliticsCard> politicsCard;
	private int numAssistants;
	private  ArrayList<PermitCard> permitCard;
	transient private CliGui cli;
	private int maxStarBonus;
	public Player(int id){
		this.id=id;
		this.numAssistants=this.id;
		this.politicsCard= new ArrayList<PoliticsCard>();
		for(int i=0;i< Constants.INITIAL_NUM_OF_POLITICS_CARD_PLAYER;i++)
			this.politicsCard.add(new PoliticsCard());
		this.permitCard = new ArrayList<PermitCard>();
		this.emporium= new ArrayList<Emporium>(Constants.EMPORIUM_PER_PLAYER);
		for(int i=0;i<Constants.EMPORIUM_PER_PLAYER;i++)
			this.emporium.add(new Emporium(this));
		builtEmporium=0;
		maxStarBonus=3;
		coin=20+id;
	}


	public int getId(){
		return this.id;
	}
	public int getCoin(){
		return this.coin;
	}
	public int get_numAssistants(){
		return this.numAssistants;
	}
	public int get_nobilityPoint(){
		return this.nobilityPoint;
	}
	public int get_victoryPoint(){
		return this.victoryPoint;
	}
	public void setCoin(int n){
		coin=n;
	}
	public void set_numAssistants(int n){
		numAssistants=n;
	}
	public void set_nobilityPoint(int n){
		if(n>=0&&n<21)
			this.nobilityPoint=n;
		else this.nobilityPoint=20;
	}
	public void set_victoryPoint(int n){
		victoryPoint=n;
	}

	public void setCli(CliGui cli){
		this.cli=cli;
	}
	public ArrayList<PoliticsCard> getPoliticsCard() {
		return politicsCard;
	}


	public  ArrayList<PermitCard> getPermitCard() {
		return permitCard;
	}


	public void setPermitCard(PermitCard permitCard) {
		this.permitCard.add(permitCard);
	}
	
	
	
public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public ArrayList<Emporium> getEmporium() {
		return emporium;
	}

	
	public boolean hasFinished(){
		if(this.builtEmporium==Constants.EMPORIUM_PER_PLAYER)
			return true;
		return false;
	}

	public int getBuiltEmporium() {
		return builtEmporium;
	}


	public void setBuiltEmporium(int builtEmporium) {
		this.builtEmporium = builtEmporium;
	}


	public CliGui getCli() {
		return cli;
	}


	public int getMaxStarBonus() {
		return maxStarBonus;
	}


	public void setMaxStarBonus(int maxStarBonus) {
		this.maxStarBonus = maxStarBonus;
	}

	
	
}
