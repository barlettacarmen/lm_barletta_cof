package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class KingCard implements Serializable {
	private int number;
	private VictoryPointBonus bonus;
	private boolean available;
	
	public KingCard(int n, Configuration config){
		this.number=n;
		this.available=true;
		bonus= new VictoryPointBonus(config);
	}

	public VictoryPointBonus getBonus() {
		return bonus;
	}
	
	public int getNumber(){
		return this.number;
	}
	public int getValourOfBonus(){
		return bonus.getVictoryPoints();
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	

}
