package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.polimi.ingsw.LM_Barletta_Cof.PoliticsCard;

import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.SwingConstants;

public class PoliticsCardPanel extends JDialog {

	private final JPanel contentPanel = new JPanel();


	public PoliticsCardPanel(ArrayList<PoliticsCard> politicsCard) {
		
		setTitle("POLITICS CARDS");
		setBounds(955, 300, 450, 300);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			for(PoliticsCard pc: politicsCard){
			JLabel label= insertAPoliticsCard(pc);
			label.setPreferredSize(new Dimension(70, 100));
			getContentPane().add(label);
		}
		
		contentPanel.setAutoscrolls(true);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
	}
	
	public static JLabel insertAPoliticsCard(PoliticsCard p){
		JLabel label= new JLabel();
		label.setPreferredSize(new Dimension (150,100));
		label.setOpaque(true);
		label.setEnabled(false);
		if(!p.isUsed()){
		switch(p.getPoliticsColour().toString()){
		case "BLACK": label.setBackground(Color.BLACK); break;
		case "CELESTIAL": label.setBackground(Color.CYAN); break;
		case "ORANGE": label.setBackground(Color.ORANGE); break;
		case "PINK": label.setBackground(Color.PINK); break;
		case "PURPLE": label.setBackground(Color.MAGENTA); break;
		case "WHITE": label.setBackground(Color.WHITE); break;
		default: {label.setBackground(Color.GREEN); 
				label.setText("JOLLY");
				label.setHorizontalTextPosition(SwingConstants.LEADING);
		}
	}} return label;
	}
	

}
