package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import it.polimi.ingsw.LM_Barletta_Cof.Cli;
import it.polimi.ingsw.LM_Barletta_Cof.networking.CliInterface;
import it.polimi.ingsw.LM_Barletta_Cof.networking.CliRmiHandler;
import it.polimi.ingsw.LM_Barletta_Cof.networking.Client;
import it.polimi.ingsw.LM_Barletta_Cof.networking.ClientGUI;
import it.polimi.ingsw.LM_Barletta_Cof.networking.RmiServerInt;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;

public class First extends JFrame {
	private static final Logger log= Logger.getLogger( First.class.getName() );
	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnNewRadioButton;
	private JButton btnEnter;
	private int choosen=0;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					First frame = new First();
					frame.setVisible(true);
				} catch (Exception e) {
					log.log( Level.WARNING, e.toString(), e );
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public First() {
		setTitle("Network");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 250, 450, 300);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		JLabel lblPleaseChooseYour = new JLabel("Please, choose your network interface");
		lblPleaseChooseYour.setBounds(115, 10, 220, 18);
		lblPleaseChooseYour.setHorizontalAlignment(SwingConstants.CENTER);
		lblPleaseChooseYour.setForeground(Color.BLACK);
		lblPleaseChooseYour.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		contentPane.add(lblPleaseChooseYour);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("RMI");
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		rdbtnNewRadioButton_1.setBounds(29, 146, 141, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		rdbtnNewRadioButton = new JRadioButton("Socket");
		rdbtnNewRadioButton.setSelected(true);
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		rdbtnNewRadioButton.setBounds(29, 79, 141, 23);
		contentPane.add(rdbtnNewRadioButton);
			
		btnEnter = new JButton("Enter");
		btnEnter.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		btnEnter.setBounds(167, 215, 117, 29);
		contentPane.add(btnEnter);	
		
		boolean flag=false;
		flag=createEvents();
		while(!flag);
	}
	public boolean createEvents(){
		
		btnEnter.addActionListener(new ActionListener() {
			
		
			public void actionPerformed(ActionEvent e) {
				
				ImageIcon icon= new ImageIcon("/Users/carmen/repo/lm_barletta_cof/src/main/java/it/polimi/ingsw/LM_Barletta_Cof/source/hourglass.png");
				JOptionPane.showMessageDialog(null, "Waiting for other players...", null, JOptionPane.INFORMATION_MESSAGE,icon);
				if(rdbtnNewRadioButton.isSelected())
					choosen=1;		
				else
					choosen=2;			
				
			}		
			
		});
		return true;
		
	}

	public int getChoosen() {
		return choosen;
	}

	public JButton getBtnEnter() {
		return btnEnter;
	}


	
	  
	  
	
}
