package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import it.polimi.ingsw.LM_Barletta_Cof.Configuration;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.NobilityCell;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.Player;

public class NobilityTrackPanel extends JPanel {
	
	private NobilityCell[] nobilityTrack;
	private NobilityCellPanel[] nobilityCellPanel;
	
	public NobilityTrackPanel(NobilityCell[] nobilityTrack){
		this.nobilityTrack=nobilityTrack;
		this.nobilityCellPanel= new NobilityCellPanel[21];
		for(int i=0; i<nobilityTrack.length;i++){
			this.nobilityCellPanel[i]= new NobilityCellPanel(this.nobilityTrack[i]);
			this.nobilityCellPanel[i].setBorder(new LineBorder(Color.ORANGE, 1, true));
			add(this.nobilityCellPanel[i]);
		}
	
	}
	public void update(NobilityCell[] n){
		this.nobilityTrack=n;
		int i=0;
		for(NobilityCellPanel ncp: nobilityCellPanel){
			ncp.updateNobilityCell(nobilityTrack[i]);
			i++;
		}
	}
	
}
