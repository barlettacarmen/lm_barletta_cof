package it.polimi.ingsw.LM_Barletta_Cof.views;


	
	import javax.swing.JFrame;
	import javax.swing.JPanel;
	import javax.swing.JScrollPane;
	import javax.swing.JTable;
	import javax.swing.table.AbstractTableModel;

import it.polimi.ingsw.LM_Barletta_Cof.Player;

import java.awt.Dimension;
	import java.awt.GridLayout;
	 
	/** 
	 * TableDemo is just like SimpleTableDemo, except that it
	 * uses a custom TableModel.
	 */
	public class TableDemo extends JPanel {
	    private boolean DEBUG = false;
	    private Player[] playerlist;
	    public JTable table;
	    public TableDemo(Player[] list) {
	        super(new GridLayout(1,0));
	        this.playerlist=list;
	        table = new JTable(new MyTableModel(playerlist));
	        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
	        table.setFillsViewportHeight(true);
	 
	        //Create the scroll pane and add the table to it.
	        JScrollPane scrollPane = new JScrollPane(table);
	 
	        //Add the scroll pane to this panel.
	        add(scrollPane);
	    }
	   	 public void updateTable(Player[] playerlist){
	   		 this.playerlist=playerlist;
	   		 table.setModel(new MyTableModel(playerlist));
			 }
	 
	    class MyTableModel extends AbstractTableModel {
	    	private String[] columnNames={ "Player", "Coin", "Victory", "Nobility", "Assistants","Emporiums Built"};
	    	transient public Object[][] data;
	    	private Player[] playerlist;
	    	
	    	public MyTableModel(Player[] playerlist){
	    		this.playerlist=playerlist;
	    		data= new Object[playerlist.length][columnNames.length];
			    for(int i=0;i<playerlist.length;i++){
			    	data[i][0]= playerlist[i].getUsername();
			    	data[i][1]=playerlist[i].getCoin();
			    	data[i][2]=playerlist[i].get_victoryPoint();
			    	data[i][3]=playerlist[i].get_nobilityPoint();
			    	data[i][4]=playerlist[i].get_numAssistants();
			    	data[i][5]=playerlist[i].getBuiltEmporium();
			    }
			    		
	    	}
	
	        public int getColumnCount() {
	            return columnNames.length;
	        }
	 
	        public int getRowCount() {
	            return data.length;
	        }
	 
	        public String getColumnName(int col) {
	            return columnNames[col];
	        }
	 
	        public Object getValueAt(int row, int col) {
	            return data[row][col];
	        }
	 
	        /*
	         * JTable uses this method to determine the default renderer/
	         * editor for each cell.  If we didn't implement this method,
	         * then the last column would contain text ("true"/"false"),
	         * rather than a check box.
	         */
	        public Class getColumnClass(int c) {
	            return getValueAt(0, c).getClass();
	        }
	 
	        /*
	         * Don't need to implement this method unless your table's
	         * editable.
	         */
	        public boolean isCellEditable(int row, int col) {
	            //Note that the data/cell address is constant,
	            //no matter where the cell appears onscreen.
	           
	                return false;
	            }
	        
	 
	        /*
	         * Don't need to implement this method unless your table's
	         * data can change.
	         */
	        public void setValueAt(Object value, int row, int col) {
	            if (DEBUG) {
	                System.out.println("Setting value at " + row + "," + col
	                                   + " to " + value
	                                   + " (an instance of "
	                                   + value.getClass() + ")");
	            }
	 
	            data[row][col] = value;
	            fireTableCellUpdated(row, col);
	 
	            if (DEBUG) {
	                System.out.println("New value of data:");
	                printDebugData();
	            }
	        }
	 
	        private void printDebugData() {
	            int numRows = getRowCount();
	            int numCols = getColumnCount();
	 
	            for (int i=0; i < numRows; i++) {
	                System.out.print("    row " + i + ":");
	                for (int j=0; j < numCols; j++) {
	                    System.out.print("  " + data[i][j]);
	                }
	                System.out.println();
	            }
	            System.out.println("--------------------------");
	        }
	    }

	}

