package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.io.IOException;
import java.io.Serializable;

import it.polimi.ingsw.LM_Barletta_Cof.Map;

public class SocketGuiObject implements Serializable {
//	public class SerObject extends Object implements Serializable{
//		
//		
//	}
	private String head;
	private   Object content;
	public SocketGuiObject(String s,Object map){
		this.head=s;
		this.content=map;
	}

	public String getHead() {
		return head;
	}

	public Object getContent() {
		return content;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public void setContent(Object content) {
		this.content = content;
	}
	private void writeObject(java.io.ObjectOutputStream out) throws IOException{
		out.defaultWriteObject();
	}
	private  void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException{
		in.defaultReadObject();
	}
	
}
