package it.polimi.ingsw.LM_Barletta_Cof.views;

import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import it.polimi.ingsw.LM_Barletta_Cof.Configuration;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.Player;

import javax.swing.border.LineBorder;

public class PermitCardPanel extends JPanel {
	private static final Logger log= Logger.getLogger( PermitCardPanel.class.getName() );
	JButton enter;
	private int choosen=-1;
	public  PermitCard pc;
	public JTextPane textArea;
	public BonusPanel panel;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private ArrayList<JRadioButton> radio;
	private JFrame frame;
	/**
	 * Create the panel.
	 */
	public PermitCardPanel() {}
	public PermitCardPanel(PermitCard pc) {
		this.pc=pc;
		setBorder(new LineBorder(new Color(184, 134, 11), 2, true));
		setBackground(new Color(240, 230, 140));
		setPreferredSize(new Dimension(150, 110));
		
		JLabel lblPermitCard = new JLabel("Permit Card");
		lblPermitCard.setHorizontalAlignment(SwingConstants.CENTER);
		lblPermitCard.setPreferredSize(new Dimension(150, 14));
		add(lblPermitCard);
		
		panel= new BonusPanel(pc.getBonus());
		panel.setBackground(new Color(240, 230, 200));
		panel.setPreferredSize(new Dimension(140, 40));
		add(panel);
		
		textArea = new JTextPane();
		textArea.setDisabledTextColor(Color.BLACK);
		textArea.setBackground(new Color(240, 230, 140));
		textArea.setPreferredSize(new Dimension(140, 30));
		textArea.setEnabled(false);
		
		
		setTextWithCityPermitted();
		add(textArea);
		this.setVisible(true);

	}
	
	public void setTextWithCityPermitted(){
		for(String c: this.pc.getCitiesPermitted()){
			 try {
				appendString(c+" ");
			} catch (BadLocationException e) {
				log.log( Level.WARNING, e.toString(), e );
			}
		}
	}
	
	public void appendString(String str) throws BadLocationException
	{
	     StyledDocument document = (StyledDocument) textArea.getDocument();
	     document.insertString(document.getLength(), str, null);
	                                                    
	 }
	
	public static JPanel panelOfPermitCard(ArrayList<PermitCard> a){
		JPanel panel= new JPanel();
		for(int i=0;i<a.size();i++){
			PermitCardPanel pc= new PermitCardPanel(a.get(i));
			pc.setVisible(true);
			panel.add(pc);
		}
		return panel;
	}
	public void panelCheckBoxOfPermitCard(ArrayList<PermitCard> a){
		 frame= new JFrame();
		 frame.setBounds(450, 250, 450, 300);
		radio= new ArrayList<JRadioButton>(a.size());
		JPanel panel= new JPanel();
		for(int i=0; i<a.size();i++){
			PermitCardPanel pc= new PermitCardPanel(a.get(i));
			pc.setVisible(true);
			panel.add(pc);
			 radio.add(new JRadioButton(""+i));
			 radio.get(i).setVisible(true);
			 buttonGroup.add(radio.get(i));
			 panel.add(radio.get(i));
		}
		radio.get(0).setSelected(true);
		enter= new JButton("Enter");
		enter.setVisible(true);
		panel.add(enter);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setVisible(true);
		frame.pack();
		boolean flag=false;
		
			flag=createEvents();
	
	
		
	}
public boolean createEvents(){
		enter.addActionListener(new ActionListener() {
		
		
			public void actionPerformed(ActionEvent e) {
			
				
				for(int i=0; i<radio.size();i++)
					if(radio.get(i).isSelected()){
						choosen=i+1;
						}
				frame.dispose();
			}			
		});
		return true;
		
	}

	public int getChoosen() {
		return choosen;
	}
	
}
