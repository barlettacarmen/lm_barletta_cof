package it.polimi.ingsw.LM_Barletta_Cof.views;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

import it.polimi.ingsw.LM_Barletta_Cof.Configuration;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.NobilityCell;
import it.polimi.ingsw.LM_Barletta_Cof.Player;

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class NobilityCellPanel extends JPanel {
	private static final Logger log= Logger.getLogger( NobilityCell.class.getName() );
	private NobilityCell nb;
	private BonusPanel bp;
	private JTextPane players;
	private JButton btnBonuses;
	private JButton btnP;
	/**
	 * Create the panel.
	 */
	public NobilityCellPanel(NobilityCell nb) {
		this.nb=nb;
		if(this.nb.getBonus()!=null)
			this.bp= new BonusPanel(nb.getBonus());
		else this.bp= new BonusPanel();
		this.players= new JTextPane();
		this.players.setDisabledTextColor(Color.BLACK);
		this.players.setPreferredSize(new Dimension(495, 200));
		this.players.setEnabled(false);
		setBackground(Color.ORANGE);
		setForeground(Color.BLACK);
		setPreferredSize(new Dimension(24, 80));
		
		JLabel label = new JLabel(""+nb.getgrade());
		label.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		add(label);
		
		btnBonuses = new JButton("B");
		btnBonuses.setPreferredSize(new Dimension(24, 20));
		add(btnBonuses);
		
		btnP = new JButton("P");
		btnP.setPreferredSize(new Dimension(24, 20));
		add(btnP);
		
		setTextWithPlayers();
		showBonuses();
		showPlayers();

	}
	
	public void showBonuses(){
		btnBonuses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bp.setVisible(true);
				bp.setAutoscrolls(true);
				JFrame frame= new JFrame();
				frame.setSize(400, 400);
				frame.getContentPane().add(bp);
				frame.setVisible(true);
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			}
		});
		
	}
	
	public void showPlayers(){
		btnP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  players.setVisible(true);
			  players.setAutoscrolls(true);
				JFrame frame= new JFrame();
				frame.setSize(400, 400);
				frame.getContentPane().add(players);
				frame.setVisible(true);
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
			}
		});
		
	}
	
	public void setTextWithPlayers(){
		players.setText("");
		for(Player pl: this.nb.getPlayerList()){
			 try {
				appendString(pl.getUsername()+" ");
			} catch (BadLocationException e) {
				log.log( Level.WARNING, e.toString(), e );
			}
		}
	}
	
	public void appendString(String str) throws BadLocationException
	{
	     StyledDocument document = (StyledDocument) players.getDocument();
	     document.insertString(document.getLength(), str, null);
	     
	                                                    
	 }
	
	//metodo per aggiornare interfaccia 
	public void updateNobilityCell(NobilityCell nc){
		this.nb=nc;
		setTextWithPlayers();
	}
}
