package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

import java.awt.Dimension;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import it.polimi.ingsw.LM_Barletta_Cof.Player;
import it.polimi.ingsw.LM_Barletta_Cof.networking.Server;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;

public class LeftSpace extends JPanel {
	private static final Logger log= Logger.getLogger(LeftSpace.class.getName());
	private BackgroundPane background_1;
	private Player[] playerlist;
	private int playerId;
	private JButton b;
	private JButton b3;
	private JButton b2;
	private JButton b4;
	private JButton b5;
	private JButton b1;
	private JButton b6;
	private TableDemo td;
	
	 ImageIcon	c= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/coin.png"));
	 ImageIcon v= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/v.png"));
	 ImageIcon n= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/next.png"));
	 ImageIcon pol= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/politics.jpg"));
	 ImageIcon a = new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/helping.png"));
	 ImageIcon emp= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/building.png"));
	


	public LeftSpace(Player[] playerlist, int playerId) {
		this.playerlist=playerlist;
		this.playerId=playerId;
		setOpaque(false);
		setAlignmentY(0.0f);
		BackgroundPane background= null;
		setAlignmentX(0.0f);
		setBackground(new Color(222, 184, 135));
		setForeground(Color.BLACK);
		//setBounds(600, 0, 250, 750);
		setSize(new Dimension(500,460));
		
		
        try {
        	 background_1 = new BackgroundPane();
        	 background_1.setAlignmentX(0.0f);
        	 background_1.setPreferredSize(new Dimension(500, 460));
  
			background_1.setBackground( ImageIO.read(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/sfondobottoni.jpg")));
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
		
		add(background_1);
		 ImageIcon	coin= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/botton.jpg"));
		 ImageIcon victory= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/victoryBo.jpg"));
		 ImageIcon nobility= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/nobilityBO.jpg"));
		 ImageIcon	permit= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/BuildingPermit.jpg"));
		 ImageIcon politics= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/politics.jpg"));
		 ImageIcon assistants = new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/assistantBO.png"));
		 ImageIcon emporium= new ImageIcon(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/emporiumBO.png"));
		 JLabel lblPlayerStatus = new JLabel("Player status");
		 background_1.add(lblPlayerStatus);
		 lblPlayerStatus.setForeground(Color.BLACK);
		 lblPlayerStatus.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		 lblPlayerStatus.setHorizontalAlignment(SwingConstants.CENTER);
		 lblPlayerStatus.setPreferredSize(new Dimension(500, 16));

		 b=new JButton("Coin");
		 background_1.add(b);
		 b.setPreferredSize(new Dimension(245, 70));
		 b.setIcon(coin);
		 b3=new JButton("Assistants");
		 background_1.add(b3);
		 b3.setPreferredSize(new Dimension(245, 70));
		 b3.setIcon(assistants);
		 b2= new JButton("Nobility");
		 background_1.add(b2);
		 b2.setPreferredSize(new Dimension(245, 70));
		 b2.setIcon(nobility);
		 b4=new JButton("Emporium");
		 background_1.add(b4);
		 b4.setPreferredSize(new Dimension(245, 70));
		 b4.setIcon(emporium);
		 b5=new JButton("Permit");		 	
		 background_1.add(b5);
		 b5.setPreferredSize(new Dimension(245, 70));
		 b5.setIcon(permit);
		 b1= new JButton("Victory");
		 background_1.add(b1);
		 b1.setPreferredSize(new Dimension(245, 70));
		 b1.setIcon(victory); 
		 b6=new JButton("Politics");
		 background_1.add(b6);
		 b6.setPreferredSize(new Dimension(495, 70));
		 b6.setIcon(politics);
		 td= new TableDemo(this.playerlist);
		 background_1.add(td);
		 td.setPreferredSize(new Dimension(495, 74));
		 td.setVisible(true);
		events();
		

			

	}
	public void update(Player[] list){
		this.playerlist=list;
		td.updateTable(list);
		//td.setVisible(false);
		
	}
	public void events(){
		 b.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		JOptionPane.showMessageDialog(b,"You have "+playerlist[playerId-1].getCoin()+" coins","COIN",JOptionPane.INFORMATION_MESSAGE, c);
			 	}
			 });
		 
		 b1.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		JOptionPane.showMessageDialog(b1,"You have "+playerlist[playerId-1].get_victoryPoint()+" victory points","VICTORY POINTS",JOptionPane.INFORMATION_MESSAGE, v);
			 	}
			 });
		 
		 b2.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		JOptionPane.showMessageDialog(b2,"You have "+playerlist[playerId-1].get_nobilityPoint()+" nobility points","NOBILITY POINTS",JOptionPane.INFORMATION_MESSAGE, n);
			 	}
			 });
		 
		 b3.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		JOptionPane.showMessageDialog(b3,"You have "+playerlist[playerId-1].get_numAssistants()+" assistants","ASSISTANTS",JOptionPane.INFORMATION_MESSAGE, a);
			 	}
			 });
		 
		 b4.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		JOptionPane.showMessageDialog(b4,"You have "+(10-playerlist[playerId-1].getBuiltEmporium())+" emporiums to build","EMPORIUMS",JOptionPane.INFORMATION_MESSAGE,emp);
			 	}
			 });
		 b5.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		JPanel permit= PermitCardPanel.panelOfPermitCard(playerlist[playerId-1].getPermitCard());
			 		JDialog frame= new JDialog();
			 		frame.setTitle("Permit Cards");
			 		frame.setBounds(955, 300, 450, 300);
			 		frame.getContentPane().add(permit);
			 		permit.setVisible(true);
			 		permit.setAutoscrolls(true);
			 		frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			 		frame.setVisible(true);
			 		
			 	}
			 });
		 
		 b6.addActionListener(new ActionListener() {
			 	public void actionPerformed(ActionEvent e) {
			 		PoliticsCardPanel dialog = new PoliticsCardPanel(playerlist[playerId-1].getPoliticsCard());
						dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
						dialog.setVisible(true);
			 	}
			 });
		
	}
	
	

}
