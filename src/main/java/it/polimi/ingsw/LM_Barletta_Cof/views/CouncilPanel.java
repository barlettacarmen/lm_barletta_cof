package it.polimi.ingsw.LM_Barletta_Cof.views;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import it.polimi.ingsw.LM_Barletta_Cof.Council;
import it.polimi.ingsw.LM_Barletta_Cof.Councillor;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class CouncilPanel extends JPanel {
	private JLabel c1= new JLabel();
	private JLabel c2=new JLabel();
	private JLabel c3=new JLabel();
	private JLabel c4=new JLabel();
	private ImageIcon black= new ImageIcon(CouncilPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/cblack.png"));
	private ImageIcon white= new ImageIcon(CouncilPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/cwhite.png"));
	private ImageIcon purple= new ImageIcon(CouncilPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/cpurple.png"));
	private ImageIcon pink= new ImageIcon(CouncilPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/cpink.png"));
	private ImageIcon orange= new ImageIcon(CouncilPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/corange.png"));
	private ImageIcon celestial= new ImageIcon(CouncilPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/ccelestial.png"));
	/**
	 * Create the panel.
	 */
	public CouncilPanel(Council c) {
		setPreferredSize(new Dimension(170, 50));
		updateCouncil(c.getCouncil().get(0),c1);
		add(c1);
		updateCouncil(c.getCouncil().get(1),c2);
		add(c2);
		updateCouncil(c.getCouncil().get(2),c3);
		add(c3);
		updateCouncil(c.getCouncil().get(3),c4);
		add(c4);
	}
	
	public void updateCouncil(Councillor c, JLabel lb){
		
			switch(c.getColour().toString()){
			case "BLACK":lb.setIcon(black); break;
			case "CELESTIAL":lb.setIcon(celestial);break;
			case "ORANGE": lb.setIcon(orange);break;
			case "PINK": lb.setIcon(pink);break;
			case "PURPLE": lb.setIcon(purple); break;
			default: lb.setIcon(white);
			
			}
			
		
			
	}
	public void update(Council c){
		updateCouncil(c.getCouncil().get(0),c1);
		add(c1);
		updateCouncil(c.getCouncil().get(1),c2);
		add(c2);
		updateCouncil(c.getCouncil().get(2),c3);
		add(c3);
		updateCouncil(c.getCouncil().get(3),c4);
		add(c4);
	
	}
}
