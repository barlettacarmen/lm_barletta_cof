package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.Dimension;
import java.awt.Font;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import java.awt.Component;
import javax.swing.JLayeredPane;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

import it.polimi.ingsw.LM_Barletta_Cof.ColourCard;
import it.polimi.ingsw.LM_Barletta_Cof.KingCard;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.NobilityCell;

import java.awt.Color;
import it.polimi.ingsw.LM_Barletta_Cof.RegionCard;
import it.polimi.ingsw.LM_Barletta_Cof.networking.Server;
public class LowerPanel extends JPanel {
	private static final Logger log= Logger.getLogger( LowerPanel.class.getName() );
	private BackgroundPane background_1;
	private KingCard[] kingCard; 
	private ColourCard[] colourCard;
	private RegionCard[] regionCard=new RegionCard[3];
	private Map m;
	CouncillorPoolPanel pool;
	CouncilPanel cp;
	JPanel[] bonusCards= new JPanel[7];
	JLabel[] bonusLabel=new JLabel[7];
	NobilityCell[] nobility;
	NobilityTrackPanel nobilityTrack;
	/**
	 * Create the panel.
	 */
	public LowerPanel(Map map) {
		this.m=map;
		this.kingCard=m.getKingCard();
		this.colourCard=m.getColourCard();
		this.nobility=new NobilityCell[21];
		this.nobility=m.getNobilityTrack();
		for(int i=0;i<3;i++){
			regionCard[i]=m.getRegions()[i].getRegionCard();
		}
			
		setPreferredSize(new Dimension(1200, 230));
		setAlignmentY(Component.TOP_ALIGNMENT);
		setAlignmentX(Component.LEFT_ALIGNMENT);
        try {
       	 background_1 = new BackgroundPane();
       	 background_1.setAlignmentY(Component.TOP_ALIGNMENT);
       	 background_1.setAlignmentX(0.0f);
       	 background_1.setPreferredSize(new Dimension(1200, 200));
			background_1.setBackground( ImageIO.read(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/Polish_magnates_1576-1586.PNG")));
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
		
		add(background_1);
		
		JPanel panel_3 = new JPanel();
		panel_3.setOpaque(false);
		panel_3.setPreferredSize(new Dimension(740, 220));
		background_1.add(panel_3);
		
		JPanel panel = new JPanel();
		panel_3.add(panel);
		panel.setOpaque(false);
		panel.setPreferredSize(new Dimension(600, 120));
		
		JLabel lblNobilityTrack = new JLabel("Nobility Track");
		lblNobilityTrack.setHorizontalAlignment(SwingConstants.CENTER);
		lblNobilityTrack.setPreferredSize(new Dimension(580, 14));
		panel.add(lblNobilityTrack);
		
		nobilityTrack= new NobilityTrackPanel(this.nobility);
	//	nobilityTrack.setPreferredSize(new Dimension(600, 80));
		nobilityTrack.setBounds(589, 100, 600, 80);
		panel.add(nobilityTrack);
		
		JPanel panel_1 = new JPanel();
		panel_3.add(panel_1);
		panel_1.setOpaque(false);
		panel_1.setPreferredSize(new Dimension(730, 90));
		
		JLabel lblCouncillorsPool = new JLabel("Councillors pool");
		lblCouncillorsPool.setPreferredSize(new Dimension(250, 14));
		lblCouncillorsPool.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblCouncillorsPool);
		
		JLabel lblBonusCards = new JLabel("Bonus Cards");
		lblBonusCards.setHorizontalAlignment(SwingConstants.CENTER);
		lblBonusCards.setPreferredSize(new Dimension(370, 14));
		panel_1.add(lblBonusCards);
		
		pool=new CouncillorPoolPanel(m.getCouncillorPool());
		panel_1.add(pool);
		
		for(int k=0;k<7;k++){
			bonusCards[k]= new JPanel();
			if(k<4){
				bonusLabel[k]= new JLabel(this.colourCard[k].getBonus().getVictoryPoints()+"VP");
				String colour=this.colourCard[k].getColour().toUpperCase();
				switch (colour){
				case "GREY": bonusCards[k].setBackground(Color.GRAY); break;
				case "ORANGE": bonusCards[k].setBackground(new Color(250,160,0)); break;
				case "YELLOW": bonusCards[k].setBackground(Color.YELLOW); break;
				default: {bonusCards[k].setBackground(Color.BLUE); 
				}
			}
			}
			else{
				JLabel region=new JLabel("Region "+(k-3));
				bonusCards[k].add(region);
				bonusLabel[k]= new JLabel(this.regionCard[k-4].getBonus().getVictoryPoints()+"VP");
			}
			bonusLabel[k].setOpaque(false);
			bonusCards[k].setPreferredSize(new Dimension(47, 47));
			bonusCards[k].add(bonusLabel[k]);
			panel_1.add(bonusCards[k]);
		
		}
		
		JLayeredPane layeredPane = new JLayeredPane();
		background_1.add(layeredPane);
		
		JPanel panel_2 = new JPanel();
		panel_2.setOpaque(false);
		panel_2.setPreferredSize(new Dimension(430, 230));
		background_1.add(panel_2);
		
		JLabel lblKingsCouncil = new JLabel("King's Council");
		panel_2.add(lblKingsCouncil);
		
		cp= new CouncilPanel(m.getKingsCouncil());
		panel_2.add(cp);
		
		JLabel lblKingsCards = new JLabel("King's cards");
		lblKingsCards.setHorizontalAlignment(SwingConstants.CENTER);
		lblKingsCards.setPreferredSize(new Dimension(440, 14));
		lblKingsCards.setForeground(Color.RED);
		panel_2.add(lblKingsCards);
		
		JPanel panel_8 = new JPanel();
		panel_8.setBackground(UIManager.getColor("Button.select"));
		panel_8.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_8.setPreferredSize(new Dimension(80, 80));
		panel_2.add(panel_8);
		panel_8.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel(m.getKingCard()[0].getValourOfBonus()+" VP");
		lblNewLabel_1.setBounds(20, 50, 43, 18);
		lblNewLabel_1.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		panel_8.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("1");
		lblNewLabel.setBounds(20, 6, 43, 32);
		panel_8.add(lblNewLabel);
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		lblNewLabel.setIcon(new ImageIcon(LowerPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/ crown.png")));
	
		
		JPanel panel_9 = new JPanel();
		panel_9.setBackground(UIManager.getColor("Button.select"));
		panel_9.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_9.setPreferredSize(new Dimension(80, 80));
		panel_2.add(panel_9);
		panel_9.setLayout(null);
		
		JLabel lblNewLabel_61 = new JLabel(m.getKingCard()[1].getValourOfBonus()+" VP");
		lblNewLabel_61.setBounds(20, 50, 43, 18);
		lblNewLabel_61.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		panel_9.add(lblNewLabel_61);
		
		JLabel lblNewLabel51 = new JLabel("2");
		lblNewLabel51.setBounds(20, 6, 43, 32);
		panel_9.add(lblNewLabel51);
		lblNewLabel51.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel51.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		lblNewLabel51.setIcon(new ImageIcon(LowerPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/ crown.png")));
		
		
		
		JPanel panel_10 = new JPanel();
		panel_10.setBackground(UIManager.getColor("Button.select"));
		panel_10.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_10.setPreferredSize(new Dimension(80, 80));
		panel_2.add(panel_10);
		panel_10.setLayout(null);
		
		JLabel lblNewLabel_67 = new JLabel(m.getKingCard()[2].getValourOfBonus()+" VP");
		lblNewLabel_67.setBounds(20, 50, 43, 18);
		lblNewLabel_67.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		panel_10.add(lblNewLabel_67);
		
		JLabel lblNewLabel55 = new JLabel("3");
		lblNewLabel55.setBounds(20, 6, 43, 32);
		panel_10.add(lblNewLabel55);
		lblNewLabel55.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel55.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		lblNewLabel55.setIcon(new ImageIcon(LowerPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/ crown.png")));
		
		JPanel panel_11 = new JPanel();
		panel_11.setBackground(UIManager.getColor("Button.select"));
		panel_11.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_11.setPreferredSize(new Dimension(80, 80));
		panel_2.add(panel_11);
		panel_11.setLayout(null);
		
		JLabel lblNewLabel_7 = new JLabel(m.getKingCard()[3].getValourOfBonus()+" VP");
		lblNewLabel_7.setBounds(20, 50, 43, 18);
		lblNewLabel_7.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		panel_11.add(lblNewLabel_7);
		
		JLabel lblNewLabel23 = new JLabel("4");
		lblNewLabel23.setBounds(20, 6, 43, 32);
		panel_11.add(lblNewLabel23);
		lblNewLabel23.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel23.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		lblNewLabel23.setIcon(new ImageIcon(LowerPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/ crown.png")));
		
		JPanel panel_12 = new JPanel();
		panel_12.setBackground(UIManager.getColor("Button.select"));
		panel_12.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_12.setPreferredSize(new Dimension(80, 80));
		panel_2.add(panel_12);
		panel_12.setLayout(null);
		
		JLabel lblNewLabel_6 = new JLabel(m.getKingCard()[4].getValourOfBonus()+" VP");
		lblNewLabel_6.setBounds(20, 50, 43, 18);
		lblNewLabel_6.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		panel_12.add(lblNewLabel_6);
		
		JLabel lblNewLabel5 = new JLabel("5");
		lblNewLabel5.setBounds(20, 6, 43, 32);
		panel_12.add(lblNewLabel5);
		lblNewLabel5.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel5.setFont(new Font("Microsoft Tai Le", Font.PLAIN, 13));
		lblNewLabel5.setIcon(new ImageIcon(LowerPanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/ crown.png")));

		
	}
	public void update(Map m){
		this.m=m;
		this.nobility=m.getNobilityTrack();
		cp.update(m.getKingsCouncil());
		pool.update(m.getCouncillorPool());
		nobilityTrack.update(nobility);
		for(int i=0;i<7;i++){
			if(i<4){
			this.bonusCards[i].setVisible(m.getColourCard()[i].isAvailable());
			this.bonusLabel[i].setText(m.getColourCard()[i].getBonus().getVictoryPoints()+"");
			}
			else{
				this.bonusCards[i].setVisible(m.getRegions()[i-4].getRegionCard().isAvailable());
				this.bonusLabel[i].setText(m.getRegions()[i-4].getRegionCard().getBonus().getVictoryPoints()+"");
			}
		}
	}
}
