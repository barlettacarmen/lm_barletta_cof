package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import it.polimi.ingsw.LM_Barletta_Cof.City;
import it.polimi.ingsw.LM_Barletta_Cof.CliGui;
import it.polimi.ingsw.LM_Barletta_Cof.Configuration;
import it.polimi.ingsw.LM_Barletta_Cof.Councillor;
import it.polimi.ingsw.LM_Barletta_Cof.CouncillorPool;
import it.polimi.ingsw.LM_Barletta_Cof.Emporium;
import it.polimi.ingsw.LM_Barletta_Cof.Exchange;
import it.polimi.ingsw.LM_Barletta_Cof.Item;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.Player;
import it.polimi.ingsw.LM_Barletta_Cof.PoliticsCard;
import it.polimi.ingsw.LM_Barletta_Cof.PoliticsColours;
import it.polimi.ingsw.LM_Barletta_Cof.Region;
import it.polimi.ingsw.LM_Barletta_Cof.networking.CliInterface;
import it.polimi.ingsw.LM_Barletta_Cof.networking.CliSocketHandler;

public class Gui implements CliGui, Serializable{
	private static final Logger log= Logger.getLogger( Gui.class.getName() );
	transient GamePanel gamePanel;
	transient Icon question= new ImageIcon(Gui.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/question.png"));
	transient Icon exclamation= new ImageIcon(Gui.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/es.png"));
	private ArrayList<Exchange> supportList=new ArrayList<Exchange>();
	private ArrayList<Item> supportListItem=new ArrayList<Item>();
	
//	main di prova 
public static void main(String[] args) {
	Player[] pl= new Player[2];
	pl[0]=new Player(1);
	pl[1]=new Player(2);
	pl[0].setUsername("Carmen");
	pl[1].setUsername("Giovanna");
	Configuration config = new Configuration("/Users/carmen/Desktop/gameConfigurationFiles/file_config1.txt", 2);
	Map m= new Map(pl,config);
	pl[0].setPermitCard(new PermitCard(m.getRegions()[1],config));	
	pl[0].setPermitCard(new PermitCard(m.getRegions()[1],config));	
	pl[0].setPermitCard(new PermitCard(m.getRegions()[1],config));	
	GamePanel gp=new GamePanel(m,1);
//	gp.setup();
	Gui gs=new Gui(gp);
	//gs.printString("Ciao");
//	supportList=new ArrayList<Exchange>();
//	supportList.add(new Exchange(pl[0],pl[0].getPermitCard().get(0), "Juvelar"));
//	supportList.add(new Exchange(pl[0],pl[0].getPermitCard().get(1), "Juvelar"));
//	supportList.add(new Exchange(pl[0],pl[0].getPermitCard().get(2), "Juvelar"));
//	supportList.add(new Exchange(pl[0],pl[0].getPoliticsCard().get(0), PoliticsColours.BLACK));
//	gs.askForExchangeToChoose();
//	gs.askForPermitCard(m.getRegions()[2].getPermitsList());
//	gs.wantToPerformQuickAction();
//	gs.askForTypeOfAction();
//	gs.askForTypeOfMainAction();
	
    }

	public Gui(GamePanel gamePanel){
		this.gamePanel=gamePanel;
	}
		
	
	public boolean wantToPerformQuickAction() {
	
		int n = JOptionPane.showConfirmDialog(
			    gamePanel.getFrame(),
			    "Do you want to perform a quick action before the main action?",
			    "Starting your turn",
			    JOptionPane.YES_NO_OPTION);
	if(n==0)
		return true;
	return false;

}
	public int askForTipeOfAction(){
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Engage an assistant", "Change the permit cards of a region", 
				"Send an assistant to elect a councillor", "Perform an additional main action"};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a quick action to perform",
		                    "Quick Action",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Engage an assistant");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Engage an assistant": n=1;break;
			case "Change the permit cards of a region": n=2;break;
			case "Send an assistant to elect a councillor":n=3;break;
			default: n=4;
			
			}
			flag=false;   	
		    
		}
	}
		return n;
	}
	
	public int askForTipeOfMainAction(){
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Elect a councillor", "Build an emporium with the help of the king", 
				"Build an emporium with a permit card", "Acquire a permit card"};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a main action to perform",
		                    "Main Action",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Elect a councillor");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Elect a councillor": n=1;break;
			case "Build an emporium with the help of the king": n=2;break;
			case "Build an emporium with a permit card":n=3;break;
			default: n=4;
			
			}
			flag=false;   	
		    
		}
	}
		return n;
	}
	
//	public  String askForColorOfCouncillor() {
//		printString("Type the colour you want to pick up");
//		return getString();
//		
//	}
	
	public  int askForCouncil() {
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Sea region's council", "Hills region's Council","Mountains region's council",  
				"King's council"};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Select the council",
		                    "Council",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Sea region's council");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Sea region's council": n=1;break;
			case "Hills region's Council": n=2;break;
			case "Mountains region's council":n=3;break;
			default: n=4;
			
			}
			flag=false;   	
		    
		}
	}
		return n;
		
	}
	public  int askForRegion() {
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Sea region", "Hills region","Mountains region"};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Select the region",
		                    "Region",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Sea region");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Sea region": n=1;break;
			case "Hills region": n=2;break;
			default:n=3;break;
			
			}
			flag=false;   	
		    
		}
	}
		return n;
		
	}
	
	
	public  int askForPermitCard(ArrayList<PermitCard> permits) {
		
		PermitCardPanel p= new PermitCardPanel();
		int answer=-1;
		 p.panelCheckBoxOfPermitCard(permits);
		while(p.getChoosen()==-1)
			try {
				Thread.sleep(200);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				log.log( Level.WARNING, e.toString(), e );
			}
		answer=p.getChoosen();
		return answer;

	}
	
	public  void printPermitCardList (ArrayList<PermitCard> a) {
		JPanel panel= PermitCardPanel.panelOfPermitCard(a);
		JFrame frame=new JFrame("Permit Cards");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setVisible(true);
		frame.pack();
		
	}


	public  City askForCity(Region r) {		
	City s=null;
	int selected;
	boolean flag=true;
	Object[] possibilities= new Object[r.getCities().size()];
	for(int i=0;i<r.getCities().size();i++){
		possibilities[i]=r.getCities().get(i).getName();
	}
	while(flag){
	String v = (String)JOptionPane.showInputDialog(
	                    this.gamePanel.getFrame(),
	                    "Choose a city","Cities:",
	                    JOptionPane.PLAIN_MESSAGE,
	                    question,
	                    possibilities,
	                    possibilities[0]);

	//If a string was returned, say so.
	if ((v != null)) {
		for(selected=0;selected<r.getCities().size()&&v!=r.getCities().get(selected).getName();selected++){	
		}
		flag=false;
		s=r.getCities().get(selected);
	    
	}
}
	return s;
		
	}
	@Override
	public  int askForNumberOfPoliticsCard() {
		int result=-1;
		boolean stop=false;
		do{
		try{
		result=Integer.parseInt(JOptionPane.showInputDialog("How many politics cards do you want to use?"));
		stop=true;
		}catch(Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}
		}while(!stop);
		return result;
	}	
	@Override
	public  int askForMulticolourCardNumber() {
		int result=-1;
		boolean stop=false;
		do{
		try{
		result=Integer.parseInt(JOptionPane.showInputDialog("How many multicolour cards do you want to use?"));
		stop=true;
		}catch(Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}
		}while(!stop);
		return result;
	}
	public  City askForNextCity(City c) {
		City s=null;
		int selected;
		boolean flag=true;
		Object[] possibilities= new Object[c.getNextCities().size()];
		for(int i=0;i<c.getNextCities().size();i++){
			possibilities[i]=c.getNextCities().get(i).getName();
		}
		while(flag){
		String v = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a city from the following","Cities:",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    possibilities[0]);

		//If a string was returned, say so.
		if ((v != null)) {
			for(selected=0;selected<c.getNextCities().size()&&v!=c.getNextCities().get(selected).getName();selected++){	
			}
			flag=false;
			s=c.getNextCities().get(selected);
		    
		}
	}
		return s;
	
	}
	
	public  boolean askIfFinalMove() {
		int n = JOptionPane.showConfirmDialog(
			    gamePanel.getFrame(),
			    "Is this the city where you want the king to be?","",
			    JOptionPane.YES_NO_OPTION);
	if(n==0)
		return true;
	return false;
	}
	public  String askForCityToColonize(ArrayList<String> list) {
		String s="";
		boolean flag=true;
		Object[] possibilities= new Object[list.size()];
		for(int i=0;i<list.size();i++){
			possibilities[i]=list.get(i);
		}
		while(flag){
		String v = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a city to colonize","Cities:",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    possibilities[0]);

		//If a string was returned, say so.
		if ((v != null)) {
			flag=false;
			s=v;
		    
		}
	}
		return s;
	}
	
	public Region choosenRegion(Map map){//returns the selected region
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Sea Reagion", 
				"Hills Region","Mountains Region"
				};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a region","Regions:",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Sea Region");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Sea Region": n=0;break;
			case "Hills Region":n=1;break;
			default: n=2;
			
			}
			flag=false;   	
		    
		}
	}
		return map.getRegions()[n];
	}
	
	public int choosenPermitCard(Region r){//to be changed with permit card panel
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Permit card 1", 
				"Permit card 2"
				};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a permit card","Permit cards:",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Permit card 1");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Permit card 1": n=1;break;
			default: n=2;
			
			}
			flag=false;   	
		    
		}
	}
		return n-1;
	}

	@Override
	public CliInterface getCliInterface() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printString(String s) {//modificare icon
		JOptionPane.showMessageDialog(null,s,"Message", JOptionPane.INFORMATION_MESSAGE,exclamation);
		
	}

	

	@Override
	public String getString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public char getChar() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String askForLogin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void printPlayerStatus(Player p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void printPlayersCardList(Player p) {
			PoliticsCardPanel dialog = new PoliticsCardPanel(p.getPoliticsCard());
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		
	}

	@Override
	public String askForColorOfCouncillor() {
		String colour="";
		boolean flag=true;
		Object[] possibilities = {"CELESTIAL", 
				"WHITE","BLACK","ORANGE","PINK","PURPLE","JOLLY"
				};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a colour","Colours:",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "WHITE");

		//If a string was returned, say so.
		if ((colour != null) && (s.length() > 0)) {
			flag=false;   	 
		}
	}
		return colour;
	
	}

	@Override
	public void printPoolOfCouncillors(CouncillorPool pool) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public City askForColonizedCity(ArrayList<Emporium> list) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int askForPoliticsCard(ArrayList<PoliticsCard> cards) {
		int selected=0;
		boolean flag=true;
		Object[] possibilities= new Object[cards.size()];
		for(int i=0;i<cards.size();i++){
			if(!cards.get(i).isUsed())
			possibilities[i]=cards.get(i).getPoliticsColour().toString();
		}
		while(flag){
		String v = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose a politics card","Politics cards:",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    possibilities[0]);

		//If a string was returned, say so.
		if ((v != null)) {
			for(selected=0;selected<cards.size()&&v!=cards.get(selected).getPoliticsColour().toString();selected++){	
			}
			flag=false;
		}
	}
		return selected;
	}

	@Override
	public int askForTypeOfCard() {
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Permit card", "Politics card"};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Choose the type of the card",
		                    "Card Types",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Permit card");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Permit card": n=1;break;
			default: n=2;
			
			}
			flag=false;   	
		    
		}
	}
		return n;

	}

	@Override
	public int askForPrice() {
		int result=-1;
		boolean stop=false;
		do{
		try{
		result=Integer.parseInt(JOptionPane.showInputDialog("Insert a price for the item"));
		stop=true;
		}catch(Exception e){
			log.log( Level.WARNING, e.toString(), e );
		}
		}while(!stop);
		return result;
	}

	@Override
	public void printItemList(ArrayList<Item> itemList) {
		for(Item i:itemList)
			supportListItem.add(i);
		
	}

	@Override //in realtà è il metodo del market che chiede cosa fare sell
	public int getInteger() {
			int n=0;
			boolean flag=true;
			Object[] possibilities = {"Sell an item", "Add an Echange", "Skip"};
			while(flag){
			String s = (String)JOptionPane.showInputDialog(
			                    this.gamePanel.getFrame(),
			                    "Select the type of action",
			                    "Market",
			                    JOptionPane.PLAIN_MESSAGE,
			                    question,
			                    possibilities,
			                    "Sell an item");

			//If a string was returned, say so.
			if ((s != null) && (s.length() > 0)) {
				switch(s){
				case "Sell an item": n=1;break;
				case "Add an Echange": n=2;break;
				default:n=3;break;
				
				}
				flag=false;   	
			    
			}
		}
			return n;
			
		
	}
	
	public int selectBuyAction() {
		int n=0;
		boolean flag=true;
		Object[] possibilities = {"Buy an item", "Accept an Echange", "Exit the market"};
		while(flag){
		String s = (String)JOptionPane.showInputDialog(
		                    this.gamePanel.getFrame(),
		                    "Select the type of buy action",
		                    "Market",
		                    JOptionPane.PLAIN_MESSAGE,
		                    question,
		                    possibilities,
		                    "Buy an item");

		//If a string was returned, say so.
		if ((s != null) && (s.length() > 0)) {
			switch(s){
			case "Buy an item": n=1;break;
			case "Accept an Echange": n=2;break;
			default:n=3;break;
			
			}
			flag=false;   	
		    
		}
	}
		return n;
		
	
}

	@Override	
	public  void  printExchangeList(ArrayList<Exchange> exchangeList) {
		for(Exchange e:exchangeList)
		this.supportList.add(e);
	}
	@Override
	public  int askForExchangeToChoose() {
		ExchangePanel p= new ExchangePanel();
		int answer=-1;
		 p.panelCheckBoxOfExchange(supportList,supportListItem);
		while(p.getChoosen()==-1)
			try {
				Thread.sleep(200);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				log.log( Level.WARNING, e.toString(), e );
			}
		answer=p.getChoosen();
		supportList=new ArrayList<Exchange>();
		return answer;

	}
	@Override	
	public  void printCouncillors(ArrayList<Councillor> list) {
	
	}
	@Override
	public  Councillor askForCouncillor(CouncillorPool list) {
		Councillor s=null;
		boolean flag=true;
		int selected=0;
		Object[] possibilities= new Object[list.getAvailableCouncillors().size()];
		for(int i=0;i<list.getAvailableCouncillors().size();i++){
			if(list.getAvailableCouncillors().get(i).isAvailable())
				possibilities[i]=list.getAvailableCouncillors().get(i).getColour().toString();
		}
		while(flag){
			String v =(String) 	JOptionPane.showInputDialog(
			             		this.gamePanel.getFrame(),
			             		"Choose a councillor from the CouncillorPool","Councillors:",
			             		JOptionPane.PLAIN_MESSAGE,
			             		question,
			             		possibilities,
			             		possibilities[0]);

		//If a string was returned, say so.
		if (v != null&&v.length()>0) {
			switch(v){
			case"BLACK": for(selected=0;selected<4;selected++)
							if(list.getAvailableCouncillors().get(selected).isAvailable())
								s=list.getAvailableCouncillors().get(selected); break;
			case"CELESTIAL":for(selected=4;selected<8;selected++)
							if(list.getAvailableCouncillors().get(selected).isAvailable())
								s=list.getAvailableCouncillors().get(selected); break;
			case"ORANGE":for(selected=8;selected<12;selected++)
						if(list.getAvailableCouncillors().get(selected).isAvailable())
						s=list.getAvailableCouncillors().get(selected); break;
			case"PINK":for(selected=12;selected<16;selected++)
						if(list.getAvailableCouncillors().get(selected).isAvailable())
								s=list.getAvailableCouncillors().get(selected); break;
			case"PURPLE":for(selected=16;selected<20;selected++)
						if(list.getAvailableCouncillors().get(selected).isAvailable())
							s=list.getAvailableCouncillors().get(selected); break;
			default:for(selected=20;selected<24;selected++)
						if(list.getAvailableCouncillors().get(selected).isAvailable())
						s=list.getAvailableCouncillors().get(selected); break;
		}
	}	flag=false;}
	
		return s;}

	@Override
	public int askForItemToChoose() {
		ExchangePanel p= new ExchangePanel();
		int answer=-1;
		if(!supportListItem.isEmpty()){
		 p.panelCheckBoxOfExchange(supportList,supportListItem);
		while(p.getChoosen()==-1)
			try {
				Thread.sleep(200);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				log.log( Level.WARNING, e.toString(), e );
			}
		answer=p.getChoosen();
		supportListItem=new ArrayList<Item>();
		}
		return answer;
	}



}