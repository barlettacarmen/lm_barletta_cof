package it.polimi.ingsw.LM_Barletta_Cof.views;

import javax.swing.JPanel;
import javax.swing.JRadioButton;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;

import it.polimi.ingsw.LM_Barletta_Cof.Exchange;
import it.polimi.ingsw.LM_Barletta_Cof.Item;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.PoliticsCard;

public class ExchangePanel extends JPanel {
	private Exchange e;
	private Item item;
	JButton enter;
	private int choosen=-1;
	public  PermitCard pc;
	public JTextPane textArea;
	public BonusPanel panel;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private ArrayList<JRadioButton> radio;
	private JFrame frame;
	/**
	 * Create the panel.
	 */
	public ExchangePanel() {}
	public ExchangePanel(Exchange e,Item i) {
		this.e=e;
		this.item=i;
		ArrayList<PoliticsCard> card;
		ArrayList<PermitCard> pCard;
		setPreferredSize(new Dimension(180, 210));
		
		JLabel lblOfferedCard = new JLabel("Offered Card");
		add(lblOfferedCard);
		
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		panel.setPreferredSize(new Dimension(170, 120));
		setBorder(new LineBorder(new Color(184, 54, 231), 2, true));
		setBackground(new Color(110, 200, 210));
		add(panel);
		if(e!=null){
		if(e.getCardOffered()!=null){
			panel.add(new  PermitCardPanel(e.getCardOffered()));
		}
		else{
			panel.add(PoliticsCardPanel.insertAPoliticsCard(e.getPoliticsCardOffered()));
		}
		}
		else if(item!=null){
			if(item.getPermitCard()!=null){
				panel.add(new  PermitCardPanel(i.getPermitCard()));
			}
			else{
				panel.add(PoliticsCardPanel.insertAPoliticsCard(item.getPoliticsCard()));
			}
		}
		JLabel lblRequest = new JLabel("Request");
		add(lblRequest);
		
		JPanel panel_1 = new JPanel();
		panel_1.setOpaque(false);
		panel_1.setPreferredSize(new Dimension(170, 30));
		add(panel_1);
		
		JTextPane textPane = new JTextPane();
		textPane.setPreferredSize(new Dimension(170, 20));
		textPane.setEditable(false);
		if(e!=null){
			if(e.getCardRequested()!=null)
				textPane.setText(e.getCardRequested());
			else
				textPane.setText(e.getPoliticsCardRequested().toString());

		}
		else if(item!=null){
			textPane.setText(item.getPrice()+"");
		}
		panel_1.add(textPane);

	}
	
	public void panelCheckBoxOfExchange(ArrayList<Exchange> a,ArrayList<Item>b){
		 
		 int size;
		if(!a.isEmpty())
			size=a.size();
		else
			size=b.size();
		
		if(size>0){
			frame= new JFrame();
			 frame.setBounds(450, 250, 450, 300);
			 JPanel panel= new JPanel();
			 radio= new ArrayList<JRadioButton>(size);
		for(int i=0; i<size;i++){
			ExchangePanel pcpanel=null;
			if(!a.isEmpty()){
				pcpanel= new ExchangePanel(a.get(i),null);
			}
			if(!b.isEmpty()){
				pcpanel= new ExchangePanel(null,b.get(i));
			}
			if(pcpanel!=null){
				pcpanel.setVisible(true);
				panel.add(pcpanel);
			}
			 radio.add(new JRadioButton(""+(i+1)));
			 radio.get(i).setVisible(true);
			 buttonGroup.add(radio.get(i));
			 panel.add(radio.get(i));
		}
		radio.get(0).setSelected(true);
		enter= new JButton("Enter");
		enter.setVisible(true);
		panel.add(enter);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		frame.setVisible(true);
		frame.pack();
		
		boolean flag=false;
		
			flag=createEvents();
		}
	
		
	}
	
	
	
public boolean createEvents(){
		enter.addActionListener(new ActionListener() {
		
		
			public void actionPerformed(ActionEvent e) {
			
				
				for(int i=0; i<radio.size();i++)
					if(radio.get(i).isSelected()){
						choosen=i+1;
						}
				frame.dispose();
			}			
		});
		return true;
		
	}

	public int getChoosen() {
		return choosen;
	}
}
