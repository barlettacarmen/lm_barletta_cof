package it.polimi.ingsw.LM_Barletta_Cof.views;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import it.polimi.ingsw.LM_Barletta_Cof.Configuration;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.Player;
import it.polimi.ingsw.LM_Barletta_Cof.networking.Server;
public class GamePanel {
	private static final Logger log= Logger.getLogger( GamePanel.class.getName() );
	private JFrame frame;

	private JLabel map;
	private JButton b[]={new JButton("Coin"), new JButton("Victory"),
            new JButton("Nobility"), new JButton("Assistants"),new JButton("Emporium") ,new JButton("Permit"), new JButton("Politics")};
	private ImageIcon coin;
	private ImageIcon victory;
	private ImageIcon nobility;
	private ImageIcon permit;
	private ImageIcon politics;
	private ImageIcon assistants;
	private ImageIcon emporium;
	private Map m;
	private int playerId;
	MapPanel mapBoard;
	LeftSpace ls;
	LowerPanel lp;

	public GamePanel(Map m, int playerId){
		frame= new JFrame("The Council of Four---> "+ m.getPlayerUsername(playerId));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		map= new JLabel();
		this.m=m;
		this.playerId=playerId;
		
		
	}
	
	public void setup(){

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ls= new LeftSpace(m.getPlayerlist(), playerId);
					ls.setVisible(true);
					frame.add(ls,BorderLayout.EAST);
					lp= new LowerPanel(m);
					lp.setVisible(true);
					frame.add(lp,BorderLayout.SOUTH);
					mapBoard=new MapPanel(m,m.getPlayerlist()[playerId-1]);
					frame.add(mapBoard, BorderLayout.WEST);
					frame.setSize(new Dimension(1200, 690));
					frame.setVisible(true);
					frame.pack();
		
		
	} catch (Exception e) {
		log.log( Level.WARNING, e.toString(), e );
	}
}
});
	}
	

	public JFrame getFrame() {
		return frame;
	}
	
	public void update(Map m){
		this.m=m;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mapBoard.update(m);
					ls.update(m.getPlayerlist());
					lp.update(m);
				} catch (Exception e) {
					log.log( Level.WARNING, e.toString(), e );
				}
			}
		});
		
		
	}


}
