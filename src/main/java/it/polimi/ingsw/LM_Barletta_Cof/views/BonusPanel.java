package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.polimi.ingsw.LM_Barletta_Cof.AssistantBonus;
import it.polimi.ingsw.LM_Barletta_Cof.Bonus;
import it.polimi.ingsw.LM_Barletta_Cof.BonusFromColonizedCityBonus;
import it.polimi.ingsw.LM_Barletta_Cof.BonusFromYourPermitCardBonus;
import it.polimi.ingsw.LM_Barletta_Cof.CoinBonus;
import it.polimi.ingsw.LM_Barletta_Cof.Configuration;
import it.polimi.ingsw.LM_Barletta_Cof.Constants;
import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.NobilityBonus;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCardGratisBonus;
import it.polimi.ingsw.LM_Barletta_Cof.Player;
import it.polimi.ingsw.LM_Barletta_Cof.PoliticsCardBonus;
import it.polimi.ingsw.LM_Barletta_Cof.StarBonus;
import it.polimi.ingsw.LM_Barletta_Cof.VictoryPointBonus;

public class BonusPanel extends JPanel {
	
	private ArrayList<Bonus> bonus;
	private ArrayList<JLabel> lb;
	ImageIcon	c= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/coinpiccolo.png"));
	 ImageIcon v= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/victorypiccolo.png"));
	 ImageIcon n= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/nextpiccolo.png"));
	 ImageIcon star= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/star.png"));
	 ImageIcon polcardb= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/polcardbonus.png"));
	 ImageIcon a = new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/helpingpiccolo.png"));
	 ImageIcon bfypc= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/icon.png"));
	 ImageIcon bcgb= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/permitcardgratis.png"));
	 ImageIcon bonfromcolcity= new ImageIcon(GamePanel.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/bonusfromcoloncity.png"));
	/**
	 * Create the panel.
	 */
	 public static void main(String[] args) {
			Player[] pl= new Player[2];
			pl[0]=new Player(1);
			pl[1]=new Player(2);
			pl[0].setUsername("Carmen");
			pl[1].setUsername("Giovanna");
			Configuration config = new Configuration("/Users/carmen/Desktop/gameConfigurationFiles/file_config1.txt", 2);
			Map m= new Map(pl,config);
			BonusPanel bp=new BonusPanel(new PermitCard(m.getRegions()[0],config).getBonus());	
			bp.setVisible(true);
			JFrame frame= new JFrame();
			frame.setSize(400, 400);
			frame.add(bp);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    }
	 
	 public BonusPanel(){
		 JLabel label= new JLabel();
		 setPreferredSize(new Dimension(32, 32));
		 add(label);
	 }
	 
	public BonusPanel(ArrayList<Bonus> bonus) {
		this.bonus=bonus;
		this.lb= new ArrayList<JLabel>(bonus.size());
		setPreferredSize(new Dimension(300, 300));
		for(int i=0;i<bonus.size();i++){
			this.lb.add(new JLabel());
			this.lb.get(i).setSize(new Dimension(32,32));
			add(this.lb.get(i));
		}
		setBonus();

	}
	public void setBonus(){
		for(int i=0;i<bonus.size();i++){
			switch(bonus.get(i).getId()){
			case Constants.VICTORY_POINT_BONUS:
			{ this.lb.get(i).setIcon(v);
				VictoryPointBonus b= (VictoryPointBonus)bonus.get(i);
			  this.lb.get(i).setText(""+b.getVictoryPoints());
			  this.lb.get(i).setToolTipText("Victory point Bonus");
			  this.lb.get(i).createToolTip().setVisible(true);
			  add(this.lb.get(i));
			} break;
	
			case Constants.NOBILITY_POINT_BONUS:
			 {this.lb.get(i).setIcon(n);
			 this.lb.get(i).setToolTipText("Nobility point Bonus");
			  add(this.lb.get(i));
			 }break;
				
			case Constants.COIN_BONUS:
			{ this.lb.get(i).setIcon(c);
			CoinBonus b= (CoinBonus)bonus.get(i);
		  this.lb.get(i).setText(""+b.getCoin());
		  this.lb.get(i).setToolTipText("Coin Bonus");
		  add(this.lb.get(i));} break;
		
			case Constants.ASSISTANT_BONUS:
			{ this.lb.get(i).setIcon(a);
			AssistantBonus b= (AssistantBonus)bonus.get(i);
			this.lb.get(i).setText(""+b.getAssistants());	
			this.lb.get(i).setToolTipText("Assistant Bonus");
			add(this.lb.get(i));} break;
				
			case Constants.STAR_BONUS:
				{this.lb.get(i).setIcon(star);
				this.lb.get(i).setToolTipText("Extra Main Action Bonus");
				add(this.lb.get(i));
				} break;
				
			case Constants.POLITICS_CARD_BONUS:
				{this.lb.get(i).setIcon(polcardb);
				this.lb.get(i).setToolTipText("Politics Card Bonus");
				add(this.lb.get(i));
				}break;
				
			case Constants.BONUS_FROM_COLONIZED_CITY_BONUS:
				{this.lb.get(i).setIcon(bonfromcolcity); 
				this.lb.get(i).setToolTipText("Bonus From a Colonized City");
				add(this.lb.get(i));
				}break;
				
			case Constants.BONUS_FROM_YOUR_PERMIT_CARD_BONUS:
				{this.lb.get(i).setIcon(bfypc); 
				this.lb.get(i).setToolTipText("Bonus From one of your Permit Card");
				add(this.lb.get(i));
				}break;
			 
			//	Constants.PERMIT_CARD_GRATIS_BONUS:
			default:	{this.lb.get(i).setIcon(bcgb); 
						this.lb.get(i).setToolTipText("Free Permit Card Bonus");
						add(this.lb.get(i));}break;
					
			}
		}
	}

}
