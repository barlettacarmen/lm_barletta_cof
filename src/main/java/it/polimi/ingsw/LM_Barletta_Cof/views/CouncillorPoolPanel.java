package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.Dimension;

import javax.swing.JPanel;

import it.polimi.ingsw.LM_Barletta_Cof.Council;
import it.polimi.ingsw.LM_Barletta_Cof.Councillor;
import it.polimi.ingsw.LM_Barletta_Cof.CouncillorPool;

public class CouncillorPoolPanel extends JPanel {
	Council council1;
	Council council2;
	CouncilPanel[] cp= new CouncilPanel[2];
	/**
	 * Create the panel.
	 * Qua per aggiornare facciamo la new CouncillorPoolPanel(pool)
	 */ 
	
	public CouncillorPoolPanel(CouncillorPool pool) {
		setPreferredSize(new Dimension(350,50));
		int i=0;
		council1= new Council();
		council2= new Council();
		for(Councillor c: pool.getAvailableCouncillors()){
			if(c.isAvailable()&&i<4){
				council1.getCouncil().add(c);
				i++;}
			else if(c.isAvailable())
				council2.getCouncil().add(c);
		}
		cp[0]= new CouncilPanel(council1);
		cp[1]=new CouncilPanel(council2);
		add(cp[0]);
		add(cp[1]);
}
	public void update(CouncillorPool pool){
		int i=0;
		Council council1= new Council();
		Council council2= new Council();
		for(Councillor c: pool.getAvailableCouncillors()){
			if(c.isAvailable()&&i<4){
				council1.getCouncil().add(c);
				i++;}
			else if(c.isAvailable())
				council2.getCouncil().add(c);
		}
		cp[0].update(council1);
		cp[1].update(council2);
	}
}
