package it.polimi.ingsw.LM_Barletta_Cof.views;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import it.polimi.ingsw.LM_Barletta_Cof.Map;
import it.polimi.ingsw.LM_Barletta_Cof.PermitCard;
import it.polimi.ingsw.LM_Barletta_Cof.Player;
import it.polimi.ingsw.LM_Barletta_Cof.networking.Server;
import it.polimi.ingsw.LM_Barletta_Cof.Council;

public class MapPanel extends JPanel {
	private static final Logger log= Logger.getLogger( MapPanel.class.getName() );
	BackgroundPane map;
	static Map m;
	static Player p;
	transient BufferedImage i;
	static JButton permitSea;
	static JButton permitHills;
	static JButton permitMountains;
	static JButton[] bonusButtons;
	static JLabel[] emporiumsBuilt;
	static JCheckBox[] check;
	static CouncilPanel sea,hills,mountains;
	static int region,k,cont;
	/**
	 * Create the panel.
	 */
	public MapPanel(Map m,Player p) {
		this.m=m;
		this.p=p;
		setPreferredSize(new Dimension(700, 460));
        try {
       	 map = new BackgroundPane();
       	 map.setAlignmentX(0.0f);
       	 map.setPreferredSize(new Dimension(700, 460));
       	 i=BackgroundPane.getScaledInstanceToFill(ImageIO.read(LeftSpace.class.getResource("/it/polimi/ingsw/LM_Barletta_Cof/source/"+m.getMapId()+".jpg")),new Dimension(700,460));
			map.setBackground( i);
		
		} catch (IOException e) {
			log.log( Level.WARNING, e.toString(), e );
		}
        add(map);
        bonusButtons=new JButton[15];
        emporiumsBuilt=new JLabel[15];
        check=new JCheckBox[15];
        for(int i=0;i<15;i++){
        	 bonusButtons[i] = new JButton("B");
        	 bonusButtons[i].setToolTipText("City bonus");
        	 bonusButtons[i].setVisible(true);
        	 bonusButtons[i].setPreferredSize(new Dimension(60,20));
        	 emporiumsBuilt[i]=new JLabel("0");
        	 emporiumsBuilt[i].setPreferredSize(new Dimension(30, 20));
        	 emporiumsBuilt[i].setHorizontalAlignment(SwingConstants.CENTER);
        	 emporiumsBuilt[i].setForeground(new Color(255,255,255));
        	 check[i] = new JCheckBox("");
        	 check[i].setEnabled(false);
        	 check[i].setOpaque(false);
        }
        JPanel panel = new JPanel();
        panel.setOpaque(false);
        panel.setPreferredSize(new Dimension(225, 450));
        map.add(panel);
        
        JLayeredPane layeredPane_1 = new JLayeredPane();
        panel.add(layeredPane_1);
        
        JPanel panel_3 = new JPanel();
        panel_3.setOpaque(false);
        panel_3.setPreferredSize(new Dimension(220, 135));
        panel.add(panel_3);
        
        JPanel panel_7 = new JPanel();
        panel_7.setOpaque(false);
        panel_7.setPreferredSize(new Dimension(100, 110));
        panel_3.add(panel_7);  
        JPanel panel_33 = new JPanel();
        panel_33.setOpaque(false);
        panel_33.setPreferredSize(new Dimension(100, 30));
        panel_33.setMinimumSize(new Dimension(100, 80));
        panel_7.add(panel_33);
        
        JLayeredPane layeredPane_4 = new JLayeredPane();
        panel_7.add(layeredPane_4);
        panel_7.add(bonusButtons[0]);
        panel_7.add(check[0]);
        panel_7.add(emporiumsBuilt[0]);
        
        JPanel panel_1 = new JPanel();
        panel_1.setPreferredSize(new Dimension(100, 130));
        panel_1.setOpaque(false);
        panel_3.add(panel_1);
        
        JPanel panel_2 = new JPanel();
        panel_2.setPreferredSize(new Dimension(100, 60));
        panel_2.setOpaque(false);
        panel_2.setMinimumSize(new Dimension(100, 80));
        panel_1.add(panel_2);
        
        JLayeredPane layeredPane_2 = new JLayeredPane();
        panel_1.add(layeredPane_2);

        panel_1.add(bonusButtons[2]);
        panel_1.add(check[2]);
        panel_1.add(emporiumsBuilt[2]);
        
        JPanel panel_4 = new JPanel();
        panel_4.setOpaque(false);
        panel_4.setPreferredSize(new Dimension(220, 110));
        panel.add(panel_4);
        
        JPanel panel_8 = new JPanel();
        panel_8.setPreferredSize(new Dimension(110, 100));
        panel_8.setOpaque(false);
        panel_4.add(panel_8);
        
        JPanel panel_11 = new JPanel();
        panel_11.setPreferredSize(new Dimension(100, 30));
        panel_11.setOpaque(false);
        panel_11.setMinimumSize(new Dimension(100, 80));
        panel_8.add(panel_11);
        
        JLayeredPane layeredPane_3 = new JLayeredPane();
        panel_8.add(layeredPane_3);

        panel_8.add(bonusButtons[1]);
        panel_8.add(check[1]);
        panel_8.add(emporiumsBuilt[1]);
        
        JPanel panel_12 = new JPanel();
        panel_12.setPreferredSize(new Dimension(100, 105));
        panel_12.setOpaque(false);
        panel_4.add(panel_12);
        
        JPanel panel_13 = new JPanel();
        panel_13.setPreferredSize(new Dimension(100, 35));
        panel_13.setOpaque(false);
        panel_13.setMinimumSize(new Dimension(100, 80));
        panel_12.add(panel_13);
        
        JLayeredPane layeredPane_5 = new JLayeredPane();
        panel_12.add(layeredPane_5);

        panel_12.add(bonusButtons[3]);
        panel_12.add(check[3]);
        panel_12.add(emporiumsBuilt[3]);
        
        JPanel panel_5 = new JPanel();
        panel_5.setOpaque(false);
        panel_5.setPreferredSize(new Dimension(220, 90));
        panel.add(panel_5);
        
        JPanel panel_9 = new JPanel();
        panel_9.setPreferredSize(new Dimension(110, 110));
        panel_9.setOpaque(false);
        panel_5.add(panel_9);
        
        JPanel panel_10 = new JPanel();
        panel_10.setPreferredSize(new Dimension(100, 10));
        panel_10.setOpaque(false);
        panel_10.setMinimumSize(new Dimension(150, 80));
        panel_9.add(panel_10);
        
        JLayeredPane layeredPane_6 = new JLayeredPane();
        panel_9.add(layeredPane_6);

        panel_9.add(bonusButtons[4]);
        panel_9.add(check[4]);
        panel_9.add(emporiumsBuilt[4]);
        
        JPanel panel_14 = new JPanel();
        panel_14.setPreferredSize(new Dimension(100, 90));
        panel_14.setOpaque(false);
        panel_5.add(panel_14);
        
        JPanel panel_51 = new JPanel();
        panel_51.setPreferredSize(new Dimension(90, 20));
        panel_51.setOpaque(false);
        panel_14.add(panel_51);
        
        JLayeredPane layeredPane_7 = new JLayeredPane();
        panel_14.add(layeredPane_7);
        
        JPanel panel_6 = new JPanel();
        panel_6.setPreferredSize(new Dimension(230, 95));
        panel_6.setOpaque(false);
        panel.add(panel_6);
        
        JPanel panel_15 = new JPanel();
        panel_15.setPreferredSize(new Dimension(230, 25));
        panel_15.setOpaque(false);
        panel_6.add(panel_15);
        
        permitSea = new JButton("Permit cards of Sea Region");
        permitSea.setPreferredSize(new Dimension(220, 20));
        panel_15.add(permitSea);
        
        sea = new CouncilPanel(m.getRegions()[0].getCouncil());
        panel_6.add(sea);
		
		JPanel panel_16 = new JPanel();
		panel_16.setPreferredSize(new Dimension(230, 450));
		panel_16.setOpaque(false);
		map.add(panel_16);
		
		JLayeredPane layeredPane_8 = new JLayeredPane();
		panel_16.add(layeredPane_8);
		
		JPanel panel_17 = new JPanel();
		panel_17.setPreferredSize(new Dimension(230, 135));
		panel_17.setOpaque(false);
		panel_16.add(panel_17);
		
		JPanel panel_18 = new JPanel();
		panel_18.setPreferredSize(new Dimension(100, 110));
		panel_18.setOpaque(false);
		panel_17.add(panel_18);
		
		JPanel panel_19 = new JPanel();
		panel_19.setPreferredSize(new Dimension(100, 40));
		panel_19.setOpaque(false);
		panel_19.setMinimumSize(new Dimension(100, 80));
		panel_18.add(panel_19);
		
		JLayeredPane layeredPane_9 = new JLayeredPane();
		panel_18.add(layeredPane_9);
		
		panel_18.add(bonusButtons[5]);
		panel_18.add(check[5]);
		panel_18.add(emporiumsBuilt[5]);
		
		JPanel panel_20 = new JPanel();
		panel_20.setPreferredSize(new Dimension(100, 130));
		panel_20.setOpaque(false);
		panel_17.add(panel_20);
		
		JPanel panel_21 = new JPanel();
		panel_21.setPreferredSize(new Dimension(100, 60));
		panel_21.setOpaque(false);
		panel_21.setMinimumSize(new Dimension(100, 80));
		panel_20.add(panel_21);
		
		JLayeredPane layeredPane_10 = new JLayeredPane();
		panel_20.add(layeredPane_10);

		panel_20.add(bonusButtons[8]);
		panel_20.add(check[8]);
		panel_20.add(emporiumsBuilt[8]);
		
		JPanel panel_22 = new JPanel();
		panel_22.setPreferredSize(new Dimension(230, 110));
		panel_22.setOpaque(false);
		panel_16.add(panel_22);
		
		JPanel panel_23 = new JPanel();
		panel_23.setPreferredSize(new Dimension(110, 100));
		panel_23.setOpaque(false);
		panel_22.add(panel_23);
		
		JPanel panel_24 = new JPanel();
		panel_24.setPreferredSize(new Dimension(100, 30));
		panel_24.setOpaque(false);
		panel_24.setMinimumSize(new Dimension(100, 80));
		panel_23.add(panel_24);
		
		JLayeredPane layeredPane_11 = new JLayeredPane();
		panel_23.add(layeredPane_11);
		
		panel_23.add(bonusButtons[6]);
		panel_23.add(check[6]);
		panel_23.add(emporiumsBuilt[6]);
		
		JPanel panel_25 = new JPanel();
		panel_25.setPreferredSize(new Dimension(100, 105));
		panel_25.setOpaque(false);
		panel_22.add(panel_25);
		
		JPanel panel_26 = new JPanel();
		panel_26.setPreferredSize(new Dimension(100, 35));
		panel_26.setOpaque(false);
		panel_26.setMinimumSize(new Dimension(100, 80));
		panel_25.add(panel_26);
		
		JLayeredPane layeredPane_12 = new JLayeredPane();
		panel_25.add(layeredPane_12);

		panel_25.add(bonusButtons[9]);
		panel_25.add(check[9]);
		panel_25.add(emporiumsBuilt[9]);
		
		JPanel panel_27 = new JPanel();
		panel_27.setPreferredSize(new Dimension(230, 90));
		panel_27.setOpaque(false);
		panel_16.add(panel_27);
		
		JPanel panel_28 = new JPanel();
		panel_28.setPreferredSize(new Dimension(110, 110));
		panel_28.setOpaque(false);
		panel_27.add(panel_28);
		
		JPanel panel_29 = new JPanel();
		panel_29.setPreferredSize(new Dimension(100, 1));
		panel_29.setOpaque(false);
		panel_29.setMinimumSize(new Dimension(150, 80));
		panel_28.add(panel_29);
		
		JLayeredPane layeredPane_13 = new JLayeredPane();
		panel_28.add(layeredPane_13);

		panel_28.add(bonusButtons[7]);
		panel_28.add(check[7]);
		panel_28.add(emporiumsBuilt[7]);
		
		JPanel panel_30 = new JPanel();
		panel_30.setPreferredSize(new Dimension(100, 80));
		panel_30.setOpaque(false);
		panel_27.add(panel_30);
		
		JLayeredPane layeredPane_14 = new JLayeredPane();
		panel_30.add(layeredPane_14);
		
		JPanel panel_31 = new JPanel();
		panel_31.setPreferredSize(new Dimension(230, 95));
		panel_31.setOpaque(false);
		panel_16.add(panel_31);
		
		JPanel panel_32 = new JPanel();
		panel_32.setPreferredSize(new Dimension(230, 25));
		panel_32.setOpaque(false);
		panel_31.add(panel_32);
		
		permitHills = new JButton("Permit cards of Hills Region");
		permitHills.setPreferredSize(new Dimension(220, 20));
		panel_32.add(permitHills);
		
		hills= new CouncilPanel(m.getRegions()[1].getCouncil());
		panel_31.add(hills);
		
		JPanel panel_34 = new JPanel();
		panel_34.setPreferredSize(new Dimension(225, 450));
		panel_34.setOpaque(false);
		map.add(panel_34);
		
		JLayeredPane layeredPane_15 = new JLayeredPane();
		panel_34.add(layeredPane_15);
		
		JPanel panel_35 = new JPanel();
		panel_35.setPreferredSize(new Dimension(230, 110));
		panel_35.setOpaque(false);
		panel_34.add(panel_35);
		
		JPanel panel_36 = new JPanel();
		panel_36.setPreferredSize(new Dimension(100, 100));
		panel_36.setOpaque(false);
		panel_35.add(panel_36);
		
		JPanel panel_37 = new JPanel();
		panel_37.setPreferredSize(new Dimension(100, 30));
		panel_37.setOpaque(false);
		panel_37.setMinimumSize(new Dimension(100, 80));
		panel_36.add(panel_37);
		
		JLayeredPane layeredPane_16 = new JLayeredPane();
		panel_36.add(layeredPane_16);

		panel_36.add(bonusButtons[10]);
		panel_36.add(check[10]);
		panel_36.add(emporiumsBuilt[10]);
		
		JPanel panel_48 = new JPanel();
		panel_48.setOpaque(false);
		panel_48.setPreferredSize(new Dimension(100, 100));
		panel_35.add(panel_48);
		
		JPanel panel_40 = new JPanel();
		panel_40.setPreferredSize(new Dimension(230, 110));
		panel_40.setOpaque(false);
		panel_34.add(panel_40);
		
		JPanel panel_41 = new JPanel();
		panel_41.setPreferredSize(new Dimension(110, 100));
		panel_41.setOpaque(false);
		panel_40.add(panel_41);
		
		JPanel panel_42 = new JPanel();
		panel_42.setPreferredSize(new Dimension(100, 30));
		panel_42.setOpaque(false);
		panel_42.setMinimumSize(new Dimension(100, 80));
		panel_41.add(panel_42);
		
		JLayeredPane layeredPane_18 = new JLayeredPane();
		panel_41.add(layeredPane_18);

		panel_41.add(bonusButtons[13]);
		panel_41.add(check[13]);
		panel_41.add(emporiumsBuilt[13]);
		
		JPanel panel_43 = new JPanel();
		panel_43.setPreferredSize(new Dimension(100, 100));
		panel_43.setOpaque(false);
		panel_40.add(panel_43);
		
		JPanel panel_44 = new JPanel();
		panel_44.setPreferredSize(new Dimension(100, 1));
		panel_44.setOpaque(false);
		panel_44.setMinimumSize(new Dimension(100, 80));
		panel_43.add(panel_44);
		
		JLayeredPane layeredPane_19 = new JLayeredPane();
		panel_43.add(layeredPane_19);

		panel_43.add(bonusButtons[11]);
		panel_43.add(check[11]);
		panel_43.add(emporiumsBuilt[11]);
		
		JPanel panel_45 = new JPanel();
		panel_45.setPreferredSize(new Dimension(230, 115));
		panel_45.setOpaque(false);
		panel_34.add(panel_45);
		
		JPanel panel_46 = new JPanel();
		panel_46.setPreferredSize(new Dimension(110, 105));
		panel_46.setOpaque(false);
		panel_45.add(panel_46);
		
		JPanel panel_47 = new JPanel();
		panel_47.setPreferredSize(new Dimension(100, 35));
		panel_47.setOpaque(false);
		panel_47.setMinimumSize(new Dimension(150, 80));
		panel_46.add(panel_47);
		
		JLayeredPane layeredPane_20 = new JLayeredPane();
		panel_46.add(layeredPane_20);

		panel_46.add(bonusButtons[14]);
		panel_46.add(check[14]);
		panel_46.add(emporiumsBuilt[14]);
		
		JPanel panel_38 = new JPanel();
		panel_38.setPreferredSize(new Dimension(100, 125));
		panel_38.setOpaque(false);
		panel_45.add(panel_38);
		
		JPanel panel_39 = new JPanel();
		panel_39.setPreferredSize(new Dimension(100, 1));
		panel_39.setOpaque(false);
		panel_39.setMinimumSize(new Dimension(100, 80));
		panel_38.add(panel_39);
		
		JLayeredPane layeredPane_17 = new JLayeredPane();
		panel_38.add(layeredPane_17);

		panel_38.add(bonusButtons[12]);
		panel_38.add(check[12]);
		panel_38.add(emporiumsBuilt[12]);
		
		JPanel panel_49 = new JPanel();
		panel_49.setPreferredSize(new Dimension(230, 95));
		panel_49.setOpaque(false);
		panel_34.add(panel_49);
		
		JPanel panel_50 = new JPanel();
		panel_50.setPreferredSize(new Dimension(230, 25));
		panel_50.setOpaque(false);
		panel_49.add(panel_50);
		
		permitMountains = new JButton("Permit cards of Mountains Region");
		permitMountains.setPreferredSize(new Dimension(220, 20));
		panel_50.add(permitMountains);
		
		mountains = new CouncilPanel(m.getRegions()[2].getCouncil());
		panel_49.add(mountains);
        
        JLayeredPane layeredPane = new JLayeredPane();
        add(layeredPane);
        events();
	}
	public static void events(){
    
		
	
		for(cont=0,k=0;k<5;k++,cont++){
				
				//System.out.println(cont+","+region+","+k);
			int c=k;
			int conto=cont;
				bonusButtons[conto].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						BonusPanel dialog = new BonusPanel(m.getRegions()[0].getCities().get(c).getBonus());
						dialog.setVisible(true);

						JFrame frame= new JFrame();//This should add a label only to the city where the king is located
						if(m.getKingsLocation().getName().equals(m.getRegions()[0].getCities().get(c).getName())){
						JLabel king= new JLabel("King's here");
						king.setVisible(true);
						king.setPreferredSize(new Dimension(180,20));
						dialog.add(king);
						}
						frame.setSize(200, 120);
						frame.getContentPane().add(dialog);
						frame.setVisible(true);
						frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					}
				});
	
				emporiumsBuilt[conto].setText(m.getRegions()[0].getCities().get(c).getEmporiumsBuilt()+"");
				check[conto].setSelected(m.getRegions()[0].getCities().get(c).isColonizedByMe(p));
			
	}
	
		for(cont=5,k=0;k<5;k++,cont++){
		int c=k;
		int conto=cont;
				bonusButtons[conto].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						BonusPanel dialog = new BonusPanel(m.getRegions()[1].getCities().get(c).getBonus());
						dialog.setVisible(true);

						JFrame frame= new JFrame();//This should add a label only to the city where the king is located
						
						if(m.getKingsLocation().getName().equals(m.getRegions()[1].getCities().get(c).getName())){
						JLabel king= new JLabel("King's here");
						king.setVisible(true);
						king.setPreferredSize(new Dimension(180,20));
						dialog.add(king);
						}
						frame.setSize(200, 120);
						frame.getContentPane().add(dialog);
						frame.setVisible(true);
						frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					}
				});
	
				emporiumsBuilt[conto].setText(m.getRegions()[1].getCities().get(c).getEmporiumsBuilt()+"");
				check[conto].setSelected(m.getRegions()[1].getCities().get(c).isColonizedByMe(p));
			
	}
	
		for(cont=10,k=0;k<5;k++,cont++){
		int c=k;
		int conto=cont;
				bonusButtons[conto].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						BonusPanel dialog = new BonusPanel(m.getRegions()[2].getCities().get(c).getBonus());
						dialog.setVisible(true);

						JFrame frame= new JFrame();//This should add a label only to the city where the king is located
					
						if(m.getKingsLocation().getName().equals(m.getRegions()[2].getCities().get(c).getName())){
						JLabel king= new JLabel("King's here");
						king.setVisible(true);
						king.setPreferredSize(new Dimension(180,20));
						dialog.add(king);
						}
						frame.setSize(200, 120);
						frame.getContentPane().add(dialog);
						frame.setVisible(true);
						frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					}
				});
	
				emporiumsBuilt[conto].setText(m.getRegions()[2].getCities().get(c).getEmporiumsBuilt()+"");
				check[conto].setSelected(m.getRegions()[2].getCities().get(c).isColonizedByMe(p));
			
	}
		permitSea.addActionListener(new ActionListener(){
			 	public void actionPerformed(ActionEvent e) {
			 		PermitCardPanel dialog = new PermitCardPanel();
			 			JPanel panel=dialog.panelOfPermitCard(m.getRegions()[0].getPermitsList());
			 			panel.setVisible(true);
						panel.setPreferredSize(new Dimension(310,120));
						JFrame frame= new JFrame();
						frame.setBounds(300, 300, 320, 120);
						frame.getContentPane().add(panel);
						frame.setVisible(true);
						frame.pack();
					
			 	}
			 });
		permitHills.addActionListener(new ActionListener(){
		 	public void actionPerformed(ActionEvent e) {
		 		PermitCardPanel dialog = new PermitCardPanel();
		 			JPanel panel=dialog.panelOfPermitCard(m.getRegions()[1].getPermitsList());
		 			panel.setVisible(true);
					panel.setPreferredSize(new Dimension(310,120));
					JFrame frame= new JFrame();
					frame.setBounds(300, 300, 320, 120);
					frame.getContentPane().add(panel);
					frame.setVisible(true);
					frame.pack();
				
		 	}
		 });
		permitMountains.addActionListener(new ActionListener(){
		 	public void actionPerformed(ActionEvent e) {
		 		PermitCardPanel dialog = new PermitCardPanel();
		 			JPanel panel=dialog.panelOfPermitCard(m.getRegions()[2].getPermitsList());
					panel.setVisible(true);
					panel.setPreferredSize(new Dimension(310,120));
					JFrame frame= new JFrame();
					frame.setBounds(300, 300, 320, 120);
					frame.getContentPane().add(panel);
					frame.setVisible(true);
					frame.pack();
					
		 	}
		 });
	
	
	}
	public static void updateButtons(){
		for(cont=0,k=0;k<5;k++,cont++){
		int c=k;
		int conto=cont;
				emporiumsBuilt[conto].setText(m.getRegions()[0].getCities().get(c).getEmporiumsBuilt()+"");
				check[conto].setSelected(m.getRegions()[0].getCities().get(c).isColonizedByMe(p));
			
	}
		for(cont=5,k=0;k<5;k++,cont++){
			int c=k;
			int conto=cont;
					emporiumsBuilt[conto].setText(m.getRegions()[1].getCities().get(c).getEmporiumsBuilt()+"");
					check[conto].setSelected(m.getRegions()[1].getCities().get(c).isColonizedByMe(p));
				
		}
		for(cont=10,k=0;k<5;k++,cont++){
			int c=k;
			int conto=cont;
					emporiumsBuilt[conto].setText(m.getRegions()[2].getCities().get(c).getEmporiumsBuilt()+"");
					check[conto].setSelected(m.getRegions()[2].getCities().get(c).isColonizedByMe(p));
				
		}
	}
	public static void update(Map map){
		m=map;
		updateButtons();
		sea.update(m.getRegions()[0].getCouncil());
		hills.update(m.getRegions()[1].getCouncil());
		mountains.update(m.getRegions()[2].getCouncil());
	}

}
