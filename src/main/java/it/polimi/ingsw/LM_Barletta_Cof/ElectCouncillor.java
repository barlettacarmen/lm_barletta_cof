package it.polimi.ingsw.LM_Barletta_Cof;

import it.polimi.ingsw.LM_Barletta_Cof.views.Gui;

public class ElectCouncillor implements MainAction {
	private Player player;
	private Map map;
	private CliGui cli;
	public  ElectCouncillor(Player p,Map m,CliGui cli){
		this.player=p;
		this.map=m;
		this.cli=cli;
	}

	public  Council chooseCouncil(){//Asks for the council to choose, returns a pointer to the council
		int i=cli.askForCouncil();
		if(i==4)
			return map.getKingsCouncil();
		return map.getRegions()[i-1].getCouncil();
	}
	public boolean electCouncillor(Council c,Councillor councillor){
	boolean	flag= false;
		if(councillor!=null&&councillor.isAvailable()){
		Councillor toelect=map.getCouncillorPool().popConucillor(councillor);
		Councillor toremove=c.getCouncil().remove(0);
		map.getCouncillorPool().pushCouncillor(toremove);
		c.getCouncil().add(toelect);
		flag=true;
		}
		else{ player.getCli().printString("The councillor you selected was not available");
				flag=false;
		}
		return flag;
	}
	private void getCoin (){
		player.setCoin(player.getCoin()+4);//4 is the reward for electing a councillor, it is defined in the rule file of the game
		
	}
	private Councillor selectCouncillor(){
		Councillor c=cli.askForCouncillor(map.getCouncillorPool());
		return c;
	}
	public boolean tryToElect(){
		boolean	flag=false;
		Council c=chooseCouncil();
		cli.printCouncillors(c.getCouncil());//Added in order to show the player the status of the chosen council
		Councillor councillor=selectCouncillor();
		flag=electCouncillor(c,councillor);
		return flag;
	}
	
	@Override
	public boolean doAction(){
	boolean	flag=false;
		flag=tryToElect();
		if(flag)
			getCoin();
		return flag;
	}
}
