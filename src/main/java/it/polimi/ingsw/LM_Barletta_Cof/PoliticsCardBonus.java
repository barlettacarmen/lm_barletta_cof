package it.polimi.ingsw.LM_Barletta_Cof;

public class PoliticsCardBonus implements Bonus {
	private int id=Constants.POLITICS_CARD_BONUS;
	@Override
	public void useBonus(Player p, Map m){ //anche qui non serve mappa 
		p.getPoliticsCard().add(new PoliticsCard());
		
	}
	@Override
	public int getId() {
		
		return id;
	}
	

}
