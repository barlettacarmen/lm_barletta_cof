package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class Exchange implements Serializable{
	private PermitCard offered;
	private String requested;
	private PoliticsCard offeredPol;
	private PoliticsColours requestedPol;
	private Player seller;



	public Exchange (Player seller,PermitCard offered,String requested){
		this.offered=offered;
		this.requested=requested;
		this.seller=seller;
	}
	public Exchange (Player seller,PoliticsCard offered,PoliticsColours requested){
		this.offeredPol=offered;
		this.requestedPol=requested;
		this.seller=seller;
	}
	public PermitCard getCardOffered(){
		return this.offered;
	}
	public PoliticsCard getPoliticsCardOffered(){
		return this.offeredPol;
	}
	public String getCardRequested(){
		return this.requested;
	}
	public PoliticsColours getPoliticsCardRequested(){
		return this.requestedPol;
	}
	public Player getSeller(){
		return this.seller;
	}

}
