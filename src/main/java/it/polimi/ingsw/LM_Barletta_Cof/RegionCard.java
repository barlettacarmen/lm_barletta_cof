package it.polimi.ingsw.LM_Barletta_Cof;

public class RegionCard implements Card{
	private Region region;
	private VictoryPointBonus bonus;
	private boolean available;
	
	public RegionCard(Region r, Configuration config){
		this.region=r;
		this.available=true;
		bonus= new VictoryPointBonus(config);
	}

	public VictoryPointBonus getBonus() {
		return bonus;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	
	

}
