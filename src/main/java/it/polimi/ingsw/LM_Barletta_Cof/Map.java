package it.polimi.ingsw.LM_Barletta_Cof;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Map implements Serializable {
	private static final Logger log= Logger.getLogger( Map.class.getName() );
	private int mapId;
	private Region[] regions= new Region[Constants.NUM_OF_REGIONS];
	private  City kingsLocation; 
	private Council kingsCouncil;
	private ColourCard[] colourCard;
	private KingCard[] kingCard; 
	private NobilityCell[] nobilityTrack;
	private  Cell[] victoryTrack;
	private  Cell[] coinsTrack;
	private CouncillorPool pool;
	private Player[] playerlist;
	
	
	public Map(Player[] playerList, Configuration config){
		this.playerlist=playerList;
		this.pool= new CouncillorPool();
		this.kingsCouncil= new Council(this.pool);
		for(int i=0;i<Constants.NUM_OF_REGIONS;i++)
			regions[i]= new Region("REGION_"+(i+1), config, this.pool);
			
		
		
		setColourCard(config);
		setKingCard(config);
		this.kingsLocation=getCityPointer(config.getKingsInitialPosition());
		setnextCitiesOfEachCity(config.getFilepath());
		checkMapConnected(config.getNumberOfCitiesForEachRegion());
		int numOfPlayers=playerList.length;
		victoryTrack=new Cell[numOfPlayers]; 
		coinsTrack=new Cell[numOfPlayers];
		for(int i=0;i<numOfPlayers;i++){
			victoryTrack[i]= new Cell(playerList[i], playerList[i].get_victoryPoint());
			coinsTrack[i]=new Cell(playerList[i],playerList[i].getCoin());
			}
		nobilityTrack= new NobilityCell[Constants.NOBILITY_TRACK_LENGTH];
		for(int i=0;i<Constants.NOBILITY_TRACK_LENGTH;i++){
		nobilityTrack[i]= new NobilityCell(i, config, this);}
		for(int i=0;i<numOfPlayers;i++){
		nobilityTrack[0].putPlayerInNobilityCell(playerList[i]);}
		
		setMapId(config);
		}
	/** 
	 * scorre tutte le città (salva i nomi delle città nell'arryList cities) e salva tutti i possibili colori in un array di supporto
	 * che controlla di inserire i colori senza duplicati. Poi genera le 4 carte regione passando i vari 
	 *  colori.
	 */	
	public void setColourCard(Configuration config){
		
		colourCard=new ColourCard[config.getCityColours().size()]; 
		for(int i=0; i<colourCard.length;i++)
			colourCard[i]= new ColourCard(config.getCityColours().get(i), config);
		}
				
	public void setKingCard(Configuration config){
		kingCard= new KingCard[Constants.NUM_OF_KING_CARD];
		for(int i=0; i<Constants.NUM_OF_KING_CARD; i++)
			kingCard[i]= new KingCard(i+1, config);
		
	}
	
	public City getCityPointer(String cityName) {
		
		for(Region r:regions)
			for(City c: r.getCities()){
			if(c.getName().equals(cityName))
					return c;
		}
		
		return null;
	}
	
	public void updateVictoryTrackStatus(Player p){
		int id=p.getId();
		int n=p.get_victoryPoint();
		this.victoryTrack[id-1].setAmount(n);
		
	}
	public void updateCoinsTrackStatus(Player p){
		int id=p.getId();
		int n=p.getCoin();
		this.coinsTrack[id-1].setAmount(n);
		
	}
	//questo metodo vede i punti nobiltà del player e se sono diversi dalla posizione in cui si trova
	//sul nobilityTrack, aggiorna quest'ultimo, rimuovendo il player dalla nobilityCell in cui era 
	//e inserendolo nella nuova posizione in base ai suoi nobility points
	public void updateNobilityTrackStatus(Player p){
		
		int nobilitypoints=p.get_nobilityPoint();
		int position;
		if(nobilitypoints>20){
			nobilitypoints=20;
			p.set_nobilityPoint(20);
		}	
		for(NobilityCell n: nobilityTrack){
			position=n.existsPlayerInThisCell(p);
			if(position!=-1 && nobilitypoints!=position){
				n.removePlayerFromNobilityCell(p);
				nobilityTrack[nobilitypoints].putPlayerInNobilityCell(p);}
			
		}
	}
	
	public void setnextCitiesOfEachCity(String path){

		String line,city_node,city_linked;
		try{
			File file= new File(path);
			FileReader fr=new FileReader(file);
			BufferedReader br= new BufferedReader(fr);
			line=br.readLine();
			while(line.compareTo("CITY_LINKS")!=0)
				line=br.readLine();
			line=br.readLine();
			while(line.compareTo("END_CITY_LINKS")!=0){
				if(line.compareTo("BEGIN_LINK")==0){
					city_node=br.readLine();
					City node=getCityPointer(city_node);
					city_linked=br.readLine();
					while(city_linked.compareTo("END_LINK")!=0){
						City linked=getCityPointer(city_linked);
						if(node.getNextCities().contains(linked)==false)
							node.getNextCities().add(linked);
						if(linked.getNextCities().contains(node)==false)
							linked.getNextCities().add(node);
						city_linked=br.readLine();}
				
				line=br.readLine();}}
			
			br.close();
			fr.close(); //it's closed!
		
	}catch(IOException e){
		log.log( Level.WARNING, e.toString(), e );
	}
}
	public Region[] getRegions() {
		return regions;
	}
	public Council getKingsCouncil(){
		return kingsCouncil;
	}
	
public CouncillorPool getCouncillorPool() {
		return pool;
	}

	public City getKingsLocation() {
	return kingsLocation;
}
	
	public ColourCard[] getColourCard() {
		return colourCard;
	}
	public KingCard[] getKingCard() {
		return kingCard;
	}
	public KingCard getKingCardBis(int i) {
		return kingCard[i];
	}
	public int getTheAvailableKingCard(){
		for(int i=0;i<kingCard.length;i++)
			if(kingCard[i].isAvailable())
				return i;
		return -1;
	}
	
	//controlla se la mappa è un grafo connesso 
//se non lo è individua le componenti connesse e lega tutte le componenti connesse 
//legandole alla prima componente connessa 
//gestisce anche gli array di nextcities che vengono settati di conseguenza
	public void checkMapConnected(int citiesInRegion){
		ArrayList<City> ordine = new ArrayList<City>(Constants.NUM_OF_REGIONS*citiesInRegion);
		for(Region r:regions)
			for(City c: r.getCities())
				ordine.add(c);
	int[]connected= cc(this.regions,ordine);
	for(int i=0;i<connected.length;i++)
		for(int k=1;k<connected.length;k++){
			if(connected[i]!=connected[k]){
				ordine.get(i).getNextCities().add(ordine.get(k));
				ordine.get(k).getNextCities().add(ordine.get(i));
				int l=connected[k];
				for(int j=k;j<connected.length;j++)
					if(l==connected[j])
						connected[j]=connected[i];
					
		}
		}

	}	
	//algoritmo di visita DFS
	public int[] cc(Region[] regions, ArrayList<City> ordine){
		int conta=0;
		int[] id=new int[ordine.size()];
//		for(int i: id){ 
//			i=0;
//		}
		int[] n=new int[ordine.size()];
		for(int i=0;i<n.length;i++) n[i]=i;
		for(int i=0; i<ordine.size();i++)
			if(id[n[i]]==0){
				conta++;
				ccdfs(regions,conta,id,i,ordine.get(i),ordine);
			}
		return id;
	}
	public void ccdfs(Region[] regions, int conta, int[] id, int i, City c,ArrayList<City> ordine){
		int j;
		
		id[i]=conta;
		for(City cit: c.getNextCities()){
			for(j=0;j<ordine.size();j++)
				if(cit.getName().equals(ordine.get(j).getName()))
					 if(id[j]==0)
						 ccdfs(regions,conta,id,j,cit,ordine);
		}	
			
		
	}
	
	public String getPlayerUsername(int id){
		
		for(int i=0;i<playerlist.length;i++)
			if(playerlist[i].getId()==id)
				return playerlist[i].getUsername();
		return null;
	}
	
	
	public Player[] getPlayerlist() {
		return playerlist;
	}
	
	public int getMapId() {
		return mapId;
	}
	public void setMapId(String path) {
		char a=path.charAt(18);
		this.mapId=a-46;
		
	}
	public void setKingsLocation(City kingsLocation) {
		this.kingsLocation = kingsLocation;
	}
	public NobilityCell[] getNobilityTrack() {
		return nobilityTrack;
	}
	
	public Cell[] getVictoryTrack() {
		return victoryTrack;
	}
	public Cell[] getCoinsTrack() {
		return coinsTrack;
	}
	public void setMapId(Configuration config) {
		this.mapId=config.getMapId();
		
	}

}

