package it.polimi.ingsw.LM_Barletta_Cof;
import java.io.Serializable;
import java.util.ArrayList;

public class NobilityCell implements Serializable{
	private ArrayList<Player> playerList= new ArrayList<Player>(); 
	private ArrayList<Bonus> bonus;
	private int grade; //c'è corrispondenza fra la cell di grade=i e il nobilitytrack[i]
	private Map m;
	
	public NobilityCell (int grade, Configuration config, Map m){
		this.grade=grade;
		this.m=m;
		if(this.grade==0) this.bonus=null;
		else putBonus(config);
	}
	
	public void putPlayerInNobilityCell(Player p){
		this.playerList.add(p);
		if(this.bonus!=null){
			for(Bonus b: this.bonus)
				b.useBonus(p, m);
		}
				}
		
	public void removePlayerFromNobilityCell(Player p){
		this.playerList.remove(p);
	}
			
	public int getgrade(){
		return this.grade;
	}
	public int existsPlayerInThisCell(Player p){
		if(playerList.contains(p)==true)
			return this.grade;
		return -1;
		
	}
	
	public void putBonus( Configuration config){
		RandomGenerator rand= new RandomGenerator(Constants.MAX_TYPE_OF_BONUS);
		int numOfBonus=RandomGenerator.choose_NumOfBonus(0, config.getMaxNumOfBonusNobilityCell() +1);
		if(numOfBonus!=0){
			this.bonus= new ArrayList<Bonus>(numOfBonus);
			for(int i=0;i<numOfBonus;i++){
				int bonus=rand.nextInt();
				if(bonus==Constants.NOBILITY_POINT_BONUS)
					 bonus=rand.nextInt();
				this.bonus.add(Bonus.setBonus(bonus, config));
			}
			
		}else 
			this.bonus=null;
		
	}

	public ArrayList<Bonus> getBonus() {
		return bonus;
	}

	public ArrayList<Player> getPlayerList() {
		return playerList;
	}
	
	//Costruttore per test
	public NobilityCell(Bonus[] b, int grade, Map m){
		this.bonus= new ArrayList<Bonus>(b.length); 
		this.grade=grade;
		this.m=m;
		for(int i=0;i<b.length;i++)
			this.bonus.add(b[i]);
		
	}
	
	
	

}
