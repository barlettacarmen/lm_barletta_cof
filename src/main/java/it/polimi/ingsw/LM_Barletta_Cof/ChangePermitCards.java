package it.polimi.ingsw.LM_Barletta_Cof;


public class ChangePermitCards implements QuickAction {
	Player player;
	Map map;
	private CliGui cli;
	public ChangePermitCards (Player p,Map m, CliGui cli){
		player=p;
		map=m;
		this.cli=cli;
	}
	public boolean discardAssistant(){
		boolean ok=false;
		if(player.get_numAssistants()>0){
			player.set_numAssistants(player.get_numAssistants()-1);
			ok=true;
		}
		return ok;
	}
	public void changePermitCards(Region r){
		r.setPermits();
		r.setPermits();
		r.getPermits(1);
		r.getPermits(0);

	}
	public Region chooseRegion() {
		int reg=cli.askForRegion();
		return map.getRegions()[reg-1];
	}
	@Override
	public boolean doAction() {
		if(discardAssistant()){
			changePermitCards(chooseRegion());
			return true;
		}
		return false;
		// TODO Auto-generated method stub

	}

}
