package it.polimi.ingsw.LM_Barletta_Cof;

public class AssistantBonus implements Bonus {
	private int assistants;
	private int id=Constants.ASSISTANT_BONUS;
	public AssistantBonus(Configuration config){
		this.assistants=RandomGenerator.choose_NumOfBonus(1,config.getMaxNumOfAssistantsBonus()+1);
	}
	public AssistantBonus(int assistants){
		this.assistants=assistants;
	} 
	@Override
	public void useBonus(Player p,Map m){ //non gli serve la mappa in ingresso 
		int n=p.get_numAssistants()+assistants;
		p.set_numAssistants(n);

	}
	@Override
	public int getId() {
		return id;}
	
	public int getAssistants() {
		return assistants;
	}
	
	
}
