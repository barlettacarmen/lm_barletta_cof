package it.polimi.ingsw.LM_Barletta_Cof;
import java.io.*;
import java.net.SocketException;
import java.rmi.RemoteException;

import it.polimi.ingsw.LM_Barletta_Cof.networking.CliInterface;
import it.polimi.ingsw.LM_Barletta_Cof.networking.CliRmiHandler;
import it.polimi.ingsw.LM_Barletta_Cof.networking.CliSocketHandler;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Cli implements CliGui, Serializable {
	private static final Logger log= Logger.getLogger( CliSocketHandler.class.getName() );
	private CliInterface handler;
	public Cli(){}
	public Cli(BufferedReader in,PrintWriter out){
		this.handler=new CliSocketHandler(in,out);
	}
	
	
	public Cli(CliInterface cli) {
		this.handler=cli;
	}
	
	
	
	@Override
	public CliInterface getCliInterface() {
		return handler;
	}
	
	
	
	///////////////////
	
	
	@Override
	public void printString(String s){
		try{
			handler.printString(s);
		}
		catch(RemoteException e){
			log.log( Level.WARNING, e.toString(), e );
		}
	}
	
	
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#getInteger()
	 */
	@Override
	public int getInteger(){
		int i=0;
		try{
			i=handler.getInteger();
		}
		catch(RemoteException | SocketException e){
			log.log( Level.WARNING, e.toString(), e );
		}
		return i;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#getString()
	 */
	@Override
	public String getString(){
		String i="";
		try{
			i=handler.getString();
		}
		catch(RemoteException e){
			log.log( Level.WARNING, e.toString(), e );
		}
		return i;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#getChar()
	 */
	@Override
	public char getChar(){
		char i='n';
		try{
			i=handler.getChar();
		}
		catch(RemoteException e){
			log.log( Level.WARNING, e.toString(), e );
		}
		return i;
	}
	////////////////////
	
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#wantToPerformQuickAction()
	 */
	@Override
	public  boolean wantToPerformQuickAction() {
		boolean output=false;
		char buffer;
			printString("Do you want to perform a quick action before the main action? [Y/N]");
		buffer=getChar();
		if((buffer=='y')||(buffer=='Y')){
			output=true;
		}
		return output;
	
	}
	private  void printRequestForQuickAction() {
		printString("Insert the code of the quick action"
				+ "you want to perform");
		printString("Insert '1' if you want to engage an assistant");
		printString("Insert '2' if you want to change the permit cards of a region");
		printString("Insert '3' if you want to send an assistant to elect a councillor");
		printString("Insert '4' if you want to perform an additional main action");
		
	}
	private  void printRequestForMainAction() {
		printString("Insert the code of the main action"
				+ "you want to perform");
		printString("Insert '1' if you want to elect a councillor");
		printString("Insert '2' if you want to build an emporium with the help of the king");
		printString("Insert '3' if you want to build an emporium with a permit card");
		printString("Insert '4' if you want to acquire a permit card");
		
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForTipeOfAction()
	 */
	@Override
	public  int askForTipeOfAction() {
		printRequestForQuickAction();
		int i=0;
		do{
		 i=getInteger();
		}while(i<1||i>4);
		return i;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForTipeOfMainAction()
	 */
	@Override
	public  int askForTipeOfMainAction() {
		printRequestForMainAction();
		int i=0;
		do{
		 i=getInteger();
		}while(i<1||i>4);
		return i;
	}
	private  void printRequestForCity() {
		printString("Insert the name of the city");
	}
	private  void printRequestForKingInitialPosition() {
		printString("Insert the name of the King's intial city");
		
	}

	public  void printInvalidCity() {
		printString("Invalid city please, insert an existing city");
	}

	public  void printInstructionToInsertLinks(String s) {
		printString("Insert cities linked to " +s+" and when you finish write END_LINK");
	}
	private  void printRequestForCityColour() {
		printString("Choose one of these 4 colours as the colour of the city: blue, yellow, grey or orange");
	}
	private  void printRequestForInteger() {
		printString("Insert the requested value");
	}
	private void printRequestForLogin(){ /////login
		printString("Please, enter your username");
	}

	public  String askForCityName() {
		printRequestForCity();
		return getString();
		
	}
	//////////////////////////////prova login///////////


	public  String askForLogin() {
		printRequestForLogin();
		return getString();
		
	}
/////////////////////////////////////////////////////////	
	
	
	public  String askForCityColour() {
		String colour;
		printRequestForCityColour();
		colour=getString().toLowerCase();
		while(!("blue".equals(colour)) &&!("yellow".equals(colour))&&!("grey".equals(colour))&&!("orange".equals(colour))){
			printRequestForCityColour();
			colour=getString().toLowerCase();}
		return colour;
		
	}

	public  String askForKingInitalPosition() {
		printRequestForKingInitialPosition();
		return getString();
	}
	

	public  int askForMaxNumerOfCitiesForEachRegion() {
		printString("How many cities do you want for each region?");
		printRequestForInteger();
		return getInteger();
	}
	
	
	public  int askForNumberOfBonusesForEachCity() {
		printString("What's the number of bonuses you want for each city?");
		printRequestForInteger();
		return getInteger();
	}
	
	
	public  int askForNumberOfCitiesonEachPermitCard() {
		printString("What's the max number of cities you want for each permit card?");
		printRequestForInteger();
		return getInteger();
	}
	
	
	public  int askForMaxNumberOfBonusesForEachPermitCard() {
		printString("What's the max number of bonuses you want for each Permit Card?");
		printRequestForInteger();
		return getInteger();
	}
	
	public  int askForMaxValueOfVictoryPointBonus() {
		printString("What's the max value of Victory Point Bonus you want?");
		printRequestForInteger();
		return getInteger();
	}

	public  int askForMaxValueOfCoinBonus() {
		printString("What's the max value of Coin Bonus you want?");
		printRequestForInteger();
		return getInteger();
	}

	public  int askForMaxValueOfAssistantBonus() {
		printString("What's the max value of Assistant Bonus you want?");
		printRequestForInteger();
		return getInteger();
	}

	public  int askForMaxNumBonusNobilityCell() {
		printString("What's the max number of bonuses you want to put in Nobility Cell?");
		printRequestForInteger();
		return getInteger();
	}

	public  void printPlayerStatus(Player p) {
		printString("Player "+p.getId()+" has got:");
		printString(p.getCoin()+" coins");
		printString(p.get_victoryPoint()+" victory coins");
		printString(p.get_nobilityPoint()+" nobility coins");
		printString(p.get_numAssistants()+" assistants");
		
	}
	private  void printPoliticsCardList (ArrayList<PoliticsCard> a){
		for(int i=0;i<a.size();i++){
			printString(i+") "+a.get(i).getPoliticsColour().toString());	
		}
	}
	private  void printPermitCardList (ArrayList<PermitCard> a) {
		PermitCard permit;
		for(int i=0;i<a.size();i++){
			permit=a.get(i);
			printString("Permit card "+(i)+" valid for the following cities:");
			for(int k=0;k<permit.getCitiesPermitted().size();k++){
				printString(permit.getCitiesPermitted().get(k)+"\n");
			}
			
		}
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#printPlayersCardList(it.polimi.ingsw.LM_Barletta_Cof.Player)
	 */
	@Override
	public  void printPlayersCardList(Player p) {
		printString("Player "+p.getId()+" has got the following politics cards:");
		printPoliticsCardList(p.getPoliticsCard());
		printString("Player "+p.getId()+" has got the following permit cards:");
		printPermitCardList(p.getPermitCard());
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForColorOfCouncillor()
	 */
	@Override
	public  String askForColorOfCouncillor() {
		printString("Type the colour you want to pick up");
		return getString();
		
	}

	
	@Override
	public  void printPoolOfCouncillors(CouncillorPool pool) {
		printString("The councillor pool contains: ");
		for(int i=0;i<pool.getAvailableCouncillors().size();i++)
			if(pool.getAvailableCouncillors().get(i).isAvailable())
				printString((i+1)+")"+pool.getAvailableCouncillors().get(i).getColour().toString());
	}
	//Methods for the player
	
	@Override
	public  int askForCouncil() {
		printString("Type the number of the council you want to select");
		printString("1 for the Sea region's council");
		printString("2 for the Hills region's Council");
		printString("3 for the Mountains region's council");
		printString("4 for the King's council");
		int i=getInteger();
		if(i<1||i>4) //default
			i=4;
		return i;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForRegion()
	 */
	@Override
	public  int askForRegion() {
			printString("Type the number of the region you want to select");
			printString("1 for the Sea region");
			printString("2 for the Hills region");
			printString("3 for the Mountains region");
			int i=getInteger();
			if(i<1||i>3) //default
				i=1;
		return i;

	}
	
	@Override
	public  int askForPermitCard(ArrayList<PermitCard> permits) {
		printString("Type the number of the permit card you want to select");
		//PermitCard permit;
		printPermitCardList(permits);
		return getInteger();
	}
	
	@Override
	public  City askForColonizedCity(ArrayList<Emporium> list) {
		int i;
		do{
		printString("Type the number of one of the following colonized cities:");
		printColonizedCities(list);
		i=getInteger();
		}while(i<0 || i>list.size()-1);
		
		return list.get(i).getCity();
	}
	private  void printColonizedCities(ArrayList<Emporium> list){
		for(int k=0;k<list.size();k++){
			if(list.get(k).getCity()!=null){
			printString(k+" "+list.get(k).getCity().getName().toString());
			}
		}
	}
	//End of player's methods
	
	@Override
	public  int askForPoliticsCard(ArrayList<PoliticsCard> cards) {
		printString("Type the number of the politics card you want to select");
		printPoliticsCardList(cards);
		return getInteger();
	}
	
	@Override
	public int askForTypeOfCard(){
		int type=0;
		printString("Type 1 if you want to select a permit card, 2 for a politics card");
		while(type!=1&&type!=2){
			type=getInteger();
		}
		return type;
	}
	
	@Override
	public int askForPrice(){
		printString("Type the price for the item you want to sell");
		return getInteger();
	}

	@Override
	public  void 	 printItemList(ArrayList<Item> itemList) {
		for(int i=0;i<itemList.size();i++){
			PermitCard card=itemList.get(i).getPermitCard();
			if(card!=null){
				printString(i+") Permit card valid for the following cities:");
			for(int k=0;k<card.getCitiesPermitted().size();k++){
				printString(card.getCitiesPermitted().get(k));
			}	
		}
			PoliticsCard pol=itemList.get(i).getPoliticsCard();
			if(pol!=null){
			printString(i+") Politics card with this colour: ");
			printString(pol.getPoliticsColour().toString());	
			}
		}
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForItemToChoose()
	 */
	@Override
	public  int askForItemToChoose() {
		printString("Type the number of the item you want to select");
		return getInteger();
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#printExchangeList(java.util.ArrayList)
	 */
	@Override
	public  void 	 printExchangeList(ArrayList<Exchange> exchangeList) {//only for exchanges between cards of the same type
		for(int i=0;i<exchangeList.size();i++){
			printString(i+" Card offered: ");
			PermitCard card=exchangeList.get(i).getCardOffered();
			if(card!=null){
				printString(i+") Permit card valid for the following cities:");
			for(int k=0;k<card.getCitiesPermitted().size();k++){
				printString(card.getCitiesPermitted().get(k));
			}
		}
			PoliticsCard pol=exchangeList.get(i).getPoliticsCardOffered();
			if(pol!=null){
				printString(i+") Politics card with this colour: ");
			printString(pol.getPoliticsColour().toString());
			}
			printString(i+" Card requested: ");
			String requested=exchangeList.get(i).getCardRequested();
			if(requested!=null){
				printString(i+") Permit card valid for the following city:");
				printString(requested);
			
		}
			PoliticsColours politics=exchangeList.get(i).getPoliticsCardRequested();
			if(politics!=null){
			printString(i+") Politics card with this colour: ");
			printString(politics.toString());
			}
		}
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForExchangeToChoose()
	 */
	@Override
	public  int askForExchangeToChoose() {
		printString("Type the number of the exchange you want to select");
		return getInteger();
	}
	//End of market methods
	//
	//Methods for the actions
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#printCouncillors(java.util.ArrayList)
	 */
	@Override
	public  void printCouncillors(ArrayList<Councillor> list) {
		for(int k=0;k<list.size();k++){
			printString(k+1+") Councillor "+list.get(k).getColour());
		}
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForCouncillor(java.util.ArrayList)
	 */
	@Override
	public  Councillor askForCouncillor(CouncillorPool list) {				
		printString("Type the number of the councillor you want to select");
		printPoolOfCouncillors(list);
		int i=getInteger()-1;
		if(i>=0 && i<24)
			return list.getAvailableCouncillors().get(i);
		return null;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForCity(it.polimi.ingsw.LM_Barletta_Cof.Region)
	 */
	@Override
	public  City askForCity(Region r) {
		printString("Type the number of the city you want to select");
		int i;
		do{
			printCityList(r.getCities());
			i=getInteger();
		}while(i<1||i>r.getCities().size());
		return r.getCities().get(i-1);
	}
	private  void printCityList(ArrayList<City> list) {
		for(int k=0;k<list.size();k++){
			printString(k+1+") "+list.get(k).getName());
		}
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForNumberOfPoliticsCard()
	 */
	@Override
	public  int askForNumberOfPoliticsCard() {
		printString("How many politics card do you want to use to satisfy the council?");
		return getInteger();
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForMulticolourCardNumber()
	 */
	@Override
	public  int askForMulticolourCardNumber() {
		printString("How many multicolour politics card do you want to use to satisfy the council?");
		return getInteger();
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForNextCity(it.polimi.ingsw.LM_Barletta_Cof.City)
	 */
	@Override
	public  City askForNextCity(City c) {
		printString("Type the number of the city you want to select");
		printCityList(c.getNextCities());
		int i;
		do{
			i=getInteger();
		}while(i<1||i>c.getNextCities().size());
		return c.getNextCities().get(i-1);
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askIfFinalMove()
	 */
	@Override
	public  boolean askIfFinalMove() {
		printString("Is this the city where you want the king to be in? [Y/N]");
		char chosen=getChar();
		if(chosen=='y'||chosen=='Y'){
			return true;
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#askForCityToColonize(java.util.ArrayList)
	 */
	@Override
	public  String askForCityToColonize(ArrayList<String> list) {
		printString("Type the number of the city you want to colonize");
			printCityPermitted(list);
			int i=getInteger();
		return list.get(i-1);
	}
	private  void printCityPermitted(ArrayList<String> list) {
		for(int k=0;k<list.size();k++){
			printString(k+1+") "+list.get(k));
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#choosenRegion(it.polimi.ingsw.LM_Barletta_Cof.Map)
	 */
	@Override
	public Region choosenRegion(Map map){
		int region=0;
		while(region<=0||region>3)
			region=askForRegion();
		return map.getRegions()[region-1];
		//dammi una l'identificativo di una regione, nome o intero
		// return puntatore alla regione
	}

	/* (non-Javadoc)
	 * @see it.polimi.ingsw.LM_Barletta_Cof.CliGui#choosenPermitCard(it.polimi.ingsw.LM_Barletta_Cof.Region)
	 */
	@Override
	public int choosenPermitCard(Region r){
		int card=0;
		while(card!=1&&card!=2)
			card=askForPermitCard(r.getPermitsList())-1;
		return card;
		//chiede quale permitCard Prendere 1 0 2 e return l'intero diminuito di uno
	}
	
	

}