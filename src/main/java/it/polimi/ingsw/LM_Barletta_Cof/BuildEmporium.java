package it.polimi.ingsw.LM_Barletta_Cof;

public class BuildEmporium implements MainAction {
	private Player player;
	private Map map;
	private CliGui cli;
	
	public BuildEmporium (Player p,Map m,CliGui cli){
		player=p;
		map=m;
		this.cli=cli;
	}

	private City selectCity(Map m){
		int reg=cli.askForRegion();
		return cli.askForCity(m.getRegions()[reg-1]);
		
	}
	public void placeEmporium (City c){
		boolean done=false;
		
		for(int i=0;i<player.getEmporium().size()&&!done;i++){
			if(!player.getEmporium().get(i).getBuilt()){
				done=true;
				player.getEmporium().get(i).setBuilt(true);
				player.getEmporium().get(i).setPlayer(player);
				player.getEmporium().get(i).setPosition(c);
				player.setBuiltEmporium(player.getBuiltEmporium()+1);
				c.getEmporium().add(player.getEmporium().get(i));
				//se player piazza ultimo emporium guadagna 3 victoryPoints
				if(player.getBuiltEmporium()==Constants.EMPORIUM_PER_PLAYER)
					player.set_victoryPoint(player.get_victoryPoint()+3);
				if(player.getBuiltEmporium()>=4){
					checkIfColonizedColor(c.getColour());
					if(checkIfColonizedRegion(c.getRegion())&& c.getRegion().getRegionCard().isAvailable()){
						int points= player.get_victoryPoint();
						player.set_victoryPoint(points+c.getRegion().getRegionCard().getBonus().getVictoryPoints());
						c.getRegion().getRegionCard().setAvailable(false);
						int kingC=map.getTheAvailableKingCard();
						if(kingC!=-1){
						points= player.get_victoryPoint();
						player.set_victoryPoint(points+map.getKingCardBis(kingC).getBonus().getVictoryPoints());
						map.getKingCardBis(kingC).setAvailable(false);}
						}
				}
				 
					
			}
		}
		
	}
	//controlla se è available la carta colore del colore della città che ha costruito
	//e se il player ha colonizzato tutte le città con quel colore gli da la color card e king card(se available)
	public void  checkIfColonizedColor(String color){
		int cont=0;
		color= color.toUpperCase();
		for(int i=0;i<4;i++){
			if(map.getColourCard()[i].isAvailable()&&map.getColourCard()[i].getColour().toUpperCase().equals(color)){
				for(int k=0;k<Constants.NUM_OF_REGIONS;k++)
					for(City c:map.getRegions()[k].getCities()){
						if(color.equals(c.getColour())&&c.isColonizedByMe(player))
							cont++;}
			if((cont==5 && color.equals("GREY"))|| (cont==4 &&!color.equals("GREY"))){
				int po= player.get_victoryPoint();
				player.set_victoryPoint(po+map.getColourCard()[i].getBonus().getVictoryPoints());
				map.getColourCard()[i].setAvailable(false);
			}}
		}
		int kingC=map.getTheAvailableKingCard();
		if(kingC!=-1){
		player.set_victoryPoint(map.getKingCardBis(kingC).getNumber());
		map.getKingCardBis(kingC).setAvailable(false);}			
	}
	
	public boolean checkIfColonizedRegion(Region r){
		boolean flag=false;
		int n=0;
		for(City c: r.getCities())
			if(c.isColonizedByMe(player))
				n++;
		if(n==5)
			flag=true;
		return flag;
			
	}
	
	public int checkExistenceOfOtherEmporium(City c){
		int cont=0;
		for(int i=0 ;i<c.getEmporium().size();i++){
			if(c.getEmporium().get(i)!=null){
				cont++;
			}
		}
		return cont;
	}
	
	public boolean payAssistants( int emporiumInCity){
		boolean ok=false;
		if(player.get_numAssistants()>=emporiumInCity){
			player.set_numAssistants(player.get_numAssistants()-emporiumInCity);
			ok=true;
		}
		return ok;
	}
	public void checkConnectedCitiesAlreadyColonizedByMeAndGetBonus (City c){
		
		for(int i=0;i<c.getNextCities().size();i++){
			for(int k=0;k<c.getNextCities().get(i).getEmporium().size();k++){
				if(c.getNextCities().get(i).getEmporium().get(k).getPlayer().equals(player)){
					takeBonus(c.getNextCities().get(i));
					checkConnectedCitiesAlreadyColonizedByMeAndGetBonus(c.getNextCities().get(i));
				}
			}
		}
	}

	public void takeBonus(City c){
		for(int j=0;j<c.getBonus().size();j++)
			c.getBonus().get(j).useBonus(player, map);
	}
	@Override
	public boolean doAction(){
		boolean ok=false;
		City c=selectCity(map);
		if(!c.isColonizedByMe(player)){
			int i=checkExistenceOfOtherEmporium(c);
			 ok=false;
			if(payAssistants(i)){
			placeEmporium(c);
			takeBonus(c);
			checkConnectedCitiesAlreadyColonizedByMeAndGetBonus(c);
			ok=true;
		}
		
	}else cli.printString("You have already built an emprium in this city");
		return ok;
	}
}