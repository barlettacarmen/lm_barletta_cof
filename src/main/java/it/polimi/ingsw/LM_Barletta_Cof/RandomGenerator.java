package it.polimi.ingsw.LM_Barletta_Cof;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomGenerator {
	private List <Integer> numbers;
	private int range; //7 cioè il numero dei tipi di bonus
	private Random r;
	
	public RandomGenerator(int range){
		this.range=range;
		r= new Random();
		numbers= new ArrayList<Integer>(range);
		for(int i=1;i<range+1;i++)
			numbers.add(i);
	}
//restituisce numeri compresi fra 1 e range senza ripetizioni	
	public int nextInt(){
		int index=r.nextInt(numbers.size());
		return numbers.remove(index);
	}
	public boolean isEmpty(){
		return numbers.isEmpty();
		
	}
	//metodo che sceglie quanti bonus piazzare (il min deve essere quello effettivo, mentre il max deve
	// essere aumentato di uno rispetto a quello voluto effettivamente
	// se per esempio voglio almeno un bonus e al massimo 7 bouns allora min=1 e max=8
	public static int choose_NumOfBonus(int min,int max){ 
		Random random= new Random();
		max-=min;
		return random.nextInt(max)+min;
	}

}


