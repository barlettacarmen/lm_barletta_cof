package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class Item implements Serializable{
	private PermitCard permitCard;
	private PoliticsCard politicsCard;
	private int price;
	private Player seller;

	public Item (Player p,PermitCard c,int price){
		this.permitCard=c;
		this.price=price;
		this.seller=p;
	}
	public Item (Player p,PoliticsCard c,int price){
		this.politicsCard=c;
		this.price=price;
		this.seller=p;
	}
	public PermitCard getPermitCard(){
		return this.permitCard;
	}
	public PoliticsCard getPoliticsCard(){
		return this.politicsCard;
	}
	public int getPrice(){
		return this.price;
	}
	public Player getSeller(){
		return this.seller;
	}

}
