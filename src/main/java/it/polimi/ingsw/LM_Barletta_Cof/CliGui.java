package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
import java.util.ArrayList;

import it.polimi.ingsw.LM_Barletta_Cof.networking.CliInterface;

public interface CliGui extends Serializable{

	CliInterface getCliInterface();

	void printString(String s);

	int getInteger();

	String getString();

	char getChar();
	////////////////////

	boolean wantToPerformQuickAction();

	int askForTipeOfAction();

	int askForTipeOfMainAction();



	//////////////////////////////prova login///////////
	String askForLogin();

	/////////////////////////////////////////////////////////	


	void printPlayerStatus(Player p);

	void printPlayersCardList(Player p);

	String askForColorOfCouncillor();

	void printPoolOfCouncillors(CouncillorPool pool);

	//Methods for the player
	int askForCouncil();

	int askForRegion();

	//quiiiiiiiiiiiiiiiiiiiiiiiiixllx's;l'L'l
	int askForPermitCard(ArrayList<PermitCard> permits);

	City askForColonizedCity(ArrayList<Emporium> list);

	//End of player's methods
	//
	//Methods for the market
	int askForPoliticsCard(ArrayList<PoliticsCard> cards);

	int askForTypeOfCard();

	int askForPrice();

	void printItemList(ArrayList<Item> itemList);

	int askForItemToChoose();

	void printExchangeList(ArrayList<Exchange> exchangeList);

	int askForExchangeToChoose();

	//End of market methods
	//
	//Methods for the actions
	void printCouncillors(ArrayList<Councillor> list);

	Councillor askForCouncillor(CouncillorPool list);

	City askForCity(Region r);

	int askForNumberOfPoliticsCard();

	int askForMulticolourCardNumber();

	City askForNextCity(City c);

	boolean askIfFinalMove();

	String askForCityToColonize(ArrayList<String> list);

	///////////////////////////////////////////////////////////////////////////////////////
	Region choosenRegion(Map map);

	int choosenPermitCard(Region r);

}