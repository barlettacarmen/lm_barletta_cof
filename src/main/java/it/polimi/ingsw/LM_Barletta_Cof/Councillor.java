package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;

public class Councillor implements Serializable{
	private PoliticsColours colour;
	private boolean available;
	
	public Councillor(PoliticsColours colour){
		this.colour=colour;
		this.available=true;
		
	}

	public PoliticsColours getColour() {
		return this.colour;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
}
