package it.polimi.ingsw.LM_Barletta_Cof;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CouncillorPool implements Serializable {
	private ArrayList<Councillor> availableCouncillors;
	
	
	public CouncillorPool(){
		int i;
		this.availableCouncillors= new ArrayList<Councillor>(Constants.NUM_OF_COUNCILLORS);
		for(i=0;i<4;i++)
			this.availableCouncillors.add(new Councillor(PoliticsColours.BLACK));
		for(i=4;i<8;i++)
			this.availableCouncillors.add(new Councillor(PoliticsColours.CELESTIAL));
		for(i=8;i<12;i++)
			this.availableCouncillors.add(new Councillor(PoliticsColours.ORANGE));
		for(i=12;i<16;i++)
			this.availableCouncillors.add(new Councillor(PoliticsColours.PINK));
		for(i=16;i<20;i++)
			this.availableCouncillors.add(new Councillor(PoliticsColours.PURPLE));
		for(i=20;i<24;i++)
			this.availableCouncillors.add(new Councillor(PoliticsColours.WHITE));
	}

	public Councillor popConucillor(Councillor c){
			c.setAvailable(false);
			return c;
			
	}
	public void pushCouncillor(Councillor c){
		c.setAvailable(true);
	}
	
	public ArrayList<Councillor> getAvailableCouncillors() {
	return availableCouncillors;
	}	
	
}



	
	

