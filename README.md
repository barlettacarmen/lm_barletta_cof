# README #

Implemented technologies :
Game working with extended rules, every time a client connects to the server it is added to the current waiting match, match starts twenty seconds after the connection of the second client, meanwhile multiple clients can connect to the match. When a match starts the server creates a new waiting match and waits for other client.
Both socket and rmi are implemented and working, when a client is started it asks the user if he wants to connect using rmi or socket.
Cli and gui have been implemented, there are two different types of clients: one uses Cli, the other has a gui.
Both clients implement socket and rmi.
A match can be played by clients of all types together.
If a match is played only by Cli clients then the first player will be asked to choose between a custom configuration file and one of the default ones chosen randomly. If he choose the custom one a new configuration file will be created by asking the player for parameters like number of cities, links between cities and number of bonuses for the various elements. Once the creation of the configuration file is complete the match starts.
During the match, if a client takes more than three minutes to finish its turn it is set offline and will no longer be asked to play another turn.
instructions :
Open the project
Go to class Server in the networking package  !  Server can be started only one at time, you can't start a server if another one is running
Start server 
Go to client or clientgui in networking package and start it
Select the type of connection (socket or RMI) 
Wait until you're asked to type your Username (rmi gui clients are asked immediately, the other ones when the match starts)
When the match starts the first player will be asked to play its turn, then the second one and so on.